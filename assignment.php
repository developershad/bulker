<?php include('config.php'); 
	$courseData  = courseList($conn);
if(isset($_POST['submit']))
{
	$addData = assignmentAdd($_POST, $_FILES, $conn) ;
}
$assignmentData = assignmentList($conn);
//echo "<pre>"; print_r($assignmentData); die;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#product-table .minwidth-1 {
	min-width: 20px;
}
</style>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    <div id="page-heading">
      <h1>Assignment</h1>
    </div>
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner">
            <table border="0" width="100%" cellpadding="" cellspacing="0">
              <tr>
                <td>
                <?php if(isset($updateData[0])) { ?>
                <div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left">Record Updated Sucessfully</td>
					<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
                <?php } ?>
                </td>
              </tr>
              <tr valign="top">
                <td>                  
                  <!-- start id-form --> 
<?php if($_SESSION['TYPE'] == 'FACULTY'){ ?>                                   
                  <form name="assignmentForm" id="assignmentForm" action="" method="post" enctype="multipart/form-data" >
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">
                      <tr >
                        <th valign="top">Course:</th>
                        <td>
                        	<input type="hidden" name="faculty" value="<?php echo $_SESSION['ID'] ?>" />
                            <input type="hidden" name="date" value="<?php echo date('y-m-d'); ?>" />
                        	<select name="course" id="course" class="selectStyle">
                            <option value="">select</option>
                            <?php foreach($courseData as $courseVal){ ?>
                            <option value="<?php echo $courseVal['id']; ?>"><?php echo $courseVal['course']; ?></option>
                            <?php } ?>
                          </select></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <th valign="top">Branch:</th>
                        <td><div id="branchResult" >
                            <select  name="branch" id="branch" class="selectStyle" disabled="disabled">
                              <option value="">Please Select Course</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr>
                        <th valign="top">Year:</th>
                        <td><div id="courseYear" >
                            <select name="courseYear" id="cy"  class="selectStyle" disabled="disabled" >
                              <option value="">Please Select Course</option>
                            </select>
                          </div></td>   
   						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <th valign="top">Topic:</th>
                        <td><input type="text" class="inp-form" name="topic" id="topic" />
                        </td>
                      </tr>
                      <tr>
                        <th valign="top">Last Date:</th>
                        <td><input type="text" name="lastDate" class="inp-form" id="lastDate" /> </td>   
   						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <th valign="top">Assignment:</th>
                        <td><input type="file" class="file_1" name="assignment" id="assignment" />
                        </td>
                      </tr>
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="submit" value="Submit"  name="submit" class="form-submit" id=""  />
                          <input type="reset" value="" class="form-reset"  /></td>
                        <td></td>
                      </tr>
                    </table>
                  </form>
<?php } ?>                  
                  <!-- end id-form  -->                  
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" >
				<tr >
					<th class="table-header-repeat line-left minwidth-1"><a>SN</a>	</th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Course</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a >Branch</a></th>
					<th class="table-header-repeat line-left"><a >Year</a></th>
					<th class="table-header-repeat line-left"><a >Topic</a></th>
                    <th class="table-header-repeat line-left"><a >Faculty</a></th>
					<th class="table-header-repeat line-left"><a >Assign Date</a></th>
                    <th class="table-header-repeat line-left"><a >Last Date</a></th>
					<th class="table-header-options line-left"><a >Download</a></th>
				</tr>
<?php $sn=0; foreach($assignmentData as $assignmentVal) { 
if( ($_SESSION['ID'] == $assignmentVal['facultyId']) OR ($_SESSION['TYPE'] == 'SUPERADMIN') OR ($_SESSION['TYPE'] == 'ADMIN') ) {
$sn++; ?>                
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> id="assignment_tr<?php echo $assignmentVal['id']; ?>" >
					<td><?php echo $sn; ?></th>
                    <td><?php echo $assignmentVal['course']; ?></td>
					<td><?php echo $assignmentVal['branch']; ?></td>
					<td><?php echo $assignmentVal['courseYear']; ?></td>
					<td><?php echo $assignmentVal['topic']; ?></td>
                    <td><?php echo $assignmentVal['facultyName']; ?></td>
					<td><?php echo $assignmentVal['date']; ?></td>
                    <td><?php echo $assignmentVal['lastDate']; ?></td>
					<td class="options-width" >
                    	<a title="Download" class="icon-5 info-tooltip" href="downloadcode.php?df=<?php echo $assignmentVal['assignment']; ?>"  ></a>
                    	<a title="Delete" class="icon-2 info-tooltip" onclick="deleteAssignment(<?php echo $assignmentVal['id'] ?>);" style="margin-left:10px;" ></a>
                     </td>
                </tr>            
<?php 
}
} 
if(!$sn){  ?> 
<tr><td colspan="8" align="center" ><img src="images/nrf.png" /> </td></tr>
<?php } ?>
                                  				
		</table>      
                  </td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <!--  start table-content  -->
            <div>
              <center>
                <img id="loaderImage" />
              </center>
            </div>
            <div id="table-content"></div>
            <!--  end content-table  -->
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div id="assignmentDel" style="display:none;" ></div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
$(document).ready(function(){
	$("#course").change(function(){	
			document.getElementById('loaderImage').src='images/loading.gif';
			document.getElementById('table-content').innerHTML='';	
			var id = $(this).find(":selected").val();
			$('#branchResult').load('data.php?id='+id);
			$('#courseYear').load('data1.php?id='+id);
	});
});
function deleteAssignment(id){
var r=confirm("Are You Sure?");
if (r==true)
  {
	  	$('#assignmentDel').load('data11.php?id='+id);
	  	$("#assignment_tr"+id).css({
		"display": "none"});
  }
}
</script>
<script type="text/javascript" >
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"lastDate",
			dateFormat:"%Y-%m-%d"
		});
	};
</script>
</body>
</html>