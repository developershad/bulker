<?php include('config.php'); 
if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		if($_SESSION['TYPE'] != "ADMIN")
		{
			header("location:home.php");	
		}
	}
if(isset($_POST['submit']))
{
	$addData = noticeBoardAdd($_POST, $conn) ;
}
if($_SESSION['TYPE'] == 'SUPERADMIN')
{
	$noticeBoardData = noticeBoardList($conn);
}
else if($_SESSION['TYPE'] == 'ADMIN')
{
	$noticeBoardData = noticeBoardListById($_SESSION['ID'], $conn);
}
//echo "<pre>"; print_r($assignmentData); die;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#product-table .minwidth-1 {
	min-width: 20px;
}
</style>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    <div id="page-heading">
    </div>
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner">
            <table border="0" width="100%" cellpadding="" cellspacing="0">
              <tr>
                <td>
                <?php if(isset($updateData[0])) { ?>
                <div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left">Record Updated Sucessfully</td>
					<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
                <?php } ?>
                </td>
              </tr>
              <tr valign="top">
                <td>                  
                  <!-- start id-form -->                                  
                  <form name="noticeBoardForm" id="noticeBoardForm" action="" method="post" enctype="multipart/form-data" >
                  <input type="hidden" name="id" value="<?php echo $_SESSION['ID'] ?>" />
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">
                      <tr >
                        <th valign="top">Topic:</th>
                        <td colspan="6" ><input type="text" name="heading" id="heading" class="inp-form" style="width:98%;" /></td>                   
                      </tr>
                      <tr >
                        <th valign="top">Detail:</th>
                        <td colspan="8" ><textarea  name="detail" id="detail" class="mceEditor"  ></textarea></td>                   
                      </tr>
                      <tr >
                        <th valign="top">Admin:</th>
                        <td><select name="admin"  class="selectStyle">
                        		<option value="0" >No</option>
                                <option value="1" >Yes</option>
                            </select>    
                          </td> 
                        <th valign="top">Faculty:</th>
                        <td><select name="faculty"  class="selectStyle">
                        		<option value="0" >No</option>
                                <option value="1" >Yes</option>
                            </select></td> 
                        <th valign="top">Student:</th>
                        <td><select name="student"  class="selectStyle">
                        		<option value="0" >No</option>
                                <option value="1" >Yes</option>
                            </select></td>                  
                      </tr>                                          
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="submit" value="Submit"  name="submit" class="form-submit" id=""  />
                          <input type="reset" value="" class="form-reset"  /></td>
                        <td></td>
                      </tr>
                    </table>
                  </form>                 
                  <!-- end id-form  -->                  
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" >
				<tr >
					<th class="table-header-repeat line-left minwidth-1"><a>SN</a>	</th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Heading</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a >Admin</a></th>
					<th class="table-header-repeat line-left"><a >Faculty</a></th>
					<th class="table-header-repeat line-left"><a >Student</a></th>					
					<th class="table-header-options line-left"><a >Action</a></th>
				</tr>   
<?php 
	$sn=0;
	foreach($noticeBoardData as $noticeBoardVal) {
	$sn++; 
?>                        
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> id="noticenews_tr<?php echo $noticeBoardVal['id']; ?>" >
					<td><?php  echo $sn; ?></th>
                    <td><?php echo $noticeBoardVal['heading']; ?></th>
					<td><?php echo ($noticeBoardVal['admin'])?'Yes':'No'; ?></th>
					<td><?php echo ($noticeBoardVal['faculty'])?'Yes':'No'; ?></th>
					<td><?php echo ($noticeBoardVal['student'])?'Yes':'No'; ?></th>						
					<td class="options-width" >
                    	<a title="View Detail" class="icon-1 info-tooltip" href="javascript: viewDetail(<?php echo $noticeBoardVal['id']; ?>);"  ></a>
                        <input type="hidden" id="detailStatus<?php echo $noticeBoardVal['id'] ?>" value="0" />
                    	<a title="Delete" class="icon-2 info-tooltip" onclick="deleteNoticeNews(<?php echo $noticeBoardVal['id'] ?>);" style="margin-left:10px;" ></a>
                     </td>
                </tr> 
                <tr id="detail<?php echo $noticeBoardVal['id']; ?>" style="display:none;" >
                	<td colspan="6">
                    	<div style="float:left;"><?php echo $noticeBoardVal['detail']; ?></div>
                    </td>
                </tr>    
<?php 
}
if(!$sn){  ?> 
<tr><td colspan="8" align="center" ><img src="images/nrf.png" /> </td></tr>
<?php } ?>                      
                                  				
		</table>      
                  </td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <!--  start table-content  -->
            <div>
              <center>
                <img id="loaderImage" />
              </center>
            </div>
            <div id="table-content"></div>
            <!--  end content-table  -->
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div id="del" style="display:none;" ></div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
function deleteNoticeNews(id){
var r=confirm("Are You Sure?");
if (r==true)
  {
	  	$('#del').load('data12.php?id='+id);
	  	$("#noticenews_tr"+id).css({
		"display": "none"});
  }
}
function viewDetail(id){
	var status = document.getElementById('detailStatus'+id).value;
	if(status == 1 )
	{
		$("#detail"+id).css({
		"display": "none"});
		document.getElementById('detailStatus'+id).value = 0;
	}
	else
	{
		$("#detail"+id).css({
		"display": "table-row"});	
		document.getElementById('detailStatus'+id).value = 1;
	}
}
</script>
</body>
</html>