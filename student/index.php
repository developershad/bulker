<?php
	include('../config.php');
	if(isset($_POST['loginButton']))
	{
		$data = loginByStudent($_POST, $conn);
		if($data){ header("location:my-notice.php"); }
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php') ?>
<script>
$().ready(function() {	
	$("#facultyLogin").validate({
		rules: {			
			username: {
				required: true,
				},
			password: {
				required: true,
				},
			loginType: {
				required: true,
				},																																	
		},
	});											
});
</script>
<style>
input.error[type="text"], input.error[type="password"], textarea.error, select.error {
    border: 2px solid #FF0000;
    border-radius: 5px;
    margin-bottom: -4%;
    margin-top: -4%;
}	
</style>
</head>
<body id="login-bg-student"> 
<!-- Start: login-holder -->
<div id="login-holder">
	<!-- start logo -->
    
	<div id="logo-login">
		<a onclick="newid();" ><h1 style="color:#FFF; margin-left:15px;">Ashoka Institute of Technology & Managment</h1></a>
      </div>
	<!-- end logo -->
	
	<div class="clear"></div>
	
	<!--  start loginbox ................................................................................. -->
	<div id="loginbox">
	<h1 style="margin-bottom: 5%; margin-top: -6%; text-align:center" >Student Login</h1>
	<!--  start login-inner -->
	<div id="login-inner">
    	<form name="facultyLogin" id="facultyLogin" method="post" >
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Username</th>
			<td><input type="text" name="username" class="login-inp" /></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password"  name="password" class="login-inp" /></td>
		</tr>
       
		<!--<tr>
			<th></th>
			<td valign="top"><input type="checkbox" class="checkbox-size" id="login-check" /><label for="login-check">Remember me</label></td>
		</tr>-->
		<tr>
			<th></th>
			<td><input type="submit" name="loginButton" class="submit-login"  /></td>
		</tr>
		</table>
        </form>
	</div>
 	<!--  end login-inner -->
	<div class="clear"></div>
	<a href="javascript:void(0);" class="forgot-pwd">Forgot Password?</a>
 </div>
 <!--  end loginbox -->
 
	<!--  start forgotbox ................................................................................... -->
	<div id="forgotbox">
		<div id="forgotbox-text">Please send us your email and we'll reset your password.</div>
		<!--  start forgot-inner -->
		<div id="forgot-inner">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Email address:</th>
			<td><input type="text" value=""   class="login-inp" /></td>
		</tr>
		<tr>
			<th> </th>
			<td><input type="button" class="submit-login"  /></td>
		</tr>
		</table>
		</div>
		<!--  end forgot-inner -->
		<div class="clear"></div>
		<a href="" class="back-login">Back to login</a>
	</div>
	<!--  end forgotbox -->

</div>
<!-- End: login-holder -->
</body>
</html>