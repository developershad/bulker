<?php include('../config.php'); 
	$attendance = attendanceForStudent($_SESSION['SID'], $conn);
	$course = $attendance[0]['course'];
	$branch = $attendance[0]['branch'];
	$courseYear = $attendance[0]['courseYear'];
	
	$sub1 = $attendance[0]['sub1'];
	$sub2 = $attendance[0]['sub2'];
	$sub3 = $attendance[0]['sub3'];
	$sub4 = $attendance[0]['sub4'];
	$sub5 = $attendance[0]['sub5'];
	$sub6 = $attendance[0]['sub6'];
	$sub7 = $attendance[0]['sub7'];
	$sub8 = $attendance[0]['sub8'];
	
	$subject1 = subjectName($course, $branch, $courseYear, 1, $conn);
	$subject2 = subjectName($course, $branch, $courseYear, 2, $conn);
	$subject3 = subjectName($course, $branch, $courseYear, 3, $conn);
	$subject4 = subjectName($course, $branch, $courseYear, 4, $conn);
	$subject5 = subjectName($course, $branch, $courseYear, 5, $conn);
	$subject6 = subjectName($course, $branch, $courseYear, 6, $conn);
	$subject7 = subjectName($course, $branch, $courseYear, 7, $conn);
	$subject8 = subjectName($course, $branch, $courseYear, 8, $conn);
	
	$subject1T = subjectTotal($course, $branch, $courseYear, 1, $conn);
	$subject2T = subjectTotal($course, $branch, $courseYear, 2, $conn);
	$subject3T = subjectTotal($course, $branch, $courseYear, 3, $conn);
	$subject4T = subjectTotal($course, $branch, $courseYear, 4, $conn);
	$subject5T = subjectTotal($course, $branch, $courseYear, 5, $conn);
	$subject6T = subjectTotal($course, $branch, $courseYear, 6, $conn);
	$subject7T = subjectTotal($course, $branch, $courseYear, 7, $conn);
	$subject8T = subjectTotal($course, $branch, $courseYear, 8, $conn);

		

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
.list {
	display: list-item;
	margin: 0 0 0 15px;
	color: #666666;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	line-height: 20px;
	text-align: justify;
	text-decoration: none;
	margin-bottom:1%;
}
.noticeNews {
	float: left;
	height: 400px;
	overflow: auto;
	width: 49%;
}
.noticeNewsRgt {
	float: right;
	height: 400px;
	overflow: auto;
	width: 49%;
}

</style>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner"> 
            
            <!--  start table-content  -->
            <div id="table-content">
              <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" >
				<tr >
					<th class="table-header-repeat line-left minwidth-1"  ><a>SN</a>	</th>
					<th class="table-header-repeat line-left"><a >Subject</a></th>
					<th class="table-header-repeat line-left minwidth-1"><a >Total</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Present</a></th>
					<th class="table-header-options line-left minwidth-1"><a >Percent [%]</a></th>
				</tr> 
                <?php $sn = 1;  ?>
                <?php if($subject1){ ?>             
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject1; ?></td>
                    <td><?php echo $subject1T; ?></td>
                    <td><?php echo $sub1; ?></td>
                    <td><?php echo round($sub1/$subject1T*100).'%'; ?></td>
                </tr>
                <?php } if($subject2){ ?>
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject2; ?></td>
                    <td><?php echo $subject2T; ?></td>
                    <td><?php echo $sub2; ?></td>
                    <td><?php echo round($sub2/$subject2T*100).'%'; ?></td>
                </tr>
                <?php } if($subject3){ ?>
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject3; ?></td>
                    <td><?php echo $subject3T; ?></td>
                    <td><?php echo $sub3; ?></td>
                    <td><?php echo round($sub3/$subject3T*100).'%'; ?></td>
                </tr>
                <?php } if($subject4){ ?>
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject4; ?></td>
                    <td><?php echo $subject4T; ?></td>
                    <td><?php echo $sub4; ?></td>
                    <td><?php echo round($sub4/$subject4T*100).'%'; ?></td>
                </tr>
                <?php } if($subject5){ ?>
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject5; ?></td>
                    <td><?php echo $subject5T; ?></td>
                    <td><?php echo $sub5; ?></td>
                    <td><?php echo round($sub5/$subject5T*100).'%'; ?></td>
                </tr>
                <?php } if($subject6){ ?>
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject6; ?></td>
                    <td><?php echo $subject6T; ?></td>
                    <td><?php echo $sub6; ?></td>
                    <td><?php echo round($sub6/$subject6T*100).'%'; ?></td>
                </tr>
                <?php } if($subject7){ ?>
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject7; ?></td>
                    <td><?php echo $subject7T; ?></td>
                    <td><?php echo $sub7; ?></td>
                    <td><?php echo round($sub7/$subject7T*100).'%'; ?></td>
                </tr>
                <?php } if($subject8){ ?>
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td><?php echo $sn++; ?></td>
                    <td><?php echo $subject8; ?></td>
                    <td><?php echo $subject8T; ?></td>
                    <td><?php echo $sub8; ?></td>
                    <td><?php echo round($sub8/$subject8T*100).'%'; ?></td>
                </tr>
                <?php } ?>
                

                            

                                  				
		</table>
            </div>
            <!--  end table-content  -->
            
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!-- <div id="footer-pad">&nbsp;</div> --> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
function viewDetail(id){
	document.getElementById('preview').innerHTML = document.getElementById('detail_div'+id).innerHTML;
}
</script>
</body>
</html>