<?php include('../config.php'); 
if(isset($_POST['submit']))
{
	$changePasswordData = changePassword($_SESSION['SID'], $_POST, $conn);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
.list {
	display: list-item;
	margin: 0 0 0 15px;
	color: #666666;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	line-height: 20px;
	text-align: justify;
	text-decoration: none;
	margin-bottom:1%;
}
.noticeNews {
	float: left;
	height: 400px;
	overflow: auto;
	width: 49%;
}
.noticeNewsRgt {
	float: right;
	height: 400px;
	overflow: auto;
	width: 49%;
}

#product-table td {
    border: 0 solid #D2D2D2;
    padding: 10px 0 10px 10px;
}
</style>

</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner"> 
          <?php 
		  if(isset($_POST['submit'])){
		  if($changePasswordData ==1 ){ ?>
            <div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left">Password Changed Sucessfully</td>
					<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>            
              <?php } else {  ?>
                <div id="message-red">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="red-left">ERROR!!! Wrong Password</td>
					<td class="red-right"><a class="close-red"><img src="images/table/icon_close_red.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>  
               <?php } }?> 
            <!--  start table-content  -->
            <div id="table-content">
            <form id="changePassword" method="post" >
              <table  width="100%" cellpadding="0" cellspacing="0" id="product-table" >
                             
				<tr>
					<td><p>Old Password</p></td>
                    <td><input type="password" class="inp-form" name="oldPassword" /> </td>
                </tr>
               	<tr>
					<td><p>New Password</p></td>
                    <td><input type="password" class="inp-form" name="newPassword" id="newPassword" /> </td>
                </tr>
				<tr>
					<td><p>Confirm Password</p></td>
                    <td><input type="password" class="inp-form" name="confirmPassword" /> </td>
                </tr> 
               	<tr>
					<td><p>&nbsp;</p></td>
                    <td><input type="submit" value="Submit" name="submit" class="form-submit" /> </td>
                </tr>                  
                           				
		</table>
        </form>
            </div>
            <!--  end table-content  -->
            
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!-- <div id="footer-pad">&nbsp;</div> --> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
function viewDetail(id){
	document.getElementById('preview').innerHTML = document.getElementById('detail_div'+id).innerHTML;
}
</script>
</body>
</html>