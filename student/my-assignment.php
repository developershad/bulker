<?php include('../config.php'); 
	$assignmentData = assignmentForStudent($_SESSION['SID'], $conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
.list {
	display: list-item;
	margin: 0 0 0 15px;
	color: #666666;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 13px;
	font-weight: bold;
	line-height: 20px;
	text-align: justify;
	text-decoration: none;
	margin-bottom:1%;
}
.noticeNews {
	float: left;
	height: 400px;
	overflow: auto;
	width: 49%;
}
.noticeNewsRgt {
	float: right;
	height: 400px;
	overflow: auto;
	width: 49%;
}

</style>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner"> 
            
            <!--  start table-content  -->
            <div id="table-content" style="height:400px; overflow:auto;">
              <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table"  >
				<tr >
					<th class="table-header-repeat line-left minwidth-1"  ><a>SN</a>	</th>
					<th class="table-header-repeat line-left"><a >Topic</a></th>
					<th class="table-header-repeat line-left minwidth-1"><a >Faculty Name</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Assign Date</a></th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Last Date</a></th>
					<th class="table-header-options line-left minwidth-1"><a >Download</a></th>
				</tr>
<?php $sn=0; foreach($assignmentData as $assignmentVal) { 
$sn++; ?>                
				<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> id="assignment_tr<?php echo $assignmentVal['id']; ?>" >
					<td><?php echo $sn; ?></th>
					<td><?php echo $assignmentVal['topic']; ?></th>
					<td><?php echo $assignmentVal['facultyName']; ?></th>
                    <td><?php echo $assignmentVal['date']; ?></th>
                    <td><?php echo $assignmentVal['lastDate']; ?></th>
					<td class="options-width" >
                    	<a title="Download" class="icon-5 info-tooltip" href="../downloadcode.php?df=<?php echo $assignmentVal['assignment']; ?>"  ></a>
                     </td>
                </tr>            
<?php 
}
if(!$sn){  ?> 
<tr><td colspan="8" align="center" ><img src="images/nrf.png" /> </td></tr>
<?php } ?>
                                  				
		</table>
            </div>
            <!--  end table-content  -->
            
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!-- <div id="footer-pad">&nbsp;</div> --> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
function viewDetail(id){
	document.getElementById('preview').innerHTML = document.getElementById('detail_div'+id).innerHTML;
}
</script>
</body>
</html>