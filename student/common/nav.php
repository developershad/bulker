<?php $pageName = basename($_SERVER['PHP_SELF']); 
$studentName = studentNameById($_SESSION['SID'], $conn);
 ?>
<div id="page-top-outer">
  <div id="page-top">
    <div id="logo"> <a href="my-notice.php"><img src="images/shared/logo.png" style="width:86%;" alt="" /></a> </div>
    <div id="top-search" style="float:left; margin-left:16%;" >
      <table border="0" cellpadding="0" cellspacing="0">        
        <tr><td ><p class="logoHeading" >ASHOKA</p></td></tr>
        <tr><td><p class="logoText" >INSTITUTE OF TECHNOLOGY AND MANAGMENT</p></td></tr>
      </table>
    </div>
    <div style=" float: right; margin-top: 0.5%;  margin-right: 0.5%;" > <img src="../images/shared/student.jpg"  alt="" /> </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear">&nbsp;</div>
<div class="nav-outer-repeat"> 
  <!--  start nav-outer -->
  <div class="nav-outer"> 
    
    <!-- start nav-right -->
    <div id="nav-right">
      <div class="nav-divider">&nbsp;</div>
      <div class="showhide-account">
        <h3 style="color:#F5F5F5;" ><?php echo $studentName; ?><img src="images/table/table_sort_arrow.gif" /></h3>
      </div>
      <div class="nav-divider">&nbsp;</div>
      <a href="logout.php" id="logout"><img src="images/shared/nav/nav_logout.gif" width="64" height="14" alt="" /></a>
      <div class="clear">&nbsp;</div>
      
      <!--  start account-content -->
      <div class="account-content">
        <div class="account-drop-inner">
        <a href="password.php" id="acc-settings">Change Password</a>
          <div class="clear">&nbsp;</div>
          <div class="acc-line">&nbsp;</div>


       
          </div>
      </div>
      <!--  end account-content --> 
      
    </div>
    <!-- end nav-right --> 
    
    <!--  start nav -->

    <div class="nav">
      <div class="table">
        <ul class="<?php if($pageName =='my-notice.php' ){echo 'current'; }else{ echo'select';} ?>">
          <li> <a href="my-notice.php"> <b>Notice Board</b> </a>
          </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
       <ul class="<?php if($pageName =='my-assignment.php' ){echo 'current'; }else{ echo'select';} ?>">
          <li><a href="my-assignment.php"><b>Assignment</b><!--[if IE 7]><!--></a><!--<![endif]--> 
          </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
        <ul class="<?php if($pageName =='my-attendance.php' ){echo 'current'; }else{ echo'select';} ?>">
        <li><a href="my-attendance.php"><b>Attendance</b><!--[if IE 7]><!--></a><!--<![endif]--> 
          </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
        

        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!--  start nav --> 
    
  </div>
  <div class="clear"></div>
  <!--  start nav-outer --> 
</div>
