     tinyMCE.init({
        // General options
        mode: "textareas",
        theme: "advanced",
        width: "100%",
        height: "100",

        // Theme options
    //    theme_advanced_buttons1: "bold,italic,underline,|,justifyleft,justifycenter,justifyright,justifyfull",
    //    theme_advanced_buttons2: "",
    //    theme_advanced_buttons3: "",
     //   theme_advanced_buttons4: "",
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect,bullist,numlist,image,code,|,insertdate,inserttime,preview,|,forecolor,backcolor,|,removeformat,charmap",
                theme_advanced_buttons2 : "",
                theme_advanced_buttons3 : "",
                theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_toolbar_align: "left",
        theme_advanced_resizing: false,

    // Selector
        editor_selector: "mceEditor",

});		
		