$().ready(function() {	
	$("#changePassword").validate({
		rules: {			
			oldPassword: {
				required: true,
				},
			newPassword: {
				required: true,
				},											
			confirmPassword: {
				required: true,
				equalTo: '#newPassword',
				},												
																				
		},
	});															
});
