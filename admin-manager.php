<?php include('config.php'); 
	if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		header("location:home.php");	
	}
	if(isset($_POST['submit']))
	{
		$updateId=$_POST['id'];
		$updateData = updateAdmin($updateId, $_POST, $conn);
		
	}
	if(isset($_POST['AddButton']))
	{
		$addData = adminAdd($_POST, $conn);		
	}	
	$id = $_SESSION['ID'];
	$adminData = allAdmin($conn);	
			//echo "<pre>"; print_r($_POST); die;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#product-table .minwidth-1 {
    min-width: 20px;
}
.rgt{
    float: right;
    margin-right: 1.6%;
    margin-top: -2%;
}
.button{
	background: none repeat scroll 0 0 #999999;
    border: 0 none;
    border-radius: 10px;
    color: #FFFFFF;
    font-family: verdana;
    padding: 4px;
}

</style>
</head>
<body> 
<?php include('common/nav.php') ?>
 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Admin Manager</h1>
        <input type="button" value="Add New Admin" class="rgt button" onclick="openAddAdmin();" />
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		<table border="0" width="100%" cellpadding="" cellspacing="0">
          <tr>
			<td>
              
              
             <?php if(isset($updateData)){ ?>
				
				<!--  start message-blue -->
				<div id="message-blue">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blue-left">Admin Record Updated Sucessfully</td>
					<td class="blue-right"><a class="close-blue"><img src="images/table/icon_close_blue.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
				<!--  end message-blue -->
		<?php } ?>	
        <?php if(isset($addData)){ ?>
				<!--  start message-green -->
				<div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left">Admin added sucessfully.</td>
					<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
				<!--  end message-green -->
		<?php } ?>
              
                </td>					
			  </tr>     
<tr>
<td>
		<div id="editForm" style="display:none; width:96%; margin-bottom:1%;" class="editFormBackground" >
                    <h1 class="editHording" >UPDATE ADMIN RECORD<span class="closeButton" onclick="javascript: closeEditForm();" >&nbsp;</span></h1>
                    <table style="border:1px solid #FFF;" width="100%" cellpadding="0" cellspacing="0">
                      <tr valign="top">
                        <td><!-- start id-form -->
                          
                     <form name="adminEditForm" id="adminEditForm" action="" method="post" enctype="multipart/form-data" style=" margin: 1% 0 0;" />
                      <input type="hidden" name="id" />                          
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form"  >                    
                    <tr>                      
                      <th valign="top"><span class="leftPadding_2">Username:</span></th>
                      <td><input type="text" class="inp-form" name="username"  disabled="disabled" readonly="readonly" /></td>
                      <th valign="top"><span class="leftPadding_2">Password</span></th>
                      <td><input type="password" class="inp-form" name="password" id="passwordEdit"  /></td>
                      <th valign="top"><span class="leftPadding_2">Confirm Password:</span></th>
                      <td><input type="password" class="inp-form" name="confirmPassword" id="confirmPassword"  /></td>
                    </tr>
                    <tr>
                      <th valign="top"><span class="leftPadding_2">Admin name:</span></th>
                      <td><input type="text" class="inp-form" name="name"   /></td>
                      <th valign="top"><span class="leftPadding_2">Father's name:</span></th>
                      <td><input type="text" class="inp-form" name="fatherName"  /></td>
                      <th valign="top"><span class="leftPadding_2">Mother's name:</span></th>
                      <td><input type="text" class="inp-form" name="motherName" /></td>
                    </tr>
                    <tr>                      
                      <th valign="top"><span class="leftPadding_2">Email:</span></th>
                      <td><input type="text" class="inp-form" name="email"  /></td>
                      <th valign="top"><span class="leftPadding_2">DOB</span></th>
                      <td><input type="text" class="inp-form" name="dob" id="dob" readonly="readonly"  /></td>
                      <th valign="top"><span class="leftPadding_2">Nationality:</span></th>
                      <td><input type="text" class="inp-form" name="nationality"   /></td>
                    </tr>
                    <tr>
                      <th valign="top"><span class="leftPadding_2">Mobile</span></th>
                      <td><input type="text" class="inp-form" name="mobile"  /></td>
                      <th valign="top"><span class="leftPadding_2">Type</span></th>
                      <td><input type="text" class="inp-form" name="type"  readonly="readonly" disabled="disabled" /></td>
                      <th valign="top"><span class="leftPadding_2">Sex</span></th>
                      <td><select name="sex" id="sex" class="selectStyle"  >                                                
                            <option value="Male" >Male</option>
                            <option value="Female" >Female</option>
                          </select></td>
                    </tr>
                    
                    <tr>                          
                      <th valign="top"><span class="leftPadding_2">Marital Status</span></th>
                      <td><select name="maritalStatus" class="selectStyle" >                                                        
                            <option value="Unmarried" >Unmarried</option>
                            <option value="Married"  >Married</option>
                          </select></td>                                                                           	  
                      <th valign="top"><span class="leftPadding_2">Permament:</span></th>
                      <td><textarea rows="" cols="" class="form-textarea" name="permamentAddress" style="width:90%; height:50px;" ></textarea></td>
                      <th valign="top"><span class="leftPadding_2">Residential:</span></th>
                      <td><textarea rows="" cols="" class="form-textarea" name="residentialAddress" style="width:90%; height:50px;" ></textarea></td>                      
                    </tr>
                    <tr>
                      <th>&nbsp;</th>
                      <td valign="top"><input type="submit" value="Submit" class="form-submit" name="submit" id="submit" />
                        <input type="reset" value="" class="form-reset"  /></td>
                      <td></td>
                    </tr>
                  </table>
                          </form>
                          
                          <!-- end id-form  --></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                        <td></td>
                      </tr>
                    </table>
                    <div class="clear"></div>
                  </div>
</td>
</tr>
<tr>
<td>
		<div id="addForm" style="display:none; width:96%; margin-bottom:1%;" class="addFormBackground" >
                    <h1 class="addHording" >ADD NEW ADMIN<span class="closeButton" onclick="javascript: closeAddForm();" >&nbsp;</span></h1>
                    <table style="border:2px solid #A8A8A8;;" width="100%" cellpadding="0" cellspacing="0">
                      <tr valign="top">
                        <td><!-- start id-form -->
                          
                     <form name="adminAddForm" id="adminAddForm" action="" method="post" enctype="multipart/form-data" style=" margin: 1% 0 0;" />
                                         
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form"  >                    
                    <tr>                      
                      <th valign="top" ><span class="leftPadding_2">Username:</span></th>
                      <td><input type="text" class="inp-form" name="username" onchange="checkUsername();" /></td>
                      <td >
                      <input type="button" style="background: none repeat scroll 0 0 #7D7D7D;
                                                    border: medium none;
                                                    border-radius: 5px;
                                                    color: #FFFFFF;
                                                    font-size: 12px;
                                                    font-weight: bold;
                                                    padding: 4px;" 
                      value="Check Availbility" onclick="checkUsername();" />
                      </td>
                      <td>
                      <span style="float:left; margin-left:5%;" id="checkUsernameText"></span></td>
                      </tr>
                      <tr>
                      <th valign="top"><span class="leftPadding_2">Password</span></th>
                      <td><input type="password" class="inp-form" name="password" id="password"  /></td>
                      <th valign="top"><span class="leftPadding_2">Confirm Password:</span></th>
                      <td><input type="password" class="inp-form" name="confirmPassword" id="confirmPassword"  /></td>
                      <th valign="top"><span class="leftPadding_2">Admin name:</span></th>
                      <td><input type="text" class="inp-form" name="name"   /></td>
                    </tr>
                    <tr>                      
                      <th valign="top"><span class="leftPadding_2">Father's name:</span></th>
                      <td><input type="text" class="inp-form" name="fatherName"  /></td>
                      <th valign="top"><span class="leftPadding_2">Mother's name:</span></th>
                      <td><input type="text" class="inp-form" name="motherName" /></td>                    
                      <th valign="top"><span class="leftPadding_2">Email:</span></th>
                      <td><input type="text" class="inp-form" name="email"  /></td>
                    </tr>
                    <tr>                        
                      <th valign="top"><span class="leftPadding_2">DOB</span></th>
                      <td><input type="text" class="inp-form" name="dob" id="dob1" readonly="readonly"  /></td>
                      <th valign="top"><span class="leftPadding_2">Nationality:</span></th>
                      <td><input type="text" class="inp-form" name="nationality"   /></td>
                      <th valign="top"><span class="leftPadding_2">Mobile</span></th>
                      <td><input type="text" class="inp-form" name="mobile"  /></td>
                    </tr>
                    <tr>                        
                      <th valign="top"><span class="leftPadding_2">Sex</span></th>
                      <td><select name="sex" id="sex" class="selectStyle"  >                                                
                            <option value="Male" >Male</option>
                            <option value="Female" >Female</option>
                          </select></td>                         
                      <th valign="top"><span class="leftPadding_2">Marital Status</span></th>
                      <td><select name="maritalStatus" class="selectStyle" >                                                        
                            <option value="Unmarried" >Unmarried</option>
                            <option value="Married"  >Married</option>
                          </select></td>                                                                           	  
                      <th valign="top"><span class="leftPadding_2">Permament:</span></th>
                      <td><textarea rows="" cols="" class="form-textarea" name="permamentAddress" style="width:93%; height:50px;" ></textarea></td>
                    </tr>
                    <tr>                        
                      <th valign="top"><span class="leftPadding_2">Residential:</span></th>
                      <td colspan="3" ><textarea rows="" cols="" class="form-textarea" name="residentialAddress" style="width:98%; height:50px;" ></textarea></td>                      <td>&nbsp;</td>
                      <td  colspan="2" style="margin-left:10%; "><input type="submit" value="Submit" class="form-submit" name="AddButton" id="AddButton" />
                        <input type="reset" value="" class="form-reset"  /></td>
                      <td></td>
                    </tr>
                  </table>
                          </form>
                          
                        </td>
                      </tr>                      
                    </table>
                    <div class="clear"></div>
                  </div>
</td>
</tr>                      
            <tr valign="top">
                <td>              
              <!-- start id-form -->
              <div><center><img id="loaderImage" /></center></div>
<?php if($adminData){ ?>              
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" >
				<tr>
					<th class="table-header-repeat line-left minwidth-1"><a>SN</a>	</th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Name</a>	</th>
					<th class="table-header-repeat line-left"><a >Email</a></th>
					<th class="table-header-repeat line-left"><a >Mobile</a></th>
					<th class="table-header-options line-left"><a >Action</a></th>
				</tr>
<?php $sn=0; foreach ($adminData as $adminVal ){ $sn++; ?>   					             
					<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> id="admin_tr<?php echo $adminVal['id']; ?>" >
					<td ><?php echo $sn; ?></td>
                    <td><?php echo $adminVal['name']  ?></td>
                    <td><?php echo $adminVal['email'] ?></td>
                    <td><?php echo $adminVal['mobile'] ?></td>
                               
					<td class="options-width">
					<a onclick="javascript: edit(
                    '<?php echo $adminVal['id']; ?>',
                    '<?php echo $adminVal['name']; ?>',
                    '<?php echo $adminVal['fatherName']; ?>',
                    '<?php echo $adminVal['motherName']; ?>',
                    '<?php echo $adminVal['email']; ?>',
                    '<?php echo $adminVal['dob']; ?>',
                    '<?php echo $adminVal['nationality']; ?>',
                    '<?php echo $adminVal['sex']; ?>',
                    '<?php echo $adminVal['maritalStatus']; ?>',                    
                    '<?php echo $adminVal['username']; ?>',
                    '<?php echo $adminVal['password']; ?>',
                    '<?php echo $adminVal['mobile']; ?>',                                                                                
                   '<?php echo mysql_real_escape_string($adminVal['residentialAddress']); ?>',
                   '<?php echo mysql_real_escape_string($adminVal['permamentAddress']); ?>',
                   '<?php echo $adminVal['status']; ?>',
                   '<?php echo $adminVal['type']; ?>'                   
                    );" title="Edit" class="icon-4 info-tooltip"></a>
                    <a onclick="javascript:deleteAdmin('<?php echo $adminVal['id']; ?>');" title="Delete" class="icon-2 info-tooltip"></a>					
					
					</td>
				</tr>
<?php } ?>                				
				</table>
<?php } else { ?>
	<script>document.getElementById('loaderImage').src='images/nrf.png';</script>
    <!--  start related-activities -->
                  
                  <div id="related-activities"  > 
                    
                    <!--  start related-act-top -->
                    <div id="related-act-top"> <img src="images/forms/header_related_act.gif" width="271" height="43" alt="" /> </div>
                    <!-- end related-act-top --> 
                    
                    <!--  start related-act-bottom -->
                    <div id="related-act-bottom"> 
                      
                      <!--  start related-act-inner -->
                      <div id="related-act-inner">
                        <div class="left"></div>
                        <div class="right">
                          <ul class="greyarrow">
                            <li><a href="faculty-add.php" style="font-size:15px" >Faculty Add</a></li>
                          </ul>
                        </div>
                        <div class="clear"></div>
                      </div>
                      <!-- end related-act-inner -->
                      <div class="clear"></div>
                    </div>
                  </div>
                  
                  <!-- end related-act-bottom -->
	
 <?php }?>                
              <!-- end id-form  -->
              
              </td>                       
            </tr>
          </table>
			<!--  start table-content  -->
            
			<div id="table-content"></div>
			<!--  end content-table  -->
			<div class="clear"></div> 
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>  
    <div id="del" style="display:none"; ></div>  
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
	<!--  start footer-left -->
	<?php include('common/footer.php') ?>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
<script>
function edit(id, name, fatherName, motherName, email, dob, nationality, sex, maritalStatus, username, password, mobile,residentialAddress, permamentAddress, status, type){
		$("#editForm").css({
		"display": "table-row"});
	document.adminEditForm.id.value = id;
	document.adminEditForm.name.value = name;
	document.adminEditForm.fatherName.value = fatherName;
	document.adminEditForm.motherName.value = motherName;
	document.adminEditForm.email.value = email;
	document.adminEditForm.dob.value = dob;
	document.adminEditForm.nationality.value = nationality;
	document.adminEditForm.mobile.value = mobile;
	document.adminEditForm.type.value = type;
	document.adminEditForm.email.value = email;
	document.adminEditForm.sex.value = sex;
	document.adminEditForm.maritalStatus.value = maritalStatus;
	document.adminEditForm.permamentAddress.value = permamentAddress;
	document.adminEditForm.residentialAddress.value = residentialAddress;
	document.adminEditForm.username.value = username;
	document.adminEditForm.password.value = password;
	document.adminEditForm.confirmPassword.value = password;
}
function closeEditForm(){
		$("#editForm").css({
		"display": "none"});	
}
function closeAddForm(){
		$("#addForm").css({
		"display": "none"});	
}
function openAddAdmin(){
		$("#addForm").css({
		"display": "table-row"});	
}
function deleteAdmin(id){
var r=confirm("Are You Sure?");
if (r==true)
  {
	  	$('#del').load('data14.php?id='+id);
	  	$("#admin_tr"+id).css({
		"display": "none"});
  }
}
function checkUsername(){
	var username = document.adminAddForm.username.value;
	if(username)
	{
		$('#checkUsernameText').load('data13.php?username='+ encodeURI(username));
	}
}
</script>
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"dob",
			dateFormat:"%Y-%m-%d"
		});
		new JsDatePick({
			useMode:2,
			target:"dob1",
			dateFormat:"%Y-%m-%d"
		});
	};
</script>
</body>
</html>