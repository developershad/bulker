<?php include('config.php'); 
	
	if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		if($_SESSION['TYPE'] != "ADMIN")
		{
			header("location:home.php");	
		}
	}
	
	$courseData  = courseList($conn);
	
	if(isset($_POST['studentAddButton']))
	{
		$insertId = studentAdd($_POST, $_FILES, $conn);
		//$username = strtolower(str_ireplace(" ",'',$_POST['studentName'])).$insertId; 
		//echo "<pre>"; print_r($username); die;
		if($insertId)
		{
			$login = studentLogin($insertId, $_POST['rollNo'], $conn);
		}
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>
<!-- start content-outer -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content">
    <div id="page-heading">
      <h1>Add New Student</h1>
    </div>
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table" style="" >
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td>
        <?php if(isset($login)){ ?>
        <div id="message-blue">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="blue-left"><center><b style="font-size:14px;" >Student Record Added Sucessfully</b></center></td>
					<td class="blue-right"><a class="close-blue"><img src="images/table/icon_close_blue.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
         <?php } ?>       
        <!--  start content-table-inner -->
          
          <div id="content-table-inner">
            <table border="0" width="100%" cellpadding="0" cellspacing="0">
              <tr valign="top">
                <td><!--  start step-holder -->
                  
                  <div id="step-holder">
                    <div class="step-no">1</div>
                    <div class="step-dark-left"><a href="">Fill Student Details</a></div>
                    <div class="step-dark-right">&nbsp;</div>
                    <div class="step-no-off">2</div>
                    <div class="step-light-left">Done</div>
                    <div class="step-light-round">&nbsp;</div>
                    <div class="clear"></div>
                  </div>
                  
                  <!--  end step-holder --> 
                  
                  <!-- start id-form -->
                  
                  <form name="studentAddForm" id="studentAddForm" action="" method="post" enctype="multipart/form-data" />
                  
                  <table border="0" cellpadding="0" cellspacing="0"  id="id-form"  >
                    <tr >
                      <th valign="top">Course:</th>
                      <td><select name="course" id="course" class="selectStyle">
                          <option value="">Select</option>
                          <?php foreach($courseData as $courseVal){ ?>
                          <option value="<?php echo $courseVal['id']; ?>"><?php echo $courseVal['course']; ?></option>
                          <?php } ?>
                        </select></td>
                      <th valign="top">Branch:</th>
                      <td><div id="branchResult" >
                          <select  class="selectStyle" disabled="disabled">
                            <option value="">Please Select Course</option>
                          </select>
                        </div></td>
                    </tr>
                    <tr>
                      <th valign="top">Year:</th>
                      <td><div id="courseYear" >
                          <select  class="selectStyle" disabled="disabled" >
                            <option value="">Please Select Course</option>
                          </select>
                        </div></td>
                    </tr>
                    <tr>
                      <th valign="top">Student Name:</th>
                      <td><input type="text" class="inp-form" name="studentName" /></td>
                      <th valign="top">Father's Name:</th>
                      <td><input type="text" class="inp-form" name="fatherName" /></td>
                    </tr>
                    <tr>
                      <th valign="top">Mother's Name:</th>
                      <td><input type="text" class="inp-form" name="motherName" /></td>
                      <th valign="top">Roll No:</th>
                      <td><input type="text" class="inp-form" name="rollNo" /></td>
                    </tr>
                    <tr>
                      <th valign="top">DOB</th>
                      <td><input type="text" class="inp-form" name="dob" id="dob" readonly="readonly" /></td>
                      <th valign="top">Nationality:</th>
                      <td><input type="text" class="inp-form" name="nationality" value="Indian" /></td>
                    </tr>
                    <tr>
                      <th valign="top">Mobile</th>
                      <td><input type="text" class="inp-form" name="mobile" /></td>
                      <th valign="top">Email</th>
                      <td><input type="text" class="inp-form" name="email" /></td>
                    </tr>
                    <tr>
                      <th valign="top">Sex</th>
                      <td><select name="sex" class="selectStyle"  >                                                        
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                          </select></td>
                      <th valign="top">Marital Status</th>
                      <td><select name="maritalStatus" class="selectStyle" >                                                        
                            <option value="Unmarried">Unmarried</option>
                            <option value="Married">Married</option>
                          </select></td>
                    </tr>
                    <tr>
                      <th valign="top">Category</th>
                      <td><select name="category" class="selectStyle" >                                                        
                            <option value="General">General</option>
                            <option value="OBC">OBC</option>
                            <option value="SC-ST">SC-ST</option>
                          </select></td>
                      <th valign="top">Hostler</th>
                      <td><select name="hostler" class="selectStyle" >                                                        
                            <option value="0">No</option>
                            <option value="1">yes</option>
                          </select></td>
                    </tr>
                    <tr>
                      <th> Student Photo</th>
                      <td ><input type="file" class="file_1" name="studentImage"/></td>
                    </tr>
                    <tr>
                      <th valign="top">Permament Address:</th>
                      <td><textarea rows="" cols="" class="form-textarea" name="permamentAddress" ></textarea></td>
                    </tr>
                    <tr>
                      <th valign="top">Residential Address:</th>
                      <td><textarea rows="" cols="" class="form-textarea" name="residentialAddress" ></textarea></td>
                    </tr>
                    <tr>
                      <th>&nbsp;</th>
                      <td valign="top"><input type="submit" value="Submit" class="form-submit" name="studentAddButton" />
                        <input type="reset" value="" class="form-reset"  /></td>
                      <td></td>
                    </tr>
                  </table>
                  </form>
                  
                  <!-- end id-form  --></td>
                  
                  
                  <td>

	<!--  start related-activities -->
	<div id="related-activities">
		
		<!--  start related-act-top -->
		<div id="related-act-top">
		<img src="images/forms/header_related_act.gif" width="271" height="43" alt="" />
		</div>
		<!-- end related-act-top -->
		
		<!--  start related-act-bottom -->
		<div id="related-act-bottom">
		
			<!--  start related-act-inner -->
			<div id="related-act-inner">
                        <div class="left"></div>
                        <div class="right">
                          <ul class="greyarrow">
                            <li><a href="student-list.php" style="font-size:15px" >Student List</a></li>
                          </ul>
                        </div>
                        <div class="clear"></div>
                      </div>
			<!-- end related-act-inner -->
			<div class="clear"></div>
		
		</div>
		<!-- end related-act-bottom -->
	
	</div>
	<!-- end related-activities -->

</td>
                  
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script type="text/javascript" src="js/calender.js" ></script>
<script>
$(document).ready(function(){
	$("#course").change(function(){		
			var id = $(this).find(":selected").val();
			$('#branchResult').load('data.php?id='+id);
	});
});
$(document).ready(function(){
	$("#course").change(function(){		
			var id = $(this).find(":selected").val();
			$('#courseYear').load('data1.php?id='+id);
	});
});
</script>
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"dob",
			dateFormat:"%Y-%m-%d"
		});
	};
</script>
</body>
</html>