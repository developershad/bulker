<?php include('config.php'); 
	if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		if($_SESSION['TYPE'] != "ADMIN")
		{
			header("location:home.php");	
		}
	}
	$courseData  = courseList($conn);
	if(isset($_POST['studentEditButton']))
	{
		$updateData = updateStudent($_POST, $_FILES, $conn);
	}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#product-table .minwidth-1 {
	min-width: 20px;
}
</style>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    <div id="page-heading">
      <h1>Student List</h1>
    </div>
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner">
            <table border="0" width="100%" cellpadding="" cellspacing="0">
              <tr>
                <td>
                <?php if(isset($updateData[0])) { ?>
                <div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left">Record Updated Sucessfully</td>
					<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
                <?php } ?>
                </td>
              </tr>
              <tr valign="top">
                <td><!--  start step-holder -->
                  
                  <div id="step-holder">
                    <div class="step-no">1</div>
                    <div class="step-dark-left"><a href="" >Fill Detail</a></div>
                    <div class="step-dark-right">&nbsp;</div>
                    <div class="step-no-off">2</div>
                    <div class="step-light-left">Student List</div>
                    <div class="step-light-round">&nbsp;</div>
                    <div class="clear"></div>
                  </div>                  
                  <!--  end step-holder -->                   
                  <!-- start id-form -->                  
                  <form name="studentListForm" id="studentListForm" action="" method="post" >
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">
                      <tr >
                        <th valign="top">Course:</th>
                        <td><select name="course" id="course" class="selectStyle">
                            <option value="">Select</option>
                            <?php foreach($courseData as $courseVal){ ?>
                            <option value="<?php echo $courseVal['id']; ?>"><?php echo $courseVal['course']; ?></option>
                            <?php } ?>
                          </select></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <th valign="top">Branch:</th>
                        <td><div id="branchResult" >
                            <select  name="branch" id="branch" class="selectStyle" disabled="disabled">
                              <option value="">Please Select Course</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr>
                        <th valign="top">Year:</th>
                        <td><div id="courseYear" >
                            <select name="courseYear" id="cy"  class="selectStyle" disabled="disabled" >
                              <option value="">Please Select Course</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="button" value="Submit"  class="form-submit" onclick="searchStudent();" id="searchStudentButton"  />
                          <input type="reset" value="" class="form-reset"  /></td>
                        <td></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!-- end id-form  --> 
                  <!--  start related-activities -->
                  
                  <div id="related-activities"  > 
                    
                    <!--  start related-act-top -->
                    <div id="related-act-top"> <img src="images/forms/header_related_act.gif" width="271" height="43" alt="" /> </div>
                    <!-- end related-act-top --> 
                    
                    <!--  start related-act-bottom -->
                    <div id="related-act-bottom"> 
                      
                      <!--  start related-act-inner -->
                      <div id="related-act-inner">
                        <div class="left"></div>
                        <div class="right">
                          <ul class="greyarrow">
                            <li><a href="student-add.php" style="font-size:15px" >Student Add</a></li>
                          </ul>
                        </div>
                        <div class="clear"></div>
                      </div>
                      <!-- end related-act-inner -->
                      <div class="clear"></div>
                    </div>
                  </div>
                  
                  <!-- end related-act-bottom -->
                  <div id="editForm" style="display:none; width:96%" class="editFormBackground" >
                    <h1 class="editHording" >UPDATE STUDENT RECORD<span class="closeButton" onclick="javascript: closeEditForm();" >&nbsp;</span></h1>
                    <table style="border:1px solid #FFF;" width="100%" cellpadding="0" cellspacing="0">
                      <tr valign="top">
                        <td><!-- start id-form -->
                          
                          <form name="studentEditForm" id="studentEditForm" action="" method="post" enctype="multipart/form-data" />
                          
                          <table border="0" cellpadding="0" cellspacing="0"  id="id-form"  style="margin:1% 1% 0;" >
                            <tr >
                              <th valign="top">Course:</th>
                              <td>
                              <input type="hidden" name="id" />
                              <input type="hidden" name="courseId" />
                              <input type="hidden" name="branchId" />
                              <input type="hidden" name="yearId" />
                              <input type="text" class="inp-form" name="course" disabled="disabled" />
                              </td>
                              <th valign="top">Branch:</th>
                              <td><div id="branchResult" >
                                  <input type="text" class="inp-form" name="branch" disabled="disabled" />
                                </div></td>
                              <th valign="top"><span class="leftPadding_2">Year:</span></th>
                              <td><div id="courseYear" >
                                <input type="text" class="inp-form" name="courseYear" disabled="disabled" />
                                </div></td>
                            </tr>
                            <tr>
                              <th valign="top">Student name:</th>
                              <td><input type="text" class="inp-form" name="studentName" /></td>
                              <th valign="top">Father's name:</th>
                              <td><input type="text" class="inp-form" name="fatherName" /></td>
                              <th valign="top"><span class="leftPadding_2">Mother's name:</span></th>
                              <td><input type="text" class="inp-form" name="motherName" /></td>
                            </tr>
                            <tr>
                              <th valign="top">Roll No:</th>
                              <td><input type="text" class="inp-form" name="rollNo" /></td>
                              <th valign="top">DOB</th>
                              <td><input type="text" class="inp-form" name="dob" id="dob" readonly="readonly" /></td>
                              <th valign="top"><span class="leftPadding_2">Nationality:</span></th>
                              <td><input type="text" class="inp-form" name="nationality" value="Indian" /></td>
                            </tr>
                            <tr>
                              <th valign="top">Mobile</th>
                              <td><input type="text" class="inp-form" name="mobile" /></td>
                              <th valign="top">Email</th>
                              <td><input type="text" class="inp-form" name="email" /></td>
                              <th valign="top"><span class="leftPadding_2">Sex</span></th>
                              <td><select name="sex" id="sex" class="selectStyle"  >
                                  <option value="Male">Male</option>
                                  <option value="Female">Female</option>
                                </select></td>
                            </tr>
                            <tr>
                              <th valign="top">Marital Status</th>
                              <td><select name="maritalStatus" class="selectStyle" >
                                  <option value="Unmarried">Unmarried</option>
                                  <option value="Married">Married</option>
                                </select></td>
                              <th valign="top">Category</th>
                              <td><select name="category" class="selectStyle" >
                                  <option value="General">General</option>
                                  <option value="OBC">OBC</option>
                                  <option value="SC-ST">SC-ST</option>
                                </select></td>
                              <th valign="top"><span class="leftPadding_2">hostler</span></th>
                              <td><select name="hostler" class="selectStyle" >
                                  <option value="0">No</option>
                                  <option value="1">yes</option>
                                </select></td>
                            </tr>
                            <tr>
                              <th> Student Photo</th>
                              <td colspan="2" ><input type="file" class="file_1" name="studentImage"/>
                              					<input type="hidden" name="oldImage" />
                                                </td>
                            </tr>
                            <tr>
                              <th valign="top">Permament:</th>
                              <td colspan="2" ><textarea rows="" cols="" class="form-textarea" name="permamentAddress" ></textarea></td>
                              <th valign="top">Residential:</th>
                              <td colspan="2" ><textarea rows="" cols="" class="form-textarea" name="residentialAddress" ></textarea></td>
                            </tr>
                            <tr>
                              <th>&nbsp;</th>
                              <td valign="top"><input type="submit" value="Submit" class="form-submit"  name="studentEditButton" />
                                <input type="reset" value="" class="form-reset"  /></td>
                              <td></td>
                            </tr>
                          </table>
                          </form>
                          
                          <!-- end id-form  --></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                        <td></td>
                      </tr>
                    </table>
                    <div class="clear"></div>
                  </div></td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <!--  start table-content  -->
            <div>
              <center>
                <img id="loaderImage" />
              </center>
            </div>
            <div id="table-content"></div>
            <!--  end content-table  -->
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div id="stdDel" style="display:none;" ></div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
$(document).ready(function(){
	$("#course").change(function(){	
			document.getElementById('loaderImage').src='images/loading.gif';
			document.getElementById('table-content').innerHTML='';	
			var id = $(this).find(":selected").val();
			$('#branchResult').load('data.php?id='+id);
			$('#courseYear').load('data1.php?id='+id);
	});
});
function searchStudent(){
		document.getElementById('loaderImage').src='images/loading.gif';
		document.getElementById('table-content').innerHTML='';
		var c = document.getElementById("course");  
		var course = c.options[c.selectedIndex].value; 
				 
		var b = document.getElementById("branch");  
		var branch = b.options[b.selectedIndex].value; 
		
		var y = document.getElementById("cy");  
		var cy = y.options[y.selectedIndex].value; 	
		
		if(course >> 0)
		{
			if(branch >> 0 && cy >> 0 )
			{
				$('#table-content').load('data2.php?course='+course+'&branch='+branch+'&cy='+cy);	
			}
		}
		else
		{
			document.getElementById('loaderImage').src='images/psc.gif';
		}								
}
function edit(id, course, branch, courseYear, studentName, fatherName, motherName, rollNo, dob, mobile, email, sex, maritalStatus, category, nationality, residentialAddress, permamentAddress, hostler, studentImage, courseId, branchId, yearId){
		$("#editForm").css({
		"display": "table-row"});	
		window.scrollTo(0,400);	
	document.studentEditForm.id.value=id;
	document.studentEditForm.studentName.value=studentName;
	document.studentEditForm.course.value=course;
	document.studentEditForm.branch.value=branch;
	document.studentEditForm.courseYear.value=courseYear;
	document.studentEditForm.fatherName.value=fatherName;
	document.studentEditForm.motherName.value=motherName;
	document.studentEditForm.rollNo.value=rollNo;
	document.studentEditForm.dob.value=dob;
	document.studentEditForm.mobile.value=mobile;
	document.studentEditForm.email.value=email;
	document.studentEditForm.sex.value=sex;
	document.studentEditForm.maritalStatus.value=maritalStatus;
	document.studentEditForm.category.value=category;
	document.studentEditForm.nationality.value=nationality;
	document.studentEditForm.residentialAddress.value=residentialAddress;
	document.studentEditForm.permamentAddress.value=permamentAddress;
	document.studentEditForm.hostler.value=hostler;
	document.studentEditForm.oldImage.value=studentImage;	
	document.studentEditForm.courseId.value=courseId;
	document.studentEditForm.branchId.value=branchId;
	document.studentEditForm.yearId.value=yearId;
}
function closeEditForm(){
		$("#editForm").css({
		"display": "none"});	
}
function deleteStudent(id){
var r=confirm("Are You Sure?");
if (r==true)
  {
	  	$('#stdDel').load('data6.php?id='+id);
	  	$("#std_tr"+id).css({
		"display": "none"});
  }
}
</script>
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"dob",
			dateFormat:"%Y-%m-%d"
		});
	};
</script>
<?php if(isset($_POST['studentEditButton'])){ ?>
<script type="text/javascript">
	document.getElementById('loaderImage').src='images/loading.gif';
	window.onload = $('#table-content').load('data2.php?course=<?php echo $updateData['0']; ?>&branch=<?php echo $updateData['1']; ?>&cy=<?php echo $updateData['2']; ?>');	
</script>
<?php } ?>
</body>
</html>