<?php
/*==================	LOGIN SECTION START's HERE		===================*/
function login($param, $conn) {
	$username = $param['username'];
	$password = $param['password'];
	$query = "SELECT * FROM admin WHERE username = '$username' AND password='$password' AND status='1' ";
		$result = mysql_query($query, $conn);
    	$row = mysql_num_rows($result);
    	$data = mysql_fetch_array($result); 			// RETURN ONLY FIRST MATCHING RESULT :- NO MULTIPLE RECORD OR ASSOCIATE ARRAY
    	if ($row == 1)
		{		
			$_SESSION['ID'] = $data['id'];	
			$_SESSION['TYPE'] = $data['type'];	
			$_SESSION['NAME'] = $data['name'];		
        	return true;
    	}
		else 
		{
        	return false;
    	}
}
function adminViewById($id, $conn) {
	$query	=	"select * from admin where id = '$id' ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function allAdmin($conn) {
	$query	=	"select * from admin";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function adminAdd($param, $conn) {
	$username = $param['username'];
	$password = $param['password'];
	$name = $param['name'];
	$fatherName = $param['fatherName'];
	$motherName = $param['motherName'];
	$email = $param['email'];
	$mobile = $param['mobile'];
	$dob = $param['dob'];
	$sex = $param['sex'];
	$nationality = $param['nationality'];
	$maritalStatus = $param['maritalStatus'];	
	$permamentAddress = $param['permamentAddress'];
	$residentialAddress = $param['residentialAddress'];
	
	$usernameResult = checkAdminUsername($username, $conn);
	if(!$usernameResult){
	$query	=	"insert into admin (username, password, name, fatherName, motherName, email, mobile, dob, sex, nationality, maritalStatus, permamentAddress, residentialAddress) values ( '$username', '$password', '$name', '$fatherName', '$motherName', '$email', '$mobile', '$dob', '$sex', '$nationality', '$maritalStatus', '$permamentAddress', '$residentialAddress'  ) ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_insert_id();	
	}
}

function updateAdmin($id, $param, $conn){
	$name = $param['name'];
	$fatherName = $param['fatherName'];
	$motherName = $param['motherName'];
	$email = $param['email'];
	$dob = $param['dob'];
	$nationality = $param['nationality'];
	$mobile = $param['mobile'];
	$sex = $param['sex'];
	$password = $param['password'];
	$maritalStatus = $param['maritalStatus'];
	$permamentAddress = $param['permamentAddress'];
	$residentialAddress = $param['residentialAddress'];
	$query	=	"update admin set name = '$name', fatherName='$fatherName', motherName ='$motherName', dob = '$dob',email='$email', nationality = '$nationality', mobile = '$mobile', sex='$sex', password = '$password', maritalStatus = '$maritalStatus', permamentAddress = '$permamentAddress' , residentialAddress = '$residentialAddress' where id = '$id' ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_affected_rows();	
		
}
function checkAdminUsername($username, $conn){
	$query	=	"select id from admin where username ='$username'  ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function deleteAdminById($id, $conn){
	$query	= "delete from admin where id = '$id' ";
	$executeQuery	=	mysql_query($query, $conn);
}
function facultyLogin($param, $conn) {
	$username = $param['username'];
	$password = $param['password'];
	$query = "SELECT * FROM faculty WHERE username = '$username' AND password='$password' ";
		$result = mysql_query($query, $conn);
    	$row = mysql_num_rows($result);
    	$data = mysql_fetch_array($result); 			// RETURN ONLY FIRST MATCHING RESULT :- NO MULTIPLE RECORD OR ASSOCIATE ARRAY
    	if ($row == 1)
		{		
			$_SESSION['ID'] = $data['id'];
			$_SESSION['NAME'] = $data['facultyName'];	
			$_SESSION['TYPE'] = "FACULTY";		
        	return true;
    	}
		else 
		{
        	return false;
    	}
}
function courseList($conn) {
	$query	=	"select * from course";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function branchList($courseId, $conn) {
	$query	=	"select * from branch where courseId='$courseId'";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function courseYearList($courseId, $conn) {
	$query	=	"select * from course_year where courseId='$courseId'";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function sectionList($conn) {
	$query	=	"select * from section";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function studentAdd($param, $file, $conn) {
	$course = $param['course'];
	$branch = $param['branch'];
	$courseYear = $param['courseYear'];
	$studentName = $param['studentName'];
	$fatherName = $param['fatherName'];
	$motherName = $param['motherName'];
	$rollNo = $param['rollNo'];
	$dob = $param['dob'];
	$nationality = $param['nationality'];
	$mobile = $param['mobile'];
	$email = $param['email'];
	$sex = $param['sex'];
	$maritalStatus = $param['maritalStatus'];
	$category = $param['category'];
	$hostler = $param['hostler'];	
	$permamentAddress = $param['permamentAddress'];
	$residentialAddress = $param['residentialAddress'];
	
	$studentImageDir='';
		if (strlen($file['studentImage']['name'])>0)
		{
			$timestamp = time();
			$studentImageDir = "UPLOADS/STUDENT_IMAGE/";
			$studentImageDir = $studentImageDir .'SI'. $timestamp . ($file['studentImage']['name']);
			$uplode = move_uploaded_file($file['studentImage']['tmp_name'], $studentImageDir);
	    }	
	 $query	=	"insert into student(course, branch, courseYear, studentName, fatherName, motherName, rollNo, dob, nationality, mobile, email, sex, maritalStatus, category, hostler, studentImage, permamentAddress, residentialAddress) values ('$course', '$branch', '$courseYear', '$studentName', '$fatherName', '$motherName', '$rollNo', '$dob', '$nationality', '$mobile', '$email','$sex', '$maritalStatus', '$category', '$hostler', '$studentImageDir','$permamentAddress', '$residentialAddress') ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_insert_id();
}
function studentLogin($sid, $username, $conn) {	
	$query	=	"insert into student_login(sid, username, password, status) values ('$sid', '$username', 'ashoka@123', '1') ";
	$executeQuery	=	mysql_query($query, $conn);
	$id = mysql_insert_id();
	return $id;
}
function updateStudent($param, $file, $conn) {

	$id = $param['id'];
	$courseId = $param['courseId'];
	$branchId = $param['branchId'];
	$yearId = $param['yearId'];
	$studentName = $param['studentName'];
	$fatherName = $param['fatherName'];
	$motherName = $param['motherName'];
	$rollNo = $param['rollNo'];
	$dob = $param['dob'];
	$nationality = $param['nationality'];
	$mobile = $param['mobile'];
	$email = $param['email'];
	$sex = $param['sex'];
	$maritalStatus = $param['maritalStatus'];
	$category = $param['category'];
	$hostler = $param['hostler'];	
	$permamentAddress = $param['permamentAddress'];
	$residentialAddress = $param['residentialAddress'];
	$studentImageDir=$param['oldImage'];
	
		if (strlen($file['studentImage']['name'])>0)
		{
			$studentImageDir = "";
			$timestamp = time();
			$studentImageDir = "UPLOADS/STUDENT_IMAGE/";
			$imgExt = explode('.', $_FILES['studentImage']['name']);
			$studentImageDir = $studentImageDir.$id.$timestamp .'.'.$imgExt['1'];
			$uplode = move_uploaded_file($file['studentImage']['tmp_name'], $studentImageDir);
			is_file($param['oldImage'])?unlink($param['oldImage']):'';
	    }	
	$query	=	"update student set studentName='$studentName', fatherName='$fatherName', motherName='$motherName', rollNo='$rollNo', dob='$dob', nationality='$nationality', mobile='$mobile', email='$email', sex='$sex', maritalStatus='$maritalStatus', category='$category', hostler='$hostler', studentImage='$studentImageDir', permamentAddress='$permamentAddress', residentialAddress='$residentialAddress' where id = '$id'";
	$executeQuery	=	mysql_query($query, $conn);
	$data=array($courseId, $branchId, $yearId);
	return $data;
}
function deleteStudentById($id, $conn){
	$query = "select studentImage from student where id='$id'";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	is_file($result[0]['studentImage'])?unlink($result[0]['studentImage']):'';
	
	$query2	= "delete from student_login where sid = '$id' ";
	$executeQuery2	=	mysql_query($query2, $conn);
	$query1	= "delete from student where id = '$id' ";
	$executeQuery1	=	mysql_query($query1, $conn);
}
function searchStudentList($course, $branch, $courseYear, $conn){
	 $query	=	"select *, std.id, std.course as courseId, std.branch as branchId, std.courseYear as yearId from student std INNER JOIN branch b on std.branch = b.id INNER JOIN course c on std.course = c.id INNER JOIN course_year cy on std.courseYear = cy.id  where std.course='$course' and std.branch='$branch' and std.courseYear = '$courseYear' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function searchStudentLogin($course, $branch, $courseYear, $conn){
	$query	=	"select std.studentName, std.rollNo, sl.username,  sl.password, sl.id from student std INNER JOIN student_login sl on sl.sid = std.id  where std.course='$course' and std.branch='$branch' and std.courseYear = '$courseYear' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function checkUsername($username, $conn){
	$query	=	"select id from faculty where username ='$username'  ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function increaseTotalClass($param, $conn){
	//echo "<pre>"; print_r($param); die;
	$course = $param['course'];
	$branch = $param['branch'];
	$year = $param['year'];
	$faculty = $param['faculty'];
	$subject = $param['subject'];
	
	$query	=	"select totalClass, id from assign_faculty where courseId='$course' AND branchID='$branch' AND yearId='$year' AND subjectCode='$subject'  ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	$totalClass = $result[0]['totalClass']+1;
	$id =  $result[0]['id'];
	
	$query1= "update assign_faculty set totalClass='$totalClass' where id = '$id' ";
	$executeQuery1	=	mysql_query($query1, $conn);
	return mysql_affected_rows();
}

function totalClass($course, $branch, $year, $subject, $faculty, $conn){
	$query	=	"select totalClass from assign_faculty where courseId='$course' AND branchID='$branch' AND yearId='$year' AND subjectCode='$subject' AND facultyId='$faculty' ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	$totalClass = (isset($result[0]['totalClass']))?$result[0]['totalClass']:0;
	return $totalClass;
}

function takeAttendance($param, $conn){
	$caArray=$param['ca'];
	$idArray = $param['id'];
	$subject = $param['subject'];
	$sn=0;
	foreach($caArray as $caArrayVal)
	{
		$ca = $caArray[$sn];
		$id = $idArray[$sn];
		$sn++;
	$query	="update student set sub".$subject."= '$ca' where id='$id' ";
	$executeQuery	=	mysql_query($query, $conn);		
	}
	return mysql_affected_rows();
}
function facultyAdd($param, $file, $conn) {
	$facultyName = $param['facultyName'];
	$fatherName = $param['fatherName'];
	$motherName = $param['motherName'];
	$experience = $param['experience'];
	$dob = $param['dob'];
	$nationality = $param['nationality'];
	$mobile = $param['mobile'];
	$email = $param['email'];
	$sex = $param['sex'];
	$maritalStatus = $param['maritalStatus'];	
	$permamentAddress = $param['permamentAddress'];
	$residentialAddress = $param['residentialAddress'];
	
	$username = $param['username'];
	$password = $param['password'];
	
	$usernameResult = checkUsername($username, $conn);
	if(!$usernameResult){
	$facultyImageDir='';
		if (strlen($file['facultyImage']['name'])>0)
		{
			$timestamp = time();
			$facultyImageDir = "UPLOADS/FACULTY_IMAGE/";
			$facultyImageDir = $facultyImageDir .'FI'. $timestamp . ($file['facultyImage']['name']);
			$uplode = move_uploaded_file($file['facultyImage']['tmp_name'], $facultyImageDir);
	    }	
	$query	=	"insert into faculty(facultyName, fatherName, motherName, experience, dob, nationality, mobile, email, sex, maritalStatus, facultyImage, permamentAddress, residentialAddress, username, password) values ('$facultyName', '$fatherName', '$motherName', '$experience', '$dob', '$nationality', '$mobile', '$email', '$sex', '$maritalStatus', '$facultyImageDir', '$permamentAddress', '$residentialAddress', '$username', '$password')  ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_insert_id();	
	}
}
function updateFaculty($param, $file, $conn) {
	$id = $param['id'];
	$password = $param['password'];
	$facultyName = $param['facultyName'];
	$fatherName = $param['fatherName'];
	$motherName = $param['motherName'];
	$experience = $param['experience'];
	$dob = $param['dob'];
	$nationality = $param['nationality'];
	$mobile = $param['mobile'];
	$email = $param['email'];
	$sex = $param['sex'];
	$maritalStatus = $param['maritalStatus'];	
	$permamentAddress = $param['permamentAddress'];
	$residentialAddress = $param['residentialAddress'];	
	$facultyImageDir = $param['OldFacultyImage'];
	
	
		if (strlen($file['facultyImage']['name'])>0)
		{
			$facultyImageDir='';
			$timestamp = time();
			$facultyImageDir = "UPLOADS/FACULTY_IMAGE/";
			$facultyImageDir = $facultyImageDir .'FI'. $timestamp . ($file['facultyImage']['name']);
			$uplode = move_uploaded_file($file['facultyImage']['tmp_name'], $facultyImageDir);
			is_file($param['OldFacultyImage'])?unlink($param['OldFacultyImage']):'';
	    }	
$query	=	"update faculty set facultyName='$facultyName', fatherName='$fatherName', motherName='$motherName', experience='$experience', dob='$dob', nationality='$nationality', mobile='$mobile', email='$email', sex='$sex', maritalStatus='$maritalStatus', facultyImage='$facultyImageDir', permamentAddress='$permamentAddress', residentialAddress='$residentialAddress', password = '$password' where id ='$id' ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_affected_rows();	
}
function facultyList($conn){
	 $query	=	"select * from faculty ORDER BY facultyName";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function deleteFacultyById($id, $conn){
	$query = "select facultyImage from faculty where id='$id'";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	is_file($result[0]['facultyImage'])?unlink($result[0]['facultyImage']):'';
	
	$query1	= "delete from faculty where id = '$id' ";
	$executeQuery1	=	mysql_query($query1, $conn);
}
function facultyViewById($id, $conn)
{
	$query	=	"select * from faculty where id= '$id' ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;		
}
function subjectList($conn){
	 $query	=	"select * from total_subject";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function assignFacultyList($conn){
	$query	=	"select *, af.id from assign_faculty af 
	INNER JOIN faculty f on af.facultyId = f.id 
	INNER JOIN total_subject ts on af.subjectName = ts.id 
	INNER JOIN course_year cy on af.yearId = cy.id 
	INNER JOIN branch b on af.branchId = b.id 
	INNER JOIN course c on af.courseId = c.id
	"; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function deleteAssignFacultyById($id, $conn){	
	$query	= "select * from assign_faculty where id = '$id' ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	$course = $result[0]['courseId'];
	$branch = $result[0]['branchId'];
	$courseYear = $result[0]['yearId'];	
	
		
	 $query	= "update student set sub".$result[0]['subjectCode']."='0' where course='$course' AND branch = '$branch' AND courseYear='$courseYear' ";
	$executeQuery	=	mysql_query($query, $conn);

	
	$query1	= "delete from assign_faculty where id = '$id' ";
	$executeQuery1	=	mysql_query($query1, $conn);
}
function studentListByFacultyId($conn, $fid){
	$query	=	"select * from assign_faculty af 

	INNER JOIN total_subject ts on af.subjectName = ts.id 
	INNER JOIN course_year cy on af.yearId = cy.id 
	INNER JOIN branch b on af.branchId = b.id 
	INNER JOIN course c on af.courseId = c.id 
	where af.facultyId = '$fid' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function assignFacultyAdd($param, $conn) {
	
	$courseId = $param['course'];
	$branchId = $param['branch'];
	$yearId = $param['courseYear'];
	$subjectCode = $param['subjectCode'];
	$subjectName = $param['subjectName'];
	$facultyId = $param['faculty'];
	
	$query1	= "select id from assign_faculty where courseId='$courseId' AND branchId='$branchId' AND yearId='$yearId' AND subjectCode='$subjectCode' AND  facultyId= '$facultyId' AND  subjectName= '$subjectName' ";
	$executeQuery1 =	mysql_query($query1, $conn);
	$result1 = fetch_results($executeQuery1);
	
	$query2	= "select id from assign_faculty where courseId='$courseId' AND branchId='$branchId' AND yearId='$yearId' AND subjectName= '$subjectName' ";
	$executeQuery2 =	mysql_query($query2, $conn);
	$result2 = fetch_results($executeQuery2);
	if((!$result1) AND (!$result2))
	{	
	$query	= "insert into assign_faculty(courseId, branchId, yearId, subjectCode, subjectName, facultyId) values ('$courseId', '$branchId', '$yearId', '$subjectCode','$subjectName', '$facultyId') ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_affected_rows();	
	}
	else
	{
		return false;
	}
}
function finalReport($course, $branch, $courseYear, $conn){		
	$query = "select * from student where course ='$course' AND  branch='$branch' AND courseYear='$courseYear'  ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;				
}
function subjectName($course, $branch, $courseYear, $subject, $conn){
	$query = "select * from assign_faculty af INNER JOIN total_subject ts on af.subjectName = ts.id where af.courseId ='$course' AND  af.branchId='$branch' AND af.yearId='$courseYear' AND af.subjectCode = '$subject'  ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	if($result)
	{
	return $result[0]['subjectName'];
	}
	else return 0;
}

function subjectTotal($course, $branch, $courseYear, $subject, $conn){
	$query = "select totalClass from assign_faculty af INNER JOIN total_subject ts on af.subjectName = ts.id where af.courseId ='$course' AND  af.branchId='$branch' AND af.yearId='$courseYear' AND af.subjectCode = '$subject'  ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
 	if($result)
	{
	return $result[0]['totalClass'];
	}
	else return 0;
}
function assignmentAdd($param, $file, $conn) {
	$course = $param['course'];
	$branch = $param['branch'];
	$year = $param['courseYear'];
	$topic = $param['topic'];
	$lastDate = $param['lastDate'];
	$faculty = $param['faculty'];
	$date = $param['date'];
	
	$assignmentDir='';
		if (strlen($file['assignment']['name'])>0)
		{
			$timestamp = time();
			$assignmentDir = "UPLOADS/ASSIGNMENT/";
			$assignmentDir = $assignmentDir .'FI'. $timestamp . ($file['assignment']['name']);
			$uplode = move_uploaded_file($file['assignment']['tmp_name'], $assignmentDir);
	    }	
	$query	=	"insert into assignment( course, branch, year, assignment, faculty, lastDate, date, topic ) value('$course', '$branch', '$year', '$assignmentDir', '$faculty', '$lastDate', '$date', '$topic') ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_insert_id();	
}
function assignmentList($conn){
	$query	=	"select a.course,a.branch,a.assignment,b.branch, c.course , f.facultyName, cy.courseYear, a.topic,a.date,a.lastDate, a.faculty as facultyId, a.id
	from assignment a 
	INNER JOIN faculty f on a.faculty = f.id 
	INNER JOIN branch b on a.branch = b.id 
	INNER JOIN course c on a.course= c.id 
	INNER JOIN course_year cy on a.year = cy.id "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function deleteAssignment($id, $conn){	
	$query	=	"select assignment from assignment where id= '$id' ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	is_file($result[0]['assignment'])?unlink($result[0]['assignment']):'';
	$query1	= "delete from assignment where id = '$id' ";
	$executeQuery1	=	mysql_query($query1, $conn);
}
function noticeBoardAdd($param, $conn) {
	$heading = $param['heading'];
	$detail = mysql_real_escape_string( $param['detail']);
	$admin = $param['admin'];
	$faculty = $param['faculty'];
	$student = $param['student'];
	$author = $param['id'];
	
	$query	=	"insert into notice_board (heading, detail, admin, faculty, student, author) values ( '$heading', '$detail', '$admin', '$faculty', '$student' ,'$author') ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_insert_id();	
}
function noticeBoardList($conn){
	$query	=	"select * from notice_board order by id DESC "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	//echo "<pre>"; print_r($result); die;
	return $result;
}
function noticeBoardListById($id, $conn){
	$query	=	"select * from notice_board where author = '$id' order by id DESC "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function noticeBoardListForAdmin($conn){
	$query	=	"select * from notice_board where admin = 1 order by id DESC "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function noticeBoardListForFaculty($conn){
	$query	=	"select * from notice_board where faculty = 1 order by id DESC "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function deleteNoticeBoardNews($id, $conn){	
	$query1	= "delete from notice_board where id = '$id' ";
	$executeQuery1	=	mysql_query($query1, $conn);
}
function courseName($id, $conn){
	$query	=	"select * from course where id = '$id' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result[0]['course'];
}
function branchName($id, $conn){
	$query	=	"select * from branch where id = '$id' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result[0]['branch'];
}
function yearName($id, $conn){
	$query	=	"select * from course_year where id = '$id' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result[0]['courseYear'];
}









/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!		STUDENT LOGIN	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
function loginByStudent($param, $conn){
	$username = $param['username'];
	$password = $param['password'];
		$query = "SELECT * FROM student_login WHERE username = '$username' AND password='$password' AND status='1' ";
		$result = mysql_query($query, $conn);
    	$row = mysql_num_rows($result);
    	$data = mysql_fetch_array($result); 			// RETURN ONLY FIRST MATCHING RESULT :- NO MULTIPLE RECORD OR ASSOCIATE ARRAY
    	if ($row == 1)
		{		
			$_SESSION['SID'] = $data['sid'];	
        	return true;
    	}
		else 
		{
        	return false;
    	}
}
function noticeBoardListForStudent($conn){
	$query	=	"select * from notice_board where student = 1 order by id DESC "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function assignmentForStudent($id, $conn){
	$query	=	"select course, branch, courseYear from student where id = '$id'"; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
		$course = $result[0]['course'];
		$branch = $result[0]['branch'];
		$year   = $result[0]['courseYear'];
	$query1	=	"select * from assignment where course = '$course' AND branch = '$branch' AND year = '$year' "; 
	$executeQuery1	=	mysql_query($query1, $conn);
	$result1 = fetch_results($executeQuery1);
	
	
	$query2	=	"select * from assignment a 
	INNER JOIN faculty f on a.faculty = f.id 
	INNER JOIN branch b on a.branch = b.id 
	INNER JOIN course c on a.course= c.id 
	INNER JOIN course_year cy on a.year = cy.id
	where a.course = '$course' AND a.branch = '$branch' AND a.year = '$year' "; 
	$executeQuery2	=	mysql_query($query2, $conn);
	$result2 = fetch_results($executeQuery2);
	//echo "<pre>"; print_r($result2); die;
	return $result2;
}
function attendanceForStudent($id, $conn){
	$query	=	"select sub1, sub2, sub3, sub4, sub5, sub6, sub7, sub8, course, branch, courseYear  from student where id = '$id' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function changePassword($sid, $param, $conn){
	$password = $param['oldPassword'];
	$newPassword = $param['newPassword'];
	$updateStatus = 0;
	
	$query	=	"select id from student_login where sid = '$sid' and password = '$password' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	$stt = isset($result[0]['id']);
	if(isset($result[0]['id']))
	{
			$query1	=	"update student_login set password = '$newPassword'  where id = '$stt' "; 
			$executeQuery1	=	mysql_query($query1, $conn);
			$updateStatus = 1;
	}
	return $updateStatus;
}
function studentPasswordByAdmin($id, $password, $conn){
			$query1	=	"update student_login set password = '$password'  where id = '$id' "; 
			$executeQuery1	=	mysql_query($query1, $conn);
			return mysql_affected_rows();
}
function studentNameById($id, $conn){
	$query	=	"select studentName from student where id = '$id' "; 
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result[0]['studentName'];
}

function getSubjectByBranch($course, $branch, $year, $conn){
	$query	=	"select * from total_subject where course = '$course' AND branch = '$branch' AND year = '$year' ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function addSubject($param, $conn) {
	$course = $param['course'];
	$branch = $param['branch'];
	$courseYear = $param['courseYear'];
	$subjectName = $param['subjectName'];	
	$query	=	"insert into total_subject (course, branch, year, subjectName) values ('$course','$branch','$courseYear','$subjectName') ";
	$executeQuery	=	mysql_query($query, $conn);
	return mysql_insert_id();	
}
function totalSubjectList($conn){
	 $query	=	"select *,ts.id as id from total_subject ts INNER JOIN branch b on ts.branch = b.id	INNER JOIN course c on ts.course= c.id INNER JOIN course_year cy on ts.year = cy.id ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	return $result;
}
function deleteSubject($id, $conn){
	$query	=	"select * from assign_faculty where subjectName = '$id' ";
	$executeQuery	=	mysql_query($query, $conn);
	$result = fetch_results($executeQuery);
	if($result)
	{
		$data = array("error"=>"1", "msg"=>"Unable to delete assigned subject [delete from assign faculty  first]");		
		return $data;
		break;
	}
	else
	{
		$query	= "delete from total_subject where id = '$id' ";
		$executeQuery	=	mysql_query($query, $conn);
		$data = array("error"=>"0", "msg"=>"Subject deleted");		
		return $data;
		break;
		
	}
}
?>



