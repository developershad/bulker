<?php include('config.php'); 
	$courseData  = courseList($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#product-table .minwidth-1 {
	min-width: 15px;
}
#product-table td {
    border: 1px solid #D2D2D2;
    padding: 0;
}
#product-table th {
    height: 50px;
    text-align: center;
}
.table-header-repeat {
    background: url("images/shared/top_bg.jpg") repeat scroll 0 0 rgba(0, 0, 0, 0);
    border: medium none;
    font-size: 0;
    line-height: 0;
    padding: 0;
}
</style>
</head>
<body >
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    <div id="page-heading">
      <h1>Final Report</h1>
    </div>
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table"  >
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner">
            <table border="0" width="100%" cellpadding="" cellspacing="0">
              <tr>
                <td>
                
                </td>
              </tr>
              <tr valign="top">
                <td><!--  start step-holder -->
                  
                  <div id="step-holder">
                    <div class="step-no">1</div>
                    <div class="step-dark-left"><a href="" >Fill Detail</a></div>
                    <div class="step-dark-right">&nbsp;</div>
                    <div class="step-no-off">2</div>
                    <div class="step-light-left">Student List</div>
                    <div class="step-light-round">&nbsp;</div>
                    <div class="clear"></div>
                  </div>                  
                  <!--  end step-holder -->                   
                  <!-- start id-form -->                  
                  <form name="studentListForm" id="studentListForm" action="" method="post" >
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">
                      <tr >
                        <th valign="top">Course:</th>
                        <td><select name="course" id="course" class="selectStyle">
                            <option value="">Select</option>
                            <?php foreach($courseData as $courseVal){ ?>
                            <option value="<?php echo $courseVal['id']; ?>"><?php echo $courseVal['course']; ?></option>
                            <?php } ?>
                          </select></td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <th valign="top">Branch:</th>
                        <td><div id="branchResult" >
                            <select  name="branch" id="branch" class="selectStyle" disabled="disabled">
                              <option value="">Please Select Course</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr>
                        <th valign="top">Year:</th>
                        <td><div id="courseYear" >
                            <select name="courseYear" id="cy"  class="selectStyle" disabled="disabled" >
                              <option value="">Please Select Course</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr>
                        <th>&nbsp;</th>
                        <td valign="top"><input type="button" value="Submit"  class="form-submit" onclick="searchStudent();" id="searchStudentButton"  />
                          <input type="reset" value="" class="form-reset"  /></td>
                        <td></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!-- end id-form  --> 
                  <!--  start related-activities -->
                                     
                  <!-- end related-act-bottom -->
                  </td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <!--  start table-content  -->
            <div>
              <center>
                <img id="loaderImage" />
              </center>
            </div>
            <div id="table-content"></div>
            <!--  end content-table  -->
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div id="stdDel" style="display:none;" ></div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
$(document).ready(function(){
	$("#course").change(function(){	
			document.getElementById('loaderImage').src='images/loading.gif';
			document.getElementById('table-content').innerHTML='';	
			var id = $(this).find(":selected").val();
			$('#branchResult').load('data.php?id='+id);
			$('#courseYear').load('data1.php?id='+id);
	});
});
function searchStudent(){
		document.getElementById('loaderImage').src='images/loading.gif';
		document.getElementById('table-content').innerHTML='';
		var c = document.getElementById("course");  
		var course = c.options[c.selectedIndex].value; 
				 
		var b = document.getElementById("branch");  
		var branch = b.options[b.selectedIndex].value; 
		
		var y = document.getElementById("cy");  
		var cy = y.options[y.selectedIndex].value; 	
		
		if(course >> 0)
		{
			if(branch >> 0 && cy >> 0 )
			{
				$('#table-content').load('data10.php?course='+course+'&branch='+branch+'&cy='+cy);	
			}
		}
		else
		{
			document.getElementById('loaderImage').src='images/psc.gif';
		}								
}
</script>
</body>
</html>