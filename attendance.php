<?php include('config.php'); 
$fid=0;
if ($_SESSION['TYPE'] == 'FACULTY' )
{
	$fid=$_SESSION['ID'];	
}
$studentCategData = studentListByFacultyId($conn, $fid);
if(isset($_POST['attendanceButton']))
{
	$attendanceData = takeAttendance($_POST, $conn);
	$increaseTotalClass = increaseTotalClass($_POST, $conn);
}
//echo "<pre>"; print_r($studentCategData); die;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#product-table .minwidth-1 {
    min-width: 20px;
}

</style>
</head>
<body> 
<?php include('common/nav.php') ?>
 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Student List</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		<table border="0" width="100%" cellpadding="" cellspacing="0">
          <tr>
			<td>
            <?php if(isset($_POST['attendanceButton'])){ ?>
              <div id="message-yellow">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="yellow-left">Attendance Saved Sucessfully</td>
					<td class="yellow-right"><a class="close-yellow"><img src="images/table/icon_close_yellow.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
            <?php } ?>    
                </td>
					
			  </tr>
            <tr valign="top">
                <td>
              <!--  start step-holder -->
              <div id="step-holder">
              <div class="step-dark-right">&nbsp;</div>
                <div class="step-no">1</div>
                <div class="step-dark-left"><a href="">Select Batch</a></div>
                <div class="step-dark-round">&nbsp;</div>
               <!-- <div class="step-no-off">2</div>
                <div class="step-light-left">Student List</div>
                <div class="step-light-round">&nbsp;</div>-->
                <div class="clear"></div>
              </div>
              <!--  end step-holder --> 
              
              <!-- start id-form -->
<?php if($studentCategData){ ?>              
              <table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" >
				<tr>
					<th class="table-header-repeat line-left minwidth-1"><a>SN</a>	</th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Course</a>	</th>
					<th class="table-header-repeat line-left minwidth-1"><a >Branch</a></th>
					<th class="table-header-repeat line-left"><a >Year</a></th>
					<th class="table-header-repeat line-left"><a >Subject</a></th>
					<th class="table-header-repeat line-left"><a >Student List</a></th>
				</tr>
<?php $sn=0; foreach ($studentCategData as $studentCategVal ){ $sn++; ?>   					             
					<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
					<td style="width:20px;"><?php echo $sn; ?></td>
                    <td><?php echo $studentCategVal['course']  ?></td>
                    <td><?php echo $studentCategVal['branch'] ?></td>
                    <td><?php echo $studentCategVal['courseYear'] ?></td>
                    <td><?php echo $studentCategVal['subjectName'] ?></td>
                    <td><a href="#" title="Take Attendance" class="icon-1 info-tooltip" onclick='showStudent(
                    "<?php echo $studentCategVal['courseId']; ?>",
                    "<?php echo $studentCategVal['branchId']; ?>",
                    "<?php echo $studentCategVal['yearId']; ?>",
                    "<?php echo $studentCategVal['subjectCode']; ?>",
                    "<?php echo $fid; ?>");' ></a>
                    </td>
				</tr>
<?php } ?>           				     				
				</table>
<?php } else echo "<h1 style='text-align:center;' > No Batch Is Assigned to you </h1>"; ?>                
              <!-- end id-form  -->
          
              </td>                       
            </tr>
            <tr>
              <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
              <td></td>
            </tr>
          </table>
			<!--  start table-content  -->
            <div><center><img id="loaderImage" /><center></div>
			<div id="table-content"></div>
			<!--  end content-table  -->
			<div class="clear"></div> 
		</div>
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
	<!--  start footer-left -->
	<?php include('common/footer.php') ?>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
<script>
function showStudent(course, branch, year, subject, fid){
		document.getElementById('loaderImage').src='images/loading.gif';
		document.getElementById('table-content').innerHTML='';
		$('#table-content').load('data3.php?course='+course+'&branch='+branch+'&cy='+year+'&subject='+subject+'&fid='+fid);							
}
function present(id){
	var ca;
	//alert(id);	
	if(document.getElementById('chkbx'+id).checked)
	{
		document.getElementById('id'+id).value=id;
		ca= parseInt(document.getElementById('ca'+id).value);
		document.getElementById('ca'+id).value=ca+1;
	}
	else
	{
		document.getElementById('id'+id).value=id;
		ca= parseInt(document.getElementById('ca'+id).value);
		document.getElementById('ca'+id).value=ca-1;	
	}
}
</script> 
</body>
</html>