<?php include('config.php'); 
	if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		if($_SESSION['TYPE'] != "ADMIN")
		{
			header("location:home.php");	
		}
	}
	if(isset($_POST['assignFacultyButton']))
	{
		$addData = assignFacultyAdd($_POST, $conn);		
	}
	$courseData  = courseList($conn); 
	$assignFacultyListData = assignFacultyList($conn);
	$totalSubject = subjectList($conn)
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<link rel="stylesheet" href="chosen/chosen.css">
<script src="chosen/chosen.jquery.js" type="text/javascript"></script>

<style>
#product-table .minwidth-1 {
	min-width: 20px;
}
</style>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    <div id="page-heading">
      <h1>Assign Faculty</h1>
    </div>
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner">
            <table border="0" width="100%" cellpadding="" cellspacing="0">
              <tr>
                <td><?php if(isset($addData)){
				if ($addData){ ?>
                  <div id="message-green">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="green-left">Faculty Assinged Successfully</td>
                        <td class="green-right"><a class="close-yellow"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
                      </tr>
                    </table>
                  </div>
                  <?php }else{ ?>
                  <div id="message-red">
                    <table border="0" width="100%" cellpadding="0" cellspacing="0">
                      <tr>
                        <td class="red-left">Error in submission</td>
                        <td class="red-right"><a class="close-yellow"><img src="images/table/icon_close_red.gif"   alt="" /></a></td>
                      </tr>
                    </table>
                  </div>
                  <?php } }?></td>
              </tr>
              <tr valign="top">
                <td><!--  start step-holder -->
                  
                  <div id="step-holder">
                    <div class="step-no">1</div>
                    <div class="step-dark-left"><a href="">Fill Detail</a></div>
                    <div class="step-dark-right">&nbsp;</div>
                    <div class="step-no-off">2</div>
                    <div class="step-light-left">Student List</div>
                    <div class="step-light-round">&nbsp;</div>
                    <div class="clear"></div>
                  </div>
                  
                  <!--  end step-holder --> 
                  
                  <!-- start id-form -->
                  
                  <form name="assignFacultyForm" id="assignFacultyForm" action="" method="post" >
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form">
                      <tr >
                        <th valign="top">Course:</th>
                        <td><select name="course" id="course"  class="selectStyle">
                            <option value="">Select</option>
                            <?php foreach($courseData as $courseVal){ ?>
                            <option value="<?php echo $courseVal['id']; ?>"><?php echo $courseVal['course']; ?></option>
                            <?php } ?>
                          </select></td>
                      </tr>
                      <tr id="branch_tr" style="display:none;" >
                        <th valign="top">Branch:</th>
                        <td><div id="branchResult">
                            <select disabled="disabled" class="selectStyle" >
                              <option>Select</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr id="year_tr" style="display:none;" >
                        <th valign="top">Year:</th>
                        <td><div id="yearResult">
                            <select disabled="disabled" class="selectStyle" >
                              <option>Select</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr id="subject_tr" style="display:none;" >
                        <th valign="top">Subject Code:</th>
                        <td><select class="selectStyle" name="subjectCode" id="subjectCode"  >
                            <option value="" >Select</option>
                            <option value="1" >Subject 1</option>
                            <option value="2" >Subject 2</option>
                            <option value="3" >Subject 3</option>
                            <option value="4" >Subject 4</option>
                            <option value="5" >Subject 5</option>
                            <option value="6" >Subject 6</option>
                            <option value="7" >Subject 7</option>
                            <option value="8" >Subject 8</option>
                            <option value="9" >Subject 9</option>
                            <option value="10" >Subject 10</option>
                            <option value="11" >Subject 11</option>
                            <option value="12" >Subject 12</option>
                            <option value="13" >Subject 13</option>
                            <option value="14" >Subject 14</option>                            
                          </select></td>
                      </tr>
                      <tr id="subjectName_tr" style="display:none;" >
                        <th valign="top">Subject Name:</th>
                        <td><div id="subjectNameResult">
                            <select disabled="disabled" class="selectStyle"  >
                              <option>Select</option>
                            </select>
                          </div></td>
                      </tr>
                      <tr id="faculty_tr" style="display:none;" >
                        <th valign="top">Faculty:</th>
                        <td><select class="selectStyle" id="faculty" name="faculty" >
                            <option value="" >Select</option>
                            <?php $facultyData = facultyList($conn);
					 		foreach($facultyData as $facultyVal){
					  ?>
                            <option value="<?php echo $facultyVal['id'] ?>" ><?php echo $facultyVal['facultyName'] ?></option>
                            <?php } ?>
                          </select></td>
                      </tr>
                      <tr id="button_tr"  style="display:none;"  >
                        <td align="center"><input type="submit" value="Submit" class="form-submit" name="assignFacultyButton" style="margin-left:128%;" /></td>
                      </tr>
                    </table>
                  </form>
                  
                  <!--  start table-content  -->
                  
                  <div>
                    <center>
                    <img id="loaderImage" />
                    <center>
                  </div>
                  
                  
                  

                  <div id="table-content">
              <iframe src="data18.php" width="100%" height="630" scrolling="auto" frameborder="0"  ></iframe>
                </div>
                  
                  
                  
                  
                  <div class="clear"></div>
                  
                  <!--  end content-table-inner ............................................END  --> 
                  <!-- end id-form  --></td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <!--  start table-content  -->
            
            <div id="table-content"></div>
            <!--  end content-table  -->
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div id="afDel" style="display:none;" ></div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script>
$(document).ready(function(){
	 $("#course").change(function(){	
			document.getElementById('loaderImage').src='images/loading.gif';
			var id = $(this).find(":selected").val();
			$('#branchResult').load('data4.php?id='+id);
			$('#yearResult').load('data1.php?id='+id);			
			
		$("#branch_tr").css({
		"display": "table-row"});	

		$("#year_tr").css({
		"display": "table-row"});
		
		$("#subject_tr").css({
		"display": "table-row"});				
		
	});	
	 $("#subjectCode").change(function(){	
		$("#subjectName_tr").css({
		"display": "table-row"});
		
		$("#faculty_tr").css({
		"display": "table-row"});
	});			
	 $("#faculty").change(function(){	
		$("#button_tr").css({
		"display": "table-row"});
	});				
});
function deleteAssignFaculty(id){
var r=confirm("Are You Sure?");
if (r==true)
  {
	  	$('#afDel').load('data9.php?id='+id);
	  	$("#assignFaculty"+id).css({
		"display": "none"});
  }
}
//		JAVA SCRIPT MULTI SELECT
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
$("#subjectCode").change(function(){	
			
			var cy = document.getElementById('cy').value;
			var branch = document.getElementById('branch').value;
			var course = document.getElementById('course').value;
			
			$('#subjectNameResult').load('data17.php?course='+course+'&branch='+branch+'&year='+cy);
			
	});							
				
</script>


</body>
</html>