-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 15, 2014 at 01:48 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ashoka_software`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `dob` date NOT NULL DEFAULT '2014-01-01',
  `nationality` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `maritalStatus` varchar(255) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `residentialAddress` varchar(255) NOT NULL,
  `permamentAddress` varchar(255) NOT NULL,
  `status` varchar(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'ADMIN',
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `fatherName`, `motherName`, `email`, `dob`, `nationality`, `sex`, `maritalStatus`, `username`, `password`, `mobile`, `residentialAddress`, `permamentAddress`, `status`, `type`, `image`) VALUES
(1, 'AAAAAAAAAA', 'aaa', 'sss', 'ankit@shadowinfosystem.com', '2011-11-11', 'Indian', 'Male', 'Married', 'superadmin', '1', '9899996124', 'res', 'perm', '1', 'SUPERADMIN', '');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE IF NOT EXISTS `assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `assignment` varchar(255) NOT NULL,
  `faculty` int(11) NOT NULL,
  `lastDate` date NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `course`, `branch`, `year`, `topic`, `assignment`, `faculty`, `lastDate`, `date`) VALUES
(2, 1, 1, 1, 'cccccccccccc', 'UPLOADS/ASSIGNMENT/FI1389600094ASSIGNED WORK STATUS REPORT - Narender - Jun 2013.xlsx', 12, '2014-01-11', '2014-01-13'),
(4, 1, 1, 1, '', 'UPLOADS/ASSIGNMENT/FI1389610932Chrysanthemum.jpg', 11, '2014-01-09', '2014-01-13'),
(5, 0, 0, 0, '', '', 11, '0000-00-00', '2014-01-13'),
(6, 1, 0, 0, '', '', 11, '0000-00-00', '2014-01-13');

-- --------------------------------------------------------

--
-- Table structure for table `assign_faculty`
--

CREATE TABLE IF NOT EXISTS `assign_faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` varchar(255) NOT NULL,
  `branchId` varchar(255) NOT NULL,
  `yearId` varchar(255) NOT NULL,
  `subjectCode` varchar(255) NOT NULL,
  `subjectName` varchar(255) NOT NULL,
  `totalClass` int(50) NOT NULL,
  `facultyId` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `assign_faculty`
--

INSERT INTO `assign_faculty` (`id`, `courseId`, `branchId`, `yearId`, `subjectCode`, `subjectName`, `totalClass`, `facultyId`) VALUES
(6, '1', '1', '1', '1', '1000', 5, '11'),
(7, '1', '1', '1', '2', '1001', 2, '11'),
(8, '1', '1', '1', '3', '1002', 2, '11'),
(9, '1', '1', '1', '4', '1003', 3, '11');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseID` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `courseID`, `branch`) VALUES
(1, 1, 'CS'),
(2, 1, 'IT'),
(4, 1, 'EC'),
(5, 1, 'ME'),
(6, 2, 'Not Applicable');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `course`) VALUES
(1, 'B.tech'),
(2, 'MBA');

-- --------------------------------------------------------

--
-- Table structure for table `course_year`
--

CREATE TABLE IF NOT EXISTS `course_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseID` int(11) NOT NULL,
  `courseYear` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `course_year`
--

INSERT INTO `course_year` (`id`, `courseID`, `courseYear`) VALUES
(1, 1, '1st Year'),
(2, 1, '2nd Year'),
(3, 1, '3rd Year'),
(4, 1, '4thYear'),
(5, 2, '1st Year'),
(6, 2, '2nd Year');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facultyName` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `experience` int(11) NOT NULL,
  `dob` date NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `maritalStatus` varchar(255) NOT NULL,
  `facultyImage` varchar(255) NOT NULL,
  `permamentAddress` text NOT NULL,
  `residentialAddress` text NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `facultyName`, `fatherName`, `motherName`, `experience`, `dob`, `nationality`, `mobile`, `email`, `sex`, `maritalStatus`, `facultyImage`, `permamentAddress`, `residentialAddress`, `username`, `password`, `status`) VALUES
(11, 'Anil Kumar', 'aaa', 'ghgf', 11, '1965-01-29', 'Indian', '01230123012', 'anil@gmail.com', 'Male', 'Married', 'UPLOADS/FACULTY_IMAGE/FI1389612648Penguins.jpg', 'asdfghjk', 'ghjk', 'anil', 'anil', '1'),
(12, 'asd', 'hjh', 'jhj', 0, '2014-01-28', 'hjh', '01230123012', 'anil@gmail.com', 'Male', 'Unmarried', '', 'hjh', 'jh', 'hj', 'aaa', '1');

-- --------------------------------------------------------

--
-- Table structure for table `notice_board`
--

CREATE TABLE IF NOT EXISTS `notice_board` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading` varchar(255) NOT NULL,
  `detail` text NOT NULL,
  `admin` int(11) NOT NULL,
  `faculty` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `notice_board`
--

INSERT INTO `notice_board` (`id`, `heading`, `detail`, `admin`, `faculty`, `student`, `author`) VALUES
(35, 'first News', '<p>Testing</p>', 1, 1, 1, 1),
(56, 'Ashoka', '<p>sssss</p>', 1, 0, 0, 1),
(57, 'asd', '<p><img src="data:image/bmp;base64,iVBORw0KGgoAAAANSUhEUgAAAGAAAABgCAYAAADimHc4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAC4hSURBVHhe7X11XFXZ9/aMXYQKirRIiKQKqNhiiyAIUmIAdmMhKqCI3Yk5Y8c4BgYgIt0dBqCIQXfDjb3ftc69ly8zv5lBCR3n8/5xPrfOrfXs9aze5ydK6U//9uOnn35q17FHd9ku4mI63SX7je+pomzfe5Dqyq5iYkPxt//U7ueO//b/8He/718v/HadOokK9+9vJjd9qq+CiVHUQFvrPEUz0+SB8+cWSY4edVa4v7yZiOIA65/btev8I4LwrwagfefOvSSG6x1UXTivfJDd/Cq4rRhkv6BGdYFtibLVnHdKlubpffV09/bR1fHoKi6m+yNqwr8WgA7du0n3NzIMVrG1ypWbOumxjMH42yh49SUOdJDDwnrQgGKVudY5A0xnxQ2YPSseqGnC/9eAVrInfXSH7hpgYhyjMMsoUmLEsMNIQx26dukrbTD+JgpbzcGOJT9j6jM8B7QgA86LENPS3PTTzz+3/9FA+FdpwM/t23eT0B9xXMHEOFpxzuyXvTXU1zWmlXYdOwq369RRBJ/vLiVpgELvozPUHR/j8z+a8BkH4t/yo7v2ER8mPnTIDmUri0y5qZMfg3Gd3aFr177/9PtwxaPxbd+li/i/5X987e/47gCAEDugeymmpbFJGehEctTIM1169x78I9LJ1wr/u2sArNw+ospKC0QGqa4QVlCwBI9mX+devTRbU/gQQ/zcSUhIumO3bn0QbHzcHEG11Xu+iwaAIDoCvUiIa2tu6TdyxAlheTmTHjLS09riT4rIyxmIq6vN6z1QxVxiiPZyUYX+UxGItviu5nzmdwGgfdeu/ZDjheTlZoEGLOwkIqLSFkLp1KOHlNLMGVdlx472GGhm6gX3r/UbOmSV2CBVq47du0s0R2Ct/Z5vDgCu/p87dOghOlDZobeW5sZuEn1HYaqhpX8MPKjOnYSFZHtISo4Q7S83qbfqQAuFKZNPa8y3jR66YukHnVXLc3TXrCrSmDc3QlJPdz1oxkSgpb5tAfzX/JdvDgAIqmtHISEFURVluy7ivXW+5sc2PhcF101cXLNbnz7aksP0Ng00n/14wPSpFwaamz7W37KJDlu/tnL4pvX1IzZv4Ggvsn81eLFDmt661aW6a1YW4rl9B2sv66mkaAxe1HfNI31zADr37KnZS1PdsVvfPqObI/x2HTp0Ex2gMF1CZ8jqwUsc0ocsW/oehT1sw7pqNRvLAKkRw7YozzK6IzFk8Iq+2pqL1W1twlQtzH0QBHVb61DdNSsK8dwhy5dkiWuqL/zeRvmbAtChW1dJERUlO+EBChYQ3fb8WgA6CfWQ7guCRUErGRnewBUvpT98KxhZ225iYurg7cggnaFQe/ST0BORkzVAIStMnew52MEuFVZ/AWrHCKeN3OEbHeskdXXWfO1vaO3zvxkAQD1dxAdrOffRGeLeuVdPjS/9I0g1kJQTRU8G+DtSdY7ZUxD8xX66OuvwM5FC/uy24mPwssS6iYtp9JCS1BeWkzMQgtteykqm/ScaHMfPkBk7xqOnooLhl/6OtjrvmwCAghKSkzXuO3zYYRFY/V9q+EDwIv30dDYoTJtySc3WJlJ5lvFvQtJSo0G44kBFXdtKKN/yc78JAJjZFIPVDx7PF/N+u/btO2mamvwycuXyl0Nsbbw1jY3OdBES6gfgtdhj+pYCbuq72hwA9Hq6S0tNYrweqGB9idFD4euamhyzPXaYWuza8VFnpqFHz34Sak39mR/x9TYHoKOwsGJP1YGL++rp7AE27/YlQpLR1Vk2Yd2aN9rGM4+Iy8sN+5L3/KjntCkAwOFiPQeqLIbVv7Br3z76TeV4UDvENdTmy44bsw9TB1+iLT+q4AW/u00B6CQqMrC3tqaTqLLywqYEhd6M9Eh9l0FWc573VlExa+r8/8rrbQoAFk16a2ps7NCtm2RTAsPUwYBpU8730dJa1NS5/6XX2wwAzPf0gGQbUM/In9q16/RPQmvfqZOw4ozpv/bTHboWkmT9/ksCbuq/tBkAHbp3l5HQH34MPKDJTf0IXP0qprPudejSpXdT5/7XXm8zACDwmj1gtkkC2AHVfxIaRqyyY0e5y4wetfO/Jtwv+T9tB8AABSvJMSPPN9Wr01VKaoTsqJFuXXr1VP6rH/wzBF54fMmf+RHPaRMAsI1EDKpdUGgf/k9CAc+nU99hehukIJ38T9yP7mjDAWj8iIL+u9/cJgCg0e2rP+xYlyYA6AqrXslw+hV5gwmHOwkLy3+JYAVAfMm5P8I5bQJAZ1FRtX6jRnp2l5Ux+ishoBB/bt+uM9Zn1awtXvQeONC8fZfOvX4EgbX2b2wTADoICytBm8lmYXl5Y/zBKHCZgQP1NY2NLyjNmH5dcabhA2VrixQ1h4WFGg52RWCsI5RNjJ+qm5rcGmZsvElz9GgTKQWFfzTerS2I7/V5bQJAu27dpGTGj7ksP2rk7uFWlr9MXLb0kdaSxVV6a1ZTy8OHqIvXQ+ry8AFd9ssluujcWWJ7/CixOnqEmu7fRw1cttIBC+ZTOXPzYl0rq9+GG81cJdK79w/beNUUsG0CgJCMzPQhSxyKRjqupUNWLKMGW52p+YED9JC/Pw0tKSFpHA4ng8vlvoEjFY40+JWRlRXkRUEBvZqUVLfr/r0Pqy6cL1SxW0glzGZTeXOz7CHmZpfl1dRGNvWHfrTXWw2AbkJCPfUMDGaOspizZ+LG9XmTtm2hU7c5V9qfOlntGR5OI8vKK5NYLPKSyyWpXEJTCaEJ9fX1CXX19S8Jpa9BcgjEGwAnubq6Orm6puZFXl7dPl/f+vFbnNiK8+dRGfPZpapGM6+Ly8vr/WiCbnMvSEl/hP2aixcKLQ8d4Mw7eYLO3LWT7vP14STU1dW/hW9/RQgBAFiJLDYbAUhisTkpHC43hcPhxtfU1MJ9gkcii8WOr62rS2ZzGM14CxoSVlRcufXOnfLRGzdSjaVL6JjFiwNExMSk/wsgtIoGQM5fWW/+PL+pbq4UufxKYiLZ8cgLKCWfJNfW1uJqR6EH5+XnR1dWVeHjJDaHg7ex1dU10eXlFQAOAwwKHrSCJQAEn3sJWvGqrq7uYWYmtTh0kDV4xXKq62CfIq2spPujg9BiANp36yrVS0Nj4zSnzW89nj6ld9PTaXxdHd3/7Bl5nPWeFV1eUZkKFIM0g7cgUJIC2hAHgk+G1Z8KEkxmszl4vERhC0Cor2chGAgSaEgdaslreP+LwgK638+Pai9fRtUXLsjvo6j43QvrLVkELQKgXccOPTr3FNVWmTnjlv3p0/W3X72iWTyB0ntvM0hgYRGzqpFqQJgcFDYKH4WdUFtbBwaY4HOJKGygmpcgbDyQtqJAK5jVD49T4DykJjz/NZz3Hj7jXkYGGb9lC5W1seZCq+Gqlgjhe763RQB0ERcfIa6re3DWjh15RwID0ajSl/Bv0KD65+eT4OISCoJFAXJRwChsXMnRlZVVDBh8DRDcMtoAdJMCYIUWFhWFFhYWCeiLAQsPAAQ1B72oR5mZZKqrC5WzsaYi2lqnO3Xu3P17CrM5391sALABSkhJaZnsTMPYNb9cKgIXkhEOCJMvJC4JLS0lCSwWejwEQUABorBRC1DYzGO8LxAugsKcRwjahciysvI/A4AagQAkIXhgX4Lz8sjsvXuogo0VGWFt6YXeWHME8b3e02wA2nXu3LuXjs5xdUvLsKN+fnmvUKAoHDabESBvpYKA8T5fwKgdeDR+jOCgOxoDriefgsgb1CJ4HjUJAUPKYlxX5sDP5JIkeA5ByEBKq64mxjt3Epk55lRITfUQZk9Fe/X629TGv6nW3GwAcHxIYuRIz803b1YE5udXwqpkhM5fsbz7PCAYAFC4KLQ4oCDGCwJOZ4wvUA5yPt7COSS+vo6EFBWRsNJSGlFWSl7j5wpiB7QnaA/AZgjsRzKAF5STwzn2yCtf224BW0x/hM8MW9vtw/T19SWkpKS6CwkJATV1hsxrQ0obk4WQi+ryvVZ94+9tNgCQvVRQmjTxuPv9+3kgFN7q/yMIDR4P0g0CwF/tLOYxXxNQ6Awd8Vc9RMqclec86eLjx8giOG4mJtLMRlrTAAaP7rgpbDYbPCp66smTZLtd7knK1pZ0iuO6nFUeHr877t17ddPBg/cXb968T3Hw4NlySkqaOAgIAHSFgT/R1miLbymIzQIAG2tFBqqsmLJixf1jfn5ZSBUo/L8AgMZWVVVHV1RUCrwYRtgodDgYygEtSIGgDJ9H6gkvLyezPdzpQKs5HDnjmWzl2ab0QdobwgOBT2eMdvEMchJPG+iNqKhM99/vVusvX0qHw2Gy042suXieNcvNhVpCUDjCwa4YMq+1YsN0vftoqLn0Vhvk2rmP+MyOIiI6AEZvoNTvMrDRLACg4C4kPHDghmlOThlng4M/o+EUaAFjiHmUwVAHGlkUvsDnR+pBT4hxLxEEFH5NTT1fS0hcbS3X8cY1MhRySErz5xJp01lEf8ki4vv+PcWIWmAH8HsYrYLvwCg7qqKCczEqkh574U/PR0aQW6kpJKSkhMJBIKXBuZmUTE4GBtDt169V2nvsejXLeUv16EX2H+UmjHvWXVpyzs8dO34X490sAPqqqU0Z5uiYO83VtdorLS0HBY4ACDQAPSHGtWxEL41dTiboQuGhQFksTjo/HvDPyvqcApz+MPMdJPC2EKWF86iK/QIiaWRITCChB/aDcXEZewLAYSwhAAG+m6bD8+nwOjyPhp2LGoUHpjTQbc3AJCB+J4dLfT9/4rrcusWZvGFdnTakwntraRzu1Kvn6G9toJsFgKS21izTkyfKzY8fKzwfGPiaL+w/0FASh03i6mpZEFDxXUle0NXY5RQY0hQQJBy1eC4Y1JxXAKar1wP2QIeFCABVWTiPqzxnNrmTkkxAyIxmYZTMGGPBZ8JtNLitaMxT8eDHHUxcgZ4S3IITwGZsDxyJcB9c3RrvDx9Y2y//mjVpkUOA9KQJyV36SXzTprBmAQCFlhlzTp/KWQp5/V/j4ioSIU/T2AijBsTV15MooBNY7Wwm6wlCFgiDAUzgIfGFE19ZWYnpChQQrlgEYfmvl+iQlcsJzHexleZaU/f79wizghu5pExqg4kbKirj8TtQyPAYXGBudFVVFQaAgtgCb/Hzk0FTwsBwBxcVlQZVVtSHlpdVn/fyClSbZRQkqqcT1k1Gaj7YhG/Sn9QsAMADUhy3fn3IKl8f6pkQX4Mr7s9GGHx0BgRYfTS2pqYGuL2Wl3BjjCcYYF7UC/aBt4pRMCA8BBOFFJaXlwd0xNp2/3dqcwyKNXt300M+PkgzjPFuHBnje5+kpaUngdAREKAnyKjW1gbm5uYyeSi+AWeA4H9XPKTGI6qq6n1LismD/DyuV042e/WtmxUaNpZh4uNGZXSV6GuExrmtKalZAMAku5bmXJunlufOZh+IiW4IjgQ2AG8xmxnPYqPv32AL0CNieJsRNo8aGtIP/IAsDuoAuHrDiktKwMOpT6iuZseDoIKLizk+Hz/WosHla9Af6Iwx9uiSAtiCpB+4p9UxVdXVmN7GNDiegylx1JCImhriU1zMfVFRTr1zc6s8X6bWuEaG1VgcPxo5at3q1J7qg/Z2FhebDG5rm6Y3mgUA+r6KRjPvWF+5zNkaFEiiq6u56OX8OR4AECj8cXyNA/mfykQQPq54BABvmQQc3BesTHzM0BUc+Bzeonah4Y0sLi6+l5CQyEtZM7WE/wV+/EwraB0noZ7Fwu8SZF8fpKSk+Lx79w5BiaqorARNSYuF25DKKvJ7Tg55VlpCH336VHkmPY1sCQ6odPTzybO9eD5NdsK4i93kZJZDY1mzJzlRTk0dTZ7wVx8AY6ZKUmPHnJl16FDe8t/vVt379Ikgb/8hGANBofAFfB1RXFKaCoKDFczEAcl8OmJoiU8LAqPMuLJ87WBSGSA8NKCgPbWpXH4OibEjCB7PBuD5eAv8zmgCCjsKag/BBQUFL7KzsxmtgANTHkFATagBtz9/Il75+fTS2wzW7sR4si0qgmyNjqDrfLyzBxrOOCuiqX69reODZgEAk+2qfUfqX5i0Y0fhikderJMpKVhqZNLJAlvA3LI5kDbgFVowDYFC57ugBNMPAg+mIT/E92iYOIIPCgRxFRGlpaXJQCGJUL5EITKC5vE9P93BS3MzzzMHlxvwOTv7YWpqKj4XBtqDzwuAwug7rLqG/p6bSxEET1j9Hglx3G1R4dxtDABPPg6Za321h7LinnadO/3jji1NrfCmXm8WAFAHEBLX0zumu3JFzqI7tyvdQoJr/UtKuEgVAi3g2wHI7dez0WfnGU5e8IT0gzTROCpuiBn4UbLAWKNgIaPKEqx0NLIofEbYmKRjVjZPY1LQsMPzPBpjsbDqhvyPVbg4MO48j4gHhHdREedqVhY9k5ZG9yQl0t0JcZxt0eFcZwDAJSGW6C9ddK+Xno5/p56i/9jd15SAm3q9WQDgh/YcNGj1ACurApMjh8u3hQTXn0pJ5sTU1HAxK4qeD/xZpviezEafnc0F+qhHIILz8/MTQChRZWVlDE8L3FHe+3jJu8aHgJ4aXsfzINgCjcCCvkAjBFSFYCUABeFjpv4M58aCYYcqXR1zDnwOeEvkaWEhuZL1nhx/lUoPpySxPeJi2EhBCMAGsANDrK0u9xs7+nl3ednlTQmxJa83GwDo5ZTpZzDhsYG7O93g61PlFBzEvZT2Buq5dfCnmRXJ1waIkIGKIPpkPKPg/IIChmKAw5EK0NOBgKqecUf5AhJoxh8A4WkXL73NGG4+5WC1ja8RCAyjEWjk0SPC1Q+2AJ9HAJhiEXyGf1kZufw+k17KfEfOAv2cy0gnO+JiyNbIMO7WyHDulrCg2ilbNntLjtK/3E1W2r4lAm7qvc0GAD9YWElxobK1VZb9oYO+LhHhXLeIcOoNhRn8k41d0qR6njFG94/nnfCNLB8AFGwscD24l5wYpCaBIBulMhoMdaPncIUjzTQWPI/nIalXUlKCgVgMHAAGFnDAOHPIs5JSRvi3P32i56FseirtNT2b9prlFhtNtkSGUefocOoUGlxje/HcW8kxo36Hqse4poTYktdbBAB+MUQE26Zsdf7s7ONTvDE4iLpDQiy0spIpmiAI+McFVS30eGL5XREQcAF9MEGVwIAT8JJ4rzdEuv9LVf/BXsAXI89HlUGag9EM3oGUhEJHADDwQ+rBWOBFWRnHv6yc3s3JobfAY7v24QOFlU8PJCeSPUkJdE9cTC0a4K1R4dQ5NLh27dNHuUb792RKjR/7BLygJservisAOP+rYGsL7SKHPm955JXhFBTI8oDgzBc49g+BGa8gA/kbMKhARbEQIAVm5+QAZWFxnvsKhMYcAoHyQUDfHl3QxrTUUNIU2AU0yPC56CXFAIDg8pYk1NTWBuXl5j3Ly6u49fED58aHD1ygGuqVn0dvfvpIjr5MpdujI4l7Qhx1jYniwuonzgDAJn+/QvsbV94PW7nsJaTc3aCQ06YT+S3WAJjv6iU1aaL3oMWLiM0u9yc7AwMK1ge8oLtjosm97GwSC4V6jF6ZBBpGyPzijaCsiIA0cDicEw+CYxq2+P4/rnToHapuKOI0oiCBPWA8Ix6w3HhY9YEfP34Mzs3N8/r4sfJ4ajL7wtt0xuBeSE9jHQHBH4ZU9Y74WOoWF0N3JcTCqg+q2hoRynKJjaLr/f2qdRbOvz54kf3brv0kzFuyur/kvS0GAL+kY48esopWlp8m7nCrs/P0fL0jIpy9LTyMHk1KIr++fctQEvAw2ACmNNkQwTLRLOOa8mu/IECB+8i0swj8fXgdjTTSCi924Pv0fJDw/aglTHSNNFZXz/ErLmY/LSqil6GOcDc7m155n0n2Jydyd6Lg4Tj8MoW7NyGOjaveKTSobnPgi6oNfr5lNhfP5cNuLIESY0Y9xlzQlwixJee0CgD4A7rLy9vKmc3+qDbPNmn4/PnXPIKDq7YDCBtAG3ZERJBDiQnUKzeXRNfWMMV2yHYyXhDyNibPeMV3TCPzBM8Hhxc8YYoBhItJvcYeEK/oA54UvI+xJXAeatxvIPBz4N38Cl7OmbQ39MLbt+Tkm9fEIzGe7k1KJC6x0XQneD0u0REcJzC8cJCVTx+xrM57lqnaWCTKGxvGwYTnUig8CbdEuF/y3lYDAOqsXcSHDt3Tf475ZzE93aML9+175BETQzYFBdLNwUFkW1goxeNQQgK9DqsxAArucdCyghSEUTJjePnBGsYLYdAXJIh2oe7LQZDwMdqEhqAMAy8QfnRVdW0MdOMFV1bS0yDwE69f0YMpSdQ9PpaAoIFm4hi6cQVPZ0dcLKQbIoHvI1Dw1Ak0YOUzb2Jx4SxbY7FdcV+DsSlSM6Z+htlmpS8RYEvPaTUA8IfgfnASo0ZdUZpnWzZ94wbfTb9cerY9MoIRPLioBI8j0DeKntK+uFh6FVYoeklMHYAvTCZLCqs6qryiAnx49IiY1zFLiiu98SHIeoKPz2Y8G/DnD6YkQ2SbQMGvp+hawkF3xscRV3jsDIIH4dPN4SFkU2gQZ33QC878a5frDHa5EQWrOSxpo+mVYnpDfxNWUXaDNPQ32Qa5VQFAEGAuTL+/lWWByvz59ZPWrU2Y53mmcAu4p9shRjiYEE/OvX5Nzrx6RXYCML9//szkiwR2gZfJ5Lmh4SWlpRFwpILQg8FbSoSgCj0ndDEF/j8CgyXHgOLimuOvXnKPvX7JrPY9kFiD1U5d46Ip0g2ufLglW2NA+EEB9RuCA7irn/mw7e/e5mousWdLzzKk/aZOKhVWG3Swg4iIHmjzN9sKv9UBQBAkxoy+ojjXpkoeqljqy5ZyJnvsYi+9+xuAEEF2REUSVwQjPp6hIt/CIoKuJ7PKwQPCA70ZRiuQ41Ej0DZgJI3NunyjzYAG9/2g8A7pBAK5HAKUw3D7LqAeWPFcF7i/LSaKbI+JoquePuKu8HlMFnndw4NO3L2TI21qREXHj6aDTIwzhkyeuFGoZ89vUgVrTFttAgDwp7ToINXV/WebpsmbznqjvGA+a+RWZzLtwH6y6vEj4h4NtAA0hJR0HlpOIHeDgRUrHjSA11UB3dMABHI+EzfwuywYYEDocB4m05go9gCP62HVJ2BCjcAtZjY5e0JDSzYE+JNNwYFk+eOHdPxOVzLGbRvV3+pE5S1mUyGdwZ9GzDELnrdu3SlDc/O5nbt0+S6NWm0CgABh7EADV04ENuV2UzI2ej545QqW8fFj1AIar9zDw4gbgLAH4gXvwkKm3zO+vp4DcQAEZtjEC5EsUA4zR8BPO6NWgDtLsIzomf6GuMDK3puUwKz+bcDtbmBXdgHfbw0P5TrcusE2P3eGTju0n2itWkZUocDfb+Z02nvsyAqF4XoPBwwaNNLY3Nxi2Zo1a9S1tbU7duz0j/tZtNTY/t372xSAxl8KV7jQUTadFYSDerrrHena27fqDsXHsY+AW3gv+zPTxAvFkhr/7JwCQSoC7QHWASDzCcaay/D9c2hZPPnmFSN4j4R4hnKQ792AapxCgsimkCC68PYNOvPIQe6Ena5ctaUOVM7SjPYco18opaVxabyhoeNSR8eNBlOmTBk8bNgwKTk5ucZti20l6O8OAP6AXlrq+zXs5lH99euI6d49lZsePmDtA6OJybEg6IjDQjl4RUytFygJPR5Y8agZLC4U+OkDiCMwhbAP8vdgaGG1MyueoSDX6Cji6O9H7e7coqaep+hIoBq43AlXYvKE6sEzZ9yYYGKy1tjCwtrJ1dV1tbOz8/Cx48Z9L9ppcxvwV2hD27i45iyjx9oOC6j1wX01E7Zv486GWbKDkRHcExCVXoZmLIhgGa8INQA0Ahp1WSSssooTVFbGBh+fnHrzmkI0ywCAgvcAd9Md8jg7QoLYG0D45uc9udMOH6Aj1q8p7j9l0scRFuaxy1xcrnucPHlmnZOT09hJkyaNmzptWi9xcXFs2v3Wq/2vvu+bUBC0i7cfpKdrbOG6/a3mooUUdq3ljnfbTlffvUP3hIeSk69fQl7+DQ2vqmYiYDC+MBfGRRBoNARYz4uK6p+XlHIvvMvAIAs4P56cTntNrmW9pyegWcv67Jn6EWtW5k9csyptxe7dz4xtbFa4Hjp8dtykSVPUtLS0phgbG08ynDmza7fubdrh0BxAvwkAEtLS8lNsbPbN2rwpSWfpoiLVxfZc2G6YY37yKNkPFah7ebmMVxMDNIMTNRicIRBAQQS0ggZVVNDHMBt2AoDaBxqACTVMK1+BFMO+gAAY9vZkD7CYnaNjauyppK6ug/MBQsLCIhAY/us39vgmAPQQFZUcbW7moTXXOkxr+RL20FXL603cXHIdb92oOwv8HVpTTSCVwE3E/lJ+QR6HL5j2Q7ADPgDOBSieoKuJBhcN8KGUJOIcAQWU58/YUDyhKnbzaiQmjg/qM2l8EPTzjGjOavwe7/kmAEBhW0PR1DhU13ENVVvswJq0fWvVo7dvC+4CjTx495bpXAMjy4F4gCTyizgIBOP/Q8AFLST0PJNmSCL8vA7ZCvVb8PHplpBASH1HUdMDe4tVFsytlpll+BpyOXFi+sN/7S4na/4tEmotAe6bAAAb9zlqwMpXX76YjnXaWOd8+1ZJRGUFDlgwLY0NHXIQhEVVV9cDEBzs3+TliQgJBwCgdQS9HsblRP9/B6QZHO7eJiu9HpBLH97T658+UFfvJ0WLTp/MGbN5Q/2AuVb18lbm1bKmRuliQ7R3whZqcigo3Ei2JQJr7fe2KgB/1UcppCBvNnDhvDyVxfZknNOmiq1Xryb4ZmZmorvZ0HzFL7Ig7URWVFSD8WUnoSsKreuYfkYALr17C17QK7oTAED68YCs5rwrv5AV9+6Sh2AfvEuKqW9ZKQ2traUPsz/Tzfd+487es6tad/VyjvQc09p+hlPT+00c79N35PBfQCO1YDtlGbz60veekmkTAHBzbriy3Ri8GpKCmWm82hKH2jkH95XfTEkpevruXTamlJm8Pn9GgBng4DfORlZW1kIPD/aNMvYAaSiezaLPSoo5ntB14QYJNWfI37vAYXbmBJ3360VyBjwoaC0hXgX5NAjcVUhzkwTQHn/oJ/XOyaWOly8XzfJwr9FetpgjYzG7pu9kgwRpoxmvpaZMDJQYO+qmsIL8HNhktjdO/sDlVf52UqYtNotqVQAE6gnb06vJz5z+VMnCLE5jxRKuqbvbW79Pn0pweELQn9/A8fAcY3D5xhdSDRyofOHUJNPLg5OWkTW1NBJaCR9DxwWsfgoFdKjjRlJHX28y6+ghuv6JFwXtIN7FRTQShjOiIZ5AgPk9SkxLO8QUnGf5+ewTYaHs9bdulFqeOk6HwWVNBsyY+kjexoLbZ7T+3X4TxvlKz5ga2VNDbUPvwVpuomqqjqAt6h2FevSHbukxkOkdARuQj4VjDALWGhcQbXUAcKt6ieHDdsNARQRc97F68NLFRTeTkopjoE1E0FrCS67xZob5moCRLwurXqgdMKBdGAQNXHiEwP3QqmpuUEUlfQ4DfNcz39UeTEmkHolx1AkAmLrbnVpeOEddo8LJsdepNBT6/lED0H6gFvA0id9dAVSHYGBKIw46t58VFNTsfvTw1ZKb1+jE1St91efNzdReuqhGwXJOucy0ySGyRjOSpAynvRTV1twvMW1yisSEsU8kJhtESBpOfSmmp3Oq78gRZ0VVVVbDbl/Nbl9sdQBg64KB/WfO8NZctoSlOMcswe7YUWaIj+l2wJow3uLBNFnx+vVh/uv9s6ysLBR4ADTOMi0n0Cf0JD093e/jp8+BBYVlgRALeOfl1dzJzKxiUg/x0XR7SCCZAgN4JrDh0/Knj8jGsGDGRtzNyaZREFPE4vY4fKpDmyLoisNsKzOzjH1JEHVHwXmQYyK3Mt+xzyXE1x70e1bodve3LKebN/KWn/MsdDhy+M3yC+dqV5459cb5zq3SDVcuF2y6eiV7+cULLLjYaIG2hdkduBqUHdiUrwaiVQFADwOuE7BR1W5+ge7qlVVrLpzPjCgvY+OKa9T53NB4iyDgivf78OHji5yc3N8TE5OwoRbtAXY0P83IyEAwvD9+zA8uK6vHqZZH+fmcX96/Iy5xUXRTwHNic86T2F44R2wuXyKr/XzoxvAQCrVegkFbRG1Ngx1hepAQDOzO4B8CjRRsr4Ad3rhY+D2uTO06CRoBXkOhHzeXwqFwpFE8D+YWqkOLi0u2Xb+W4nr7dqbJDrdPsE+2C9CV4td4Sq0KAFx42WSAhVmiisNCztJznsWwmsthDJUZjGhorOJrAM4MMBMy/BEmbCPffOzY+cthYYnPP3z4cC85ORnfhyDcjo2NhU63UuysgNQ1B7SBe/PzJxQ0tb9+lZqfOkGMDh4gSx7egzpvKN0YGkQ2wIF24khqMgRx6Vw0zih4/pAfrHz+fAEKE+IPxuuCW6Qt7B1F28OjR57GNmiu4D4AAoMjVUkABExnFpm7bo+WHD/mblcpSdPvAgD42dIykwyu97ey+DTNZVuxX25uPe5swp/R4k27N9IE/mgrQ0ER0KiLQxR7b9z4/fjjxyFnnjx5AhqRI6AlpKZA0BDsL4qsrmHf+vCB7fPxU/H+pHjqHPiCWJ05Rae47yRGRw7SdQHP6SZezZcAENQxJICuDXpB3KBeAGDQX0F7YDKGETZSVBIXBI69SvA7mNiDCxlYACEWMrAIBkz5YNG/hgEPwEAbAg5CbXRNLed6UlKpx/1775acOJ4xwm5BkLCy4goYdxX55gCg29lzkOqSwSuX16gYTr+058H9169whTfu7UTO5zdoCYywYDoGaQZX+qHbt297w+2V0NDQWFD9B0BH3gAMbIWQj68jd4NAuIHl5Zzf370reQSFnGNpr8jSe7/RiW4uxOTIIbrS+wnTZoIasBlsAhwEasBs6P1hr4da8Jogf7IPasZPigpoSFUliapDr6mObx844HHVoEvMaAQz3wZAgLBxRKr8+PPnmQv27g7St1/ob75nd6nZ7l0lMlMmevfWUHOEy3NpNLVJ+V8B02IKQlcM9ok2VJo+9dfJ25wL7Q4fTIksKalgjO6fjK1gBkAABKPyQCvQlkKBggp3X7lyJQioKAW5F7QHp1miQM0RIHwee/xjWPU0tLyiHnJHJLKulv6SlUm2hQYTk6OHyQwY5Ft46QIXGqyIE9gC0AL2ppBAtqOvT7ETNF5tDglkOUeEQMUshLsdvKaL7zK4UZBtRUHDaqf42WHVVdxoMOBQm4AWGC6JAI2Lho4Mr/T0zCFWFjeFh2p7Qs7JV3aWUYbU9CnhwooD5veQlTVuzmW5EJAWA4DXfReB60Eaurp8XnTq5CefD1kVzC4njSYS/8/wBZ9HMcAKAN8d0g+c8NLSMpgdKIB5s2oEAMqT9QgC9vrjgfeZeV+cfiktZflB1AvCI/fzcum5dxlkyc0bxAC2JTA7fIAz3WMHd8KO7ayZez3SbS54vnW4dfMzTL0UrPXzrdgEWrENZsFW3bia6BwayNobEVb+IDeH+6K8jKLgkYYSIAGImhYPvawxsPqTsfsCNNpi44YL0lMMvBQszXLkjAxjoCNQnhFiC67Q2iIAcI/oXpoa63UXzg8dYW/n90tcbDW2l2Mw9X8GLfh0xJtk4Q1hwMonwaWlXOhmaxgfReEjdYWDXYBArB69JAjM6vA5fA1pKwLSDejvQ5GGGwjbXT4CEN3DQshE9x10gstWMgRqwIMW2xGVeTbFKnDtedW5VnEzD+x9t+D6lez1gf41G4P865bcuPpm2fUrL22OHw1Y+9grd1dkeO1vnz9xIPXNjoB0CBpf1AAI4NhxuIcduKv43+a4ucTKmpnkSU0y8MLL8X4N37eYgviheMO2L5hLgV1w7889eZzOdXO55/3+fSGu/oYJdr7rJwBDsIIZ7YDXYGsaptSIFMDQAKx0FDSudtiypggfoyYIngfjyMHoGI3j0/z8uoCKCqwhkKsfs+hRCMKW3L5JTA4dIMPXr6XjtjlRuHZksaqNZZyCmUk67DvBgWvPB09w215offli7Vr/Z/WWnqdT5xw7Gj1xyyZf++uX328PDKjyjIx8DxP0MHGP7ipv5gyBD8ovKMKMrePpUy+Mtjm/gqn6e13Eeg/9bgBgEktYWcnefO+e7DXXr5WeCQ76iC4nUgS6ln9221D4kfDHBNSEHg1kRLnPCwt5G/cxOR92w25aMO5Ug8IHLmahABAEbM7F4AyeZ+GMrw/0BKEHA4PWBHr/yXbgfmvP02Ss82a60PMUZ/zKZT5yo/RXwdYKpuLaWo491dU2SU42eC47c3q4nuOaIu1FdqnMYbcgfMbBvaVjtm6pdDjnWXXrzRtOWElJHY5S4YwBjsoGFxSWR5RXVF+Kia4Aqs1XtbXOgiuBt/gKT82moE4iwop9h+t6LLhwju3+/Hkh5Hpy+FHn/3x+PtUwUS8cKDwEBwWOKz+4tIR7/30mM1eWCHtLMAESagM+xtUPwkcg0C4wgMDqx3ghDLQDtIFC7ocAFRGIC8gt6LK7AJUym7NnyOSdbtTq8IHP6uPH2XUWEpLE65LhgsFNmvAWZn8HwfUs54npDj0gOXHCE1nD6WGyRtOj5EyN3klOmRisudi+du6xI9xTwcG1h548SV97/ly6/d49L3bc/e2j0/VruepzzJ53l5E2/tIrAv6TljQLANz0SFih/5whDnZhuwJe1F569QoNJUMrDduS8T0gwRZjKHT0ZiDq/cAMVONecvDc9VcvqT9sWQyrn+mQY/I4WJbEQAfOR4Gj5gjyRLx0AnRDw+s464s2AKfdHxUV0stQNdv8+BE1PLCXLr1w7r2clqYRXp/m7wSAe7hDwaZ7t34S49Cg9tEbuk9y/NibMkbTY+UszcvV7eaV6q1eUStlOCUFCjw3Yb+heFVryzfiOkNa7WofzQIAor3pstMm/zbn2JGSA5ERrNtZ7+tjoUGW2YIAJ1r46WZBqM9ssMejGW4ApBh440OwRxys8vuQ59/+6CE3ilVPQmqqwMDWkDhwS+PAQ8J2dP/Pnz8jAJgbYsZV+Z6QwJCjBqAmPC8ro76lJeQ+1AIWeJ6mxgf3c9QMZ3jCNQoGfg1PtxfqMUCov7xZ3+F6RyTGjLwiMX7MY9mZM+LFdYce7Ddh7APpaZNfgParfM1ntqoGQKQnDBfmma413zbwcGwMvZaewQ4pL68X+P0CATV4QTyXErOcvMwn379HMNAOhJSVcS2PHiLHoV/UD1xBL0g5Q9oAPRxoSanE/UPLcdMNzIoiLeH7G7agwUAJNOZxUSGBYQyuP7wf71+GwXFTCMrGrF9b2EtxgMnXCot/ldbOeOU/iHPA1RAf3VFYSBl0RR4yn1jEwSt6tMplVb5KA1D44iOGH5GdNPE30z27M29DS/gjGHwTFFYEK57ZwgxoBHM3zOA2f4dDfnTJz7EQhnL8s7Nz5h87Wjz//FkaDBlPf+h88y4upujnw4rG/D4NARrCwW5mBpi/uwoTpeJ8AdiCSAimsBaADV53c3PgvWXU7vRJarzHg45aMP82CE62Nfj6a4H8kvO/CoDOoiIqCobTA9UX2ObanDj26VRsTPGTrKx8HAnF7QAwj4/3fbKySvxzckpAuCVP378vxlsczH6SkZF5MyoqDrceQFuARwCUJ3fcvBFvfeRgqbv306JrqSklt9Jel1xISS6+mJpSfCM9rSSgoKAUckE5fjD75QPnMzUCyET6ZGcX+3z+XPw8L7fkzru3JbfeZuBRfD3tTTFse1ZsfnBfiem+PaXCigr2sJr/dT1BCND/A3LN4vr+R9BIAAAAAElFTkSuQmCC" alt="" /></p>', 1, 0, 0, 1),
(58, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(59, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(60, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(61, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(62, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(63, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(70, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(71, '1111111111111', '<p>22222222222222222222</p>', 1, 0, 0, 1),
(72, 'AAA', '<p>fff</p>', 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `courseYear` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `studentName` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `rollNo` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `maritalStatus` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `residentialAddress` varchar(255) NOT NULL,
  `permamentAddress` varchar(255) NOT NULL,
  `hostler` int(11) NOT NULL,
  `studentImage` varchar(255) NOT NULL,
  `sub1` int(11) NOT NULL,
  `sub2` int(11) NOT NULL,
  `sub3` int(11) NOT NULL,
  `sub4` int(11) NOT NULL,
  `sub5` int(11) NOT NULL,
  `sub6` int(11) NOT NULL,
  `sub7` int(11) NOT NULL,
  `sub8` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `sid`, `course`, `branch`, `courseYear`, `section`, `studentName`, `fatherName`, `motherName`, `rollNo`, `dob`, `mobile`, `email`, `sex`, `maritalStatus`, `category`, `nationality`, `image`, `residentialAddress`, `permamentAddress`, `hostler`, `studentImage`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `sub6`, `sub7`, `sub8`) VALUES
(31, 0, 1, 1, 1, 0, 'Hemant Dev', 'fg', 'fgf', '0120', 'fg', '0120', 'ankit@gmail.com', 'Female', 'Married', 'General', 'fgf', '', 'ghfgh', 'ghjhh', 0, 'UPLOADS/STUDENT_IMAGE/SI1389008105Koala.jpg', 5, 1, 1, 3, 0, 0, 0, 0),
(32, 0, 1, 1, 2, 0, 'ghg', 'hghg', 'hg', '1', '2014-01-31', '0', 'ankit@shadowinfosystem.com', 'Male', 'Married', 'General', 'gh', '', 'h', 'hg', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(56, 0, 1, 1, 1, 0, 'Deepak', 'Papa', 'Mammi', '0714510023', '1989-09-08', '9794783192', 'deepak@shadowinfosystem.com', 'Male', 'Unmarried', 'General', 'Indian', '', 'Ashok Nagar Delhi', 'Gursarai (jhansi)', 1, 'UPLOADS/STUDENT_IMAGE/SI1389077683photo[1].jpg_sz=155', 0, 2, 1, 1, 0, 0, 0, 0),
(58, 0, 1, 1, 1, 0, 'XYZ', 'aaa', 'fgh', '0714510023', '1998-01-23', '0123456789', 'GMAIL@EMAIL.COM', 'Female', 'Unmarried', 'General', 'Indian', '', 'WHY', 'WHY', 0, 'UPLOADS/STUDENT_IMAGE/SI1389098943Lighthouse.jpg', 4, 1, 1, 0, 0, 0, 0, 0),
(59, 0, 1, 1, 1, 0, 'aaaaaaa', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 1, 2, 0, 0, 0, 0, 0),
(60, 0, 1, 1, 1, 0, 'bbbbbbbb', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 1, 1, 1, 0, 0, 0, 0, 0),
(61, 0, 1, 1, 1, 0, 'cccccccccc', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 1, 1, 1, 0, 0, 0, 0),
(63, 0, 1, 1, 1, 0, 'eeeeeeeee', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 1, 1, 3, 0, 0, 0, 0),
(64, 0, 1, 1, 1, 0, 'fffffffff', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 1, 1, 0, 0, 0, 0, 0, 0),
(65, 0, 1, 1, 1, 0, 'ggggggggg', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 2, 0, 0, 0, 0, 0, 0),
(66, 0, 1, 1, 1, 0, 'hhhhhhhhhhh', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 3, 1, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `total_subject`
--

CREATE TABLE IF NOT EXISTS `total_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1007 ;

--
-- Dumping data for table `total_subject`
--

INSERT INTO `total_subject` (`id`, `subjectName`) VALUES
(1000, 'math'),
(1001, 'physics'),
(1002, 'DBMS'),
(1003, 'Compiler'),
(1004, 'Computer Networks'),
(1005, 'mechanics'),
(1006, 'chemistry');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
