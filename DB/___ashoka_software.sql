-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 05, 2014 at 06:30 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ashoka_software`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` varchar(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `username`, `password`, `mobile`, `address`, `status`, `type`) VALUES
(1, 'shadow infosystem', 'info@shadowinfosystem.com', 'superadmin', 'shadow@12345', '9899996124', '208,2nd Floor Rajhans Plaza,Indirapuram, Ghaziabad-201010 ', '1', 'SUPERADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `assign_faculty`
--

CREATE TABLE IF NOT EXISTS `assign_faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` varchar(255) NOT NULL,
  `branchId` varchar(255) NOT NULL,
  `yearId` varchar(255) NOT NULL,
  `subjectCode` varchar(255) NOT NULL,
  `subjectName` varchar(255) NOT NULL,
  `totalClass` int(50) NOT NULL,
  `facultyId` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseID` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `courseID`, `branch`) VALUES
(1, 1, 'CS'),
(2, 1, 'IT'),
(4, 1, 'EC'),
(5, 1, 'ME'),
(6, 2, 'Not Applicable');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `course`) VALUES
(1, 'B.tech'),
(2, 'MBA');

-- --------------------------------------------------------

--
-- Table structure for table `course_year`
--

CREATE TABLE IF NOT EXISTS `course_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseID` int(11) NOT NULL,
  `courseYear` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `course_year`
--

INSERT INTO `course_year` (`id`, `courseID`, `courseYear`) VALUES
(1, 1, '1st Year'),
(2, 1, '2nd Year'),
(3, 1, '3rd Year'),
(4, 1, '4thYear'),
(5, 2, '1st Year'),
(6, 2, '2nd Year');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facultyName` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `experience` int(11) NOT NULL,
  `dob` date NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `maritalStatus` varchar(255) NOT NULL,
  `facultyImage` varchar(255) NOT NULL,
  `permamentAddress` text NOT NULL,
  `residentialAddress` text NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `courseYear` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `studentName` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `rollNo` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `maritalStatus` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `residentialAddress` varchar(255) NOT NULL,
  `permamentAddress` varchar(255) NOT NULL,
  `hostler` int(11) NOT NULL,
  `studentImage` varchar(255) NOT NULL,
  `sub1` int(11) NOT NULL,
  `sub2` int(11) NOT NULL,
  `sub3` int(11) NOT NULL,
  `sub4` int(11) NOT NULL,
  `sub5` int(11) NOT NULL,
  `sub6` int(11) NOT NULL,
  `sub7` int(11) NOT NULL,
  `sub8` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `sid`, `course`, `branch`, `courseYear`, `section`, `studentName`, `fatherName`, `motherName`, `rollNo`, `dob`, `mobile`, `email`, `sex`, `maritalStatus`, `category`, `nationality`, `image`, `residentialAddress`, `permamentAddress`, `hostler`, `studentImage`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `sub6`, `sub7`, `sub8`) VALUES
(29, 0, 1, 1, 4, 1, 'Ankit Arjaria', 'Ram Prakash Arjaria', 'Pushpa Arjaria', '0841410008', '04-NOV-1990', '8800589538', 'a.arjaria@gmail.com', 'Male', 'Unmarried', 'General', 'Indian', '', '', '', 1, 'UPLOADS/STUDENT_IMAGE/SI1388215976logo.png', 0, 0, 0, 0, 0, 0, 0, 0),
(31, 0, 1, 1, 1, 0, 'hemant Dev Viduagfgfgfg', 'fg', 'fgf', 'gfg', 'fg', 'gf', 'gg', 'Female', 'Unmarried', 'General', 'fgf', '', 'ghfgh', 'ff', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(32, 0, 1, 1, 1, 0, 'ghg', 'hghg', 'hg', 'h', '2014-01-31', 'g', 'h', 'Male', 'Unmarried', 'General', 'gh', '', 'h', 'hg', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(33, 0, 1, 1, 1, 0, 'ghg', 'gh', 'gh', 'gh', '2014-01-09', 'gh', 'g', 'Male', 'Unmarried', 'General', 'hg', '', 'gh', 'hgh', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(34, 0, 1, 1, 1, 0, 'hgh', 'ghg', 'h', 'g', '2014-01-02', 'gh', 'g', 'Male', 'Unmarried', 'General', 'gh', '', 'g', 'gh', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(35, 0, 1, 1, 1, 0, 'hjhhj', 'hjh', 'jh', '', '2014-01-02', 'h', 'hh', 'Male', 'Unmarried', 'General', 'hh', '', 'h', 'h', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(36, 0, 1, 1, 1, 0, 'ty', 'ty', 'ty', 't', '2014-01-10', 'yt', 'yt', 'Male', 'Unmarried', 'General', 'tyt', '', 'ty', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(37, 0, 1, 1, 1, 0, 'ty', 'ty', 'ty', 't', '2014-01-10', 'yt', 'yt', 'Male', 'Unmarried', 'General', 'tyt', '', 'ty', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(38, 0, 1, 1, 1, 0, 'ty', 'ty', 'ty', 't', '2014-01-10', 'yt', 'yt', 'Male', 'Unmarried', 'General', 'tyt', '', 'ty', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(39, 0, 1, 1, 1, 0, 'ghgh', 'gh', 'gh', 'gh', '2014-01-17', 'ghgh', 'g', 'Male', 'Unmarried', 'General', 'gh', '', 'ghg', 'gh', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(40, 0, 1, 1, 1, 0, 'erere', 're', 'r', 'e', '2014-01-17', 'er', 'e', 'Male', 'Unmarried', 'General', 'r', '', 'e', 'er', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(41, 0, 1, 1, 1, 0, 'yuyuy', 'uy', 'uy', 'u', '2014-01-17', 'yu', 'y', 'Male', 'Unmarried', 'General', 'u', '', 'y', 'yu', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(42, 0, 1, 1, 1, 0, 't', 'yt', 'y', 't', '2014-01-24', 't', 'g', 'Male', 'Unmarried', 'General', 'ty', '', 't', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(43, 0, 1, 1, 1, 0, 't', 'yt', 'y', 't', '2014-01-24', 't', 'g', 'Male', 'Unmarried', 'General', 'ty', '', 't', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(44, 0, 1, 1, 1, 0, 't', 'yt', 'y', 't', '2014-01-24', 't', 'g', 'Male', 'Unmarried', 'General', 'ty', '', 't', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(45, 0, 1, 1, 1, 0, 't', 'yt', 'y', 't', '2014-01-24', 't', 'g', 'Male', 'Unmarried', 'General', 'ty', '', 't', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(46, 0, 1, 1, 1, 0, 'yuy', 'uy', 'u', 'yu', '2014-01-17', 'uy', 'u', 'Male', 'Unmarried', 'General', 'y', '', 'y', 'yu', 1, '', 0, 0, 0, 0, 0, 0, 0, 0),
(47, 0, 1, 1, 1, 0, 'tyt', 'yty', 'ty', 'ty', '2014-01-10', 'y', 't', 'Male', 'Unmarried', 'General', 'tyt', '', 'hj', 'ty', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(48, 0, 1, 1, 1, 0, 'rtr', 'tr', 'trt', 'rt', '2014-01-31', 'tr', 'tr', 'Male', 'Unmarried', 'General', 'r', '', 'rt', 'rt', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(49, 0, 1, 1, 1, 0, 'rtr', 'trt', 'rt', 'r', '2014-01-31', 'hju', 'rt', 'Male', 'Unmarried', 'General', 'tr', '', 'tr', 'tr', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(50, 0, 1, 1, 1, 0, 'rtr', 'trt', 'rt', 'r', '2014-01-31', 'hju', 'rt', 'Male', 'Unmarried', 'General', 'tr', '', 'tr', 'tr', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(51, 0, 1, 1, 1, 0, 'rtrt', 'rt', 'rt', 'r', '2014-01-10', 'rt', 'r', 'Male', 'Unmarried', 'General', 't', '', 't', 'tr', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(52, 0, 1, 1, 1, 0, 'rtrt', 'rt', 'rt', 'r', '2014-01-10', 'rt', 'r', 'Male', 'Unmarried', 'General', 't', '', 't', 'tr', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(53, 0, 0, 0, 0, 0, '', '', '', '', '', '7888', '', 'Male', 'Unmarried', 'General', 'Indian', '', '', '', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(54, 0, 1, 1, 1, 0, 'gh', 'gh', 'gh', '123', '2014-01-09', '98', 'arjaria.amit@gmail.com', 'Male', 'Unmarried', 'General', 'h', '', 'hg', 'g', 0, '', 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `total_subject`
--

CREATE TABLE IF NOT EXISTS `total_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1002 ;

--
-- Dumping data for table `total_subject`
--

INSERT INTO `total_subject` (`id`, `subjectName`) VALUES
(1000, 'math'),
(1001, 'physics');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
