-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2014 at 02:35 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ashoka_software`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(250) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `status` varchar(11) NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT 'admin',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `username`, `password`, `mobile`, `address`, `status`, `type`) VALUES
(1, 'shadow infosystem', 'info@shadowinfosystem.com', 'superadmin', 'shadow@12345', '9899996124', '208,2nd Floor Rajhans Plaza,Indirapuram, Ghaziabad-201010 ', '1', 'SUPERADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE IF NOT EXISTS `assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `assignment` varchar(255) NOT NULL,
  `faculty` int(11) NOT NULL,
  `lastDate` date NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `assignment`
--

INSERT INTO `assignment` (`id`, `course`, `branch`, `year`, `topic`, `assignment`, `faculty`, `lastDate`, `date`) VALUES
(2, 1, 1, 1, 'cccccccccccc', 'UPLOADS/ASSIGNMENT/FI1389600094ASSIGNED WORK STATUS REPORT - Narender - Jun 2013.xlsx', 12, '2014-01-11', '2014-01-13'),
(4, 1, 1, 1, '', 'UPLOADS/ASSIGNMENT/FI1389610932Chrysanthemum.jpg', 11, '2014-01-09', '2014-01-13'),
(5, 0, 0, 0, '', '', 11, '0000-00-00', '2014-01-13'),
(6, 1, 0, 0, '', '', 11, '0000-00-00', '2014-01-13');

-- --------------------------------------------------------

--
-- Table structure for table `assign_faculty`
--

CREATE TABLE IF NOT EXISTS `assign_faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseId` varchar(255) NOT NULL,
  `branchId` varchar(255) NOT NULL,
  `yearId` varchar(255) NOT NULL,
  `subjectCode` varchar(255) NOT NULL,
  `subjectName` varchar(255) NOT NULL,
  `totalClass` int(50) NOT NULL,
  `facultyId` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `assign_faculty`
--

INSERT INTO `assign_faculty` (`id`, `courseId`, `branchId`, `yearId`, `subjectCode`, `subjectName`, `totalClass`, `facultyId`) VALUES
(6, '1', '1', '1', '1', '1000', 5, '11'),
(7, '1', '1', '1', '2', '1001', 2, '11'),
(8, '1', '1', '1', '3', '1002', 2, '11'),
(9, '1', '1', '1', '4', '1003', 2, '11');

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE IF NOT EXISTS `branch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseID` int(11) NOT NULL,
  `branch` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`id`, `courseID`, `branch`) VALUES
(1, 1, 'CS'),
(2, 1, 'IT'),
(4, 1, 'EC'),
(5, 1, 'ME'),
(6, 2, 'Not Applicable');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `course`) VALUES
(1, 'B.tech'),
(2, 'MBA');

-- --------------------------------------------------------

--
-- Table structure for table `course_year`
--

CREATE TABLE IF NOT EXISTS `course_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `courseID` int(11) NOT NULL,
  `courseYear` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `course_year`
--

INSERT INTO `course_year` (`id`, `courseID`, `courseYear`) VALUES
(1, 1, '1st Year'),
(2, 1, '2nd Year'),
(3, 1, '3rd Year'),
(4, 1, '4thYear'),
(5, 2, '1st Year'),
(6, 2, '2nd Year');

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE IF NOT EXISTS `faculty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `facultyName` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `experience` int(11) NOT NULL,
  `dob` date NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `maritalStatus` varchar(255) NOT NULL,
  `facultyImage` varchar(255) NOT NULL,
  `permamentAddress` text NOT NULL,
  `residentialAddress` text NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `facultyName`, `fatherName`, `motherName`, `experience`, `dob`, `nationality`, `mobile`, `email`, `sex`, `maritalStatus`, `facultyImage`, `permamentAddress`, `residentialAddress`, `username`, `password`, `status`) VALUES
(11, 'Anil Kumar', 'aaa', 'ghgf', 11, '1965-01-29', 'Indian', '01230123012', 'anil@gmail.com', 'Male', 'Married', 'UPLOADS/FACULTY_IMAGE/FI1389612648Penguins.jpg', 'asdfghjk', 'ghjk', 'anil', 'anil', '1'),
(12, 'asd', 'hjh', 'jhj', 0, '2014-01-28', 'hjh', '01230123012', 'anil@gmail.com', 'Male', 'Unmarried', '', 'hjh', 'jh', 'hj', 'aaa', '1');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `branch` int(11) NOT NULL,
  `courseYear` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `studentName` varchar(255) NOT NULL,
  `fatherName` varchar(255) NOT NULL,
  `motherName` varchar(255) NOT NULL,
  `rollNo` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `sex` varchar(255) NOT NULL,
  `maritalStatus` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `residentialAddress` varchar(255) NOT NULL,
  `permamentAddress` varchar(255) NOT NULL,
  `hostler` int(11) NOT NULL,
  `studentImage` varchar(255) NOT NULL,
  `sub1` int(11) NOT NULL,
  `sub2` int(11) NOT NULL,
  `sub3` int(11) NOT NULL,
  `sub4` int(11) NOT NULL,
  `sub5` int(11) NOT NULL,
  `sub6` int(11) NOT NULL,
  `sub7` int(11) NOT NULL,
  `sub8` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=67 ;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `sid`, `course`, `branch`, `courseYear`, `section`, `studentName`, `fatherName`, `motherName`, `rollNo`, `dob`, `mobile`, `email`, `sex`, `maritalStatus`, `category`, `nationality`, `image`, `residentialAddress`, `permamentAddress`, `hostler`, `studentImage`, `sub1`, `sub2`, `sub3`, `sub4`, `sub5`, `sub6`, `sub7`, `sub8`) VALUES
(31, 0, 1, 1, 1, 0, 'Hemant Dev', 'fg', 'fgf', '0120', 'fg', '0120', 'ankit@gmail.com', 'Female', 'Married', 'General', 'fgf', '', 'ghfgh', 'ghjhh', 0, 'UPLOADS/STUDENT_IMAGE/SI1389008105Koala.jpg', 5, 1, 1, 2, 0, 0, 0, 0),
(32, 0, 1, 1, 2, 0, 'ghg', 'hghg', 'hg', '1', '2014-01-31', '0', 'ankit@shadowinfosystem.com', 'Male', 'Married', 'General', 'gh', '', 'h', 'hg', 0, '', 0, 0, 0, 0, 0, 0, 0, 0),
(56, 0, 1, 1, 1, 0, 'Deepak', 'Papa', 'Mammi', '0714510023', '1989-09-08', '9794783192', 'deepak@shadowinfosystem.com', 'Male', 'Unmarried', 'General', 'Indian', '', 'Ashok Nagar Delhi', 'Gursarai (jhansi)', 1, 'UPLOADS/STUDENT_IMAGE/SI1389077683photo[1].jpg_sz=155', 0, 2, 1, 1, 0, 0, 0, 0),
(58, 0, 1, 1, 1, 0, 'XYZ', 'aaa', 'fgh', '0714510023', '1998-01-23', '0123456789', 'GMAIL@EMAIL.COM', 'Female', 'Unmarried', 'General', 'Indian', '', 'WHY', 'WHY', 0, 'UPLOADS/STUDENT_IMAGE/SI1389098943Lighthouse.jpg', 4, 1, 1, 0, 0, 0, 0, 0),
(59, 0, 1, 1, 1, 0, 'aaaaaaa', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 1, 2, 0, 0, 0, 0, 0),
(60, 0, 1, 1, 1, 0, 'bbbbbbbb', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 1, 1, 1, 0, 0, 0, 0, 0),
(61, 0, 1, 1, 1, 0, 'cccccccccc', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 1, 1, 1, 0, 0, 0, 0),
(63, 0, 1, 1, 1, 0, 'eeeeeeeee', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 1, 1, 2, 0, 0, 0, 0),
(64, 0, 1, 1, 1, 0, 'fffffffff', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 1, 1, 0, 0, 0, 0, 0, 0),
(65, 0, 1, 1, 1, 0, 'ggggggggg', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 2, 2, 0, 0, 0, 0, 0, 0),
(66, 0, 1, 1, 1, 0, 'hhhhhhhhhhh', 'sd', 'ss', '0120', '2014-01-09', '8800589538', 'ankit@gmail.com', 'Male', 'Unmarried', 'SC-ST', 's', '', 'sd', 'ds', 0, '', 3, 1, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `total_subject`
--

CREATE TABLE IF NOT EXISTS `total_subject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subjectName` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1007 ;

--
-- Dumping data for table `total_subject`
--

INSERT INTO `total_subject` (`id`, `subjectName`) VALUES
(1000, 'math'),
(1001, 'physics'),
(1002, 'DBMS'),
(1003, 'Compiler'),
(1004, 'Computer Networks'),
(1005, 'mechanics'),
(1006, 'chemistry');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
