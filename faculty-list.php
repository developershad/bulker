<?php include('config.php'); 
	if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		if($_SESSION['TYPE'] != "ADMIN")
		{
			header("location:home.php");	
		}
	}
	if(isset($_POST['facultyEditButton']))
	{
		$updateData = updateFaculty($_POST, $_FILES, $conn);	
	}
	$facultyData  = facultyList($conn);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#product-table .minwidth-1 {
    min-width: 20px;
}

</style>
</head>
<body> 
<?php include('common/nav.php') ?>
 <div class="clear"></div>
 
<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer">
<!-- start content -->
<div id="content">

	<!--  start page-heading -->
	<div id="page-heading">
		<h1>Faculty List</h1>
	</div>
	<!-- end page-heading -->

	<table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
	<tr>
		<th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
		<th class="topleft"></th>
		<td id="tbl-border-top">&nbsp;</td>
		<th class="topright"></th>
		<th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
	</tr>
	<tr>
		<td id="tbl-border-left"></td>
		<td>
		<!--  start content-table-inner ...................................................................... START -->
		<div id="content-table-inner">
		<table border="0" width="100%" cellpadding="" cellspacing="0">
          <tr>
			<td>
              <!--<div id="message-yellow">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="yellow-left">Attendance Saved Sucessfully on Dated: 24:Dec:2013 at 01:35PM</td>
					<td class="yellow-right"><a class="close-yellow"><img src="images/table/icon_close_yellow.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>-->
                </td>					
			  </tr>     
<tr>
<td>
		<div id="editForm" style="display:none; width:96%; margin-bottom:1%;" class="editFormBackground" >
                    <h1 class="editHording" >UPDATE FACULTY RECORD<span class="closeButton" onclick="javascript: closeEditForm();" >&nbsp;</span></h1>
                    <table style="border:1px solid #FFF;" width="100%" cellpadding="0" cellspacing="0">
                      <tr valign="top">
                        <td><!-- start id-form -->
                          
                     <form name="facultyEditForm" id="facultyEditForm" action="" method="post" enctype="multipart/form-data" style=" margin: 1% 0 0;" />
                      <input type="hidden" name="id" />
                      <input type="hidden" name="OldFacultyImage" />
                          
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form"  >                    
                    <tr>
                      <th valign="top"><span class="leftPadding_2">Faculty name:</span></th>
                      <td><input type="text" class="inp-form" name="facultyName" /></td>
                      <th valign="top"><span class="leftPadding_2">Father's name:</span></th>
                      <td><input type="text" class="inp-form" name="fatherName" /></td>
                      <th valign="top"><span class="leftPadding_2">Mother's name:</span></th>
                      <td><input type="text" class="inp-form" name="motherName" /></td>
                    </tr>
                    <tr>                      
                      <th valign="top"><span class="leftPadding_2">Exp:</span></th>
                      <td><input type="text" class="inp-form" name="experience" /></td>
                      <th valign="top"><span class="leftPadding_2">DOB</span></th>
                      <td><input type="text" class="inp-form" name="dob" id="dob" readonly="readonly" /></td>
                      <th valign="top"><span class="leftPadding_2">Nationality:</span></th>
                      <td><input type="text" class="inp-form" name="nationality" value="Indian" /></td>
                    </tr>
                    <tr>
                      <th valign="top"><span class="leftPadding_2">Mobile</span></th>
                      <td><input type="text" class="inp-form" name="mobile" /></td>
                      <th valign="top"><span class="leftPadding_2">Email</span></th>
                      <td><input type="text" class="inp-form" name="email" /></td>
                      <th valign="top"><span class="leftPadding_2">Sex</span></th>
                      <td><select name="sex" class="selectStyle"  >                                                        
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                          </select></td>
                    </tr>
                    <tr>                      
                      <th valign="top"><span class="leftPadding_2">Username:</span></th>
                      <td><input type="text" class="inp-form" name="username" id="username" disabled="disabled"  readonly="readonly" /></td>
                      <th valign="top"><span class="leftPadding_2">Password</span></th>
                      <td><input type="password" class="inp-form" name="password" id="password" /></td>
                      <th valign="top"><span class="leftPadding_2">Confirm Password:</span></th>
                      <td><input type="password" class="inp-form" name="confirmPassword" id="confirmPassword" /></td>
                    </tr>
                    <tr>                          
                      <th valign="top"><span class="leftPadding_2">Marital Status</span></th>
                      <td><select name="maritalStatus" class="selectStyle" >                                                        
                            <option value="Unmarried">Unmarried</option>
                            <option value="Married">Married</option>
                          </select></td>                     
                      <th><span class="leftPadding_2">photo</span></th>
                      <td><input type="file" class="file_1" name="facultyImage"  /></td>  
                    </tr>
                    
                    <tr>                                       	  
                      <th valign="top"><span class="leftPadding_2">Permament:</span></th>
                      <td><textarea rows="" cols="" class="form-textarea" name="permamentAddress" style="width:90%; height:50px;" ></textarea></td>
                      <th valign="top"><span class="leftPadding_2">Residential:</span></th>
                      <td><textarea rows="" cols="" class="form-textarea" name="residentialAddress" style="width:90%; height:50px;" ></textarea></td>
                    </tr>
                    <tr>
                      <th>&nbsp;</th>
                      <td valign="top"><input type="submit" value="Submit" class="form-submit" name="facultyEditButton" id="facultyEditButton" />
                        <input type="reset" value="" class="form-reset"  /></td>
                      <td></td>
                    </tr>
                  </table>
                          </form>
                          
                          <!-- end id-form  --></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                        <td></td>
                      </tr>
                    </table>
                    <div class="clear"></div>
                  </div>
</td>
</tr>                      
            <tr valign="top">
                <td>              
              <!-- start id-form -->
              <div><center><img id="loaderImage" /></center></div>
<?php if($facultyData){ ?>              
<table border="0" width="100%" cellpadding="0" cellspacing="0" id="product-table" >
				<tr>
					<th class="table-header-repeat line-left minwidth-1"><a>SN</a>	</th>
                    <th class="table-header-repeat line-left minwidth-1"><a >Name</a>	</th>
					<th class="table-header-repeat line-left"><a >Email</a></th>
					<th class="table-header-repeat line-left"><a >Mobile</a></th>
					<th class="table-header-repeat line-left"><a >Image</a></th>
					<th class="table-header-options line-left"><a >Options</a></th>
				</tr>
<?php $sn=0; foreach ($facultyData as $facultyVal ){ $sn++; ?>   					             
					<tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> id="faculty_tr<?php echo $facultyVal['id']; ?>" >
					<td ><?php echo $sn; ?></td>
                    <td><?php echo $facultyVal['facultyName']  ?></td>
                    <td><?php echo $facultyVal['email'] ?></td>
                    <td><?php echo $facultyVal['mobile'] ?></td>
                    <td style="width:10%;">
                    <img src="<?php echo $facultyVal['facultyImage'] ?>" width="57%" style="margin-bottom:-13%; margin-top: -10%; max-height: 50px;" ></td>           
					<td class="options-width">
                    <a href="" title="View" class="icon-3 info-tooltip"
                     onMouseover="ddrivetip('<p><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Name</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["facultyName"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Email</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["email"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Mobile</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["mobile"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Father Name</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["fatherName"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Mother Name</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["motherName"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Experience</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["experience"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>DOB</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["dob"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Nationality</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["nationality"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Sex</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["sex"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Marital Status</font></b> : &nbsp;&nbsp;&nbsp;<font style=font-size:12px><?php echo $facultyVal["maritalStatus"]; ?></font><br/><font style=font-size:12px;font-family:Arial><b><font color=#224A9B>Permament Address</font></b> :<font style=font-size:12px;><?php echo mysql_real_escape_string( $facultyVal["permamentAddress"]); ?></font><font style=font-size:12px;font-family:Arial><b><br/><font color=#224A9B>Residential Address</font></b> :<font style=font-size:12px><?php echo mysql_real_escape_string( $facultyVal["residentialAddress"]); ?></font></b></p>');" onMouseout="hideddrivetip();"
                    ></a>
					<a href="javascript: edit(
                    '<?php echo $facultyVal['id']; ?>',
                    '<?php echo $facultyVal['facultyName']; ?>',
                    '<?php echo $facultyVal['fatherName']; ?>',
                    '<?php echo $facultyVal['motherName']; ?>',
                    '<?php echo $facultyVal['experience']; ?>',
                    '<?php echo $facultyVal['dob']; ?>',
                    '<?php echo $facultyVal['nationality']; ?>',
                    '<?php echo $facultyVal['mobile']; ?>',
                    '<?php echo $facultyVal['email']; ?>',
                    '<?php echo $facultyVal['sex']; ?>',
                    '<?php echo $facultyVal['maritalStatus']; ?>',
                   '<?php echo $facultyVal['facultyImage']; ?>',
                   '<?php echo $facultyVal['permamentAddress']; ?>',
                   '<?php echo $facultyVal['residentialAddress']; ?>',
                   '<?php echo $facultyVal['username']; ?>',
                   '<?php echo $facultyVal['password']; ?>'                    
                    );" title="Edit" class="icon-4 info-tooltip"></a>
                    <a href="javascript:deleteFaculty('<?php echo $facultyVal['id']; ?>');" title="Delete" class="icon-2 info-tooltip"></a>					
					
					</td>
				</tr>
<?php } ?>                				
				</table>
<?php } else { ?>
	<script>document.getElementById('loaderImage').src='images/nrf.png';</script>
    <!--  start related-activities -->
                  
                  <div id="related-activities"  > 
                    
                    <!--  start related-act-top -->
                    <div id="related-act-top"> <img src="images/forms/header_related_act.gif" width="271" height="43" alt="" /> </div>
                    <!-- end related-act-top --> 
                    
                    <!--  start related-act-bottom -->
                    <div id="related-act-bottom"> 
                      
                      <!--  start related-act-inner -->
                      <div id="related-act-inner">
                        <div class="left"></div>
                        <div class="right">
                          <ul class="greyarrow">
                            <li><a href="faculty-add.php" style="font-size:15px" >Faculty Add</a></li>
                          </ul>
                        </div>
                        <div class="clear"></div>
                      </div>
                      <!-- end related-act-inner -->
                      <div class="clear"></div>
                    </div>
                  </div>
                  
                  <!-- end related-act-bottom -->
	
 <?php }?>                
              <!-- end id-form  -->
              
              </td>                       
            </tr>
          </table>
			<!--  start table-content  -->
            
			<div id="table-content"></div>
			<!--  end content-table  -->
			<div class="clear"></div> 
		<!--  end content-table-inner ............................................END  -->
		</td>
		<td id="tbl-border-right"></td>
	</tr>
	<tr>
		<th class="sized bottomleft"></th>
		<td id="tbl-border-bottom">&nbsp;</td>
		<th class="sized bottomright"></th>
	</tr>
	</table>  
    <div id="facultyDel" style="display:none"; ></div>  
	<div class="clear">&nbsp;</div>

</div>
<!--  end content -->
<div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>
    
<!-- start footer -->         
<div id="footer">
	<!--  start footer-left -->
	<?php include('common/footer.php') ?>
	<!--  end footer-left -->
	<div class="clear">&nbsp;</div>
</div>
<!-- end footer -->
<script>
function edit(id, facultyName, fatherName, motherName, experience, dob, nationality, mobile, email, sex, maritalStatus, facultyImage, permamentAddress, residentialAddress, username, password ){
		$("#editForm").css({
		"display": "table-row"});
	window.scrollTo(0,250);			
	document.facultyEditForm.id.value = id;
	document.facultyEditForm.OldFacultyImage.value = facultyImage;
	document.facultyEditForm.facultyName.value = facultyName;
	document.facultyEditForm.fatherName.value = fatherName;
	document.facultyEditForm.motherName.value = motherName;
	document.facultyEditForm.experience.value = experience;
	document.facultyEditForm.dob.value = dob;
	document.facultyEditForm.nationality.value = nationality;
	document.facultyEditForm.mobile.value = mobile;
	document.facultyEditForm.email.value = email;
	document.facultyEditForm.sex.value = sex;
	document.facultyEditForm.maritalStatus.value = maritalStatus;
	document.facultyEditForm.permamentAddress.value = permamentAddress;
	document.facultyEditForm.residentialAddress.value = residentialAddress;
	document.facultyEditForm.username.value = username;
	document.facultyEditForm.password.value = password;
	document.facultyEditForm.confirmPassword.value = password;
}
function closeEditForm(){
		$("#editForm").css({
		"display": "none"});	
}
function deleteFaculty(id){
var r=confirm("Are You Sure?");
if (r==true)
  {
	  	$('#facultyDel').load('data8.php?id='+id);
	  	$("#faculty_tr"+id).css({
		"display": "none"});
  }
}
</script>
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"dob",
			dateFormat:"%Y-%m-%d"
		});
	};
</script>
</body>
</html>