<?php include('config.php'); 
	if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		if($_SESSION['TYPE'] != "ADMIN")
		{
			header("location:home.php");	
		}
	}	
	$courseData  = courseList($conn); 
	$assignFacultyListData = assignFacultyList($conn);
	$totalSubject = subjectList($conn)
?>
<!DOCTYPE>
<html>
<head>
<link rel="stylesheet" href="jtable/css/bootstrap.min.css">
<link rel="stylesheet" href="jtable/css/DT_bootstrap.css">
</head>
<body>
<br/><br/>
<div style="width:90%; margin:0 auto;" >
<table id="dataTable" class="table table-bordered table-condensed table-hover table-striped" style="font-size:12px;" >
                  <thead>
                    <tr>
                      <th>SN</th>
                      <th>Faculty Name</th>
                      <th>Course</th>
                      <th>Branch</th>
                      <th>Year</th>
                      <th>Subject</th>
                      <th>Subject Code</th>
                      <th>Delete</th>
                    </tr>
                  </thead>
                  <tbody>
                      <?php $sn=0; foreach ($assignFacultyListData as $aflVal ){ $sn++;  ?>
                      <!--<tr id="assignFaculty<?php echo $aflVal['id'] ?>" >
                        <td ><?php echo $sn; ?></td>
                        <td><?php echo $aflVal['facultyName']  ?></td>
                        <td><?php echo $aflVal['course'] ?></td>
                        <td><?php echo $aflVal['branch'] ?></td>
                        <td><?php echo $aflVal['courseYear'] ?></td>
                        <td><?php echo $aflVal['subjectName'] ?></td>
                        <td><?php echo 'Subject '.$aflVal['subjectCode'] ?></td>
                        <td><a onclick="deleteAssignFaculty('<?php echo $aflVal['id']; ?>');" title="Delete" class="icon-2 info-tooltip"></a></td>
                      <tr>-->
                      <tr id="assignFaculty<?php echo $aflVal['id'] ?>" >
                      <td><?php echo $sn; ?></td>
                      <td><?php echo $aflVal['facultyName']  ?></td>
                      <td><?php echo $aflVal['course'] ?></td>
                      <td><?php echo $aflVal['branch'] ?></td>
                      <td><?php echo $aflVal['courseYear'] ?></td>
                      <td><?php echo $aflVal['subjectName'] ?></td>
                      <td><?php echo 'Subject '.$aflVal['subjectCode'] ?></td>
                      <td><a onclick="deleteAssignFaculty('<?php echo $aflVal['id']; ?>');" title="Delete"><img src="images/table/action_delete.gif" style="cursor:pointer;" ></a></td>
                    </tr>
                        <?php } ?>
                  </tbody>
                </table>
</div>
<div id="afDel" style="display:none;" ></div>
<script>
function deleteAssignFaculty(id){
var r=confirm("Are You Sure?");
if (r==true)
  {
	  	$('#afDel').load('data9.php?id='+id);
	  	$("#assignFaculty"+id).css({
		"display": "none"});
  }
}
</script>

<script src="jtable/js/jquery.min.js"></script> 
<script src="jtable/js/bootstrap.min.js"></script> 
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
<script src="jtable/js/jquery.dataTables.js"></script> 
<script src="jtable/js/DT_bootstrap.js"></script> 
<script src="jtable/js/jquery.tablesorter.min.js"></script> 
<script src="jtable/js/main.min.js"></script> 
<script>
      $(function() {
        metisTable();
      });
    </script>


</body>
</html>    