<?php
	include('config.php');
	$error = 0;
	if(isset($_POST['loginButton']))
	{
		$data = login($_POST, $conn);
		if($data){ header("location:home.php"); }
		else {	$error = 1;	}
	}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php') ?>
</head>
<body id="login-bg"> 
 
<!-- Start: login-holder -->
<div id="login-holder">

	<!-- start logo -->
    
	<div id="logo-login">
		<a href="index.html"><h1 style="color:#FFF; margin-left:15px;">Ashoka Institute of Technology & Managment</h1></a>
      </div>
	<!-- end logo -->
	
	<div class="clear"></div>
	
	<!--  start loginbox ................................................................................. -->
	<div id="loginbox">
	<h1 style="margin-bottom: 5%; margin-left: 38%; margin-top: -6%;" >Admin Login</h1>
    <?php if($error){ ?><span style="color:#F00; margin-left:5%; margin-left: 41%;" >Invalid Username / Password</span> <?php } ?>
	<!--  start login-inner -->
    
	<div id="login-inner">
    	<form name="adminLoginForm" id="adminLoginForm" method="post" >
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Username</th>
			<td><input type="text" name="username" class="login-inp" /></td>
		</tr>
		<tr>
			<th>Password</th>
			<td><input type="password"  name="password" class="login-inp" /></td>
		</tr>
       <!--<tr>
			<th>Type</th>
			<td>
            <select name="loginType" class="styledselect" style="border-radius: 5px; width: 220px; height: 32px; font-size: 15px;" >
              <option value="Faculty">Faculty</option>
              <option value="Admin">Admin</option>
            </select></td>
		</tr>-->
		<!--<tr>
			<th></th>
			<td valign="top"><input type="checkbox" class="checkbox-size" id="login-check" /><label for="login-check">Remember me</label></td>
		</tr>-->
		<tr>
			<th></th>
			<td><input type="submit" name="loginButton" class="submit-login"  /></td>
		</tr>
		</table>
        </form>
	</div>
 	<!--  end login-inner -->
	<div class="clear"></div>
	<a href="" class="forgot-pwd">Forgot Password?</a>
 </div>
 <!--  end loginbox -->
 
	<!--  start forgotbox ................................................................................... -->
	<div id="forgotbox">
		<div id="forgotbox-text">Please send us your email and we'll reset your password.</div>
		<!--  start forgot-inner -->
		<div id="forgot-inner">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<th>Email address:</th>
			<td><input type="text" value=""   class="login-inp" /></td>
		</tr>
		<tr>
			<th> </th>
			<td><input type="button" class="submit-login"  /></td>
		</tr>
		</table>
		</div>
		<!--  end forgot-inner -->
		<div class="clear"></div>
		<a href="" class="back-login">Back to login</a>
	</div>
	<!--  end forgotbox -->

</div>
<!-- End: login-holder -->
</body>
</html>