<?php
	include('config.php');
	$courseId=$_GET['id'];	
    $courseYearData  = courseYearList($courseId, $conn);
	echo "<script>document.getElementById('loaderImage').src='images/shared/blank.gif';</script>";
	?>

<select name="courseYear" id="cy"  class="selectStyle">
  <?php foreach($courseYearData as $courseYearVal){ ?>
  <option value="<?php echo $courseYearVal['id']; ?>"><?php echo $courseYearVal['courseYear']; ?></option>
  <?php } ?>
</select>
