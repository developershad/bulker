<?php $pageName = basename($_SERVER['PHP_SELF']);  ?>
<div id="page-top-outer">
  <div id="page-top">
    <div id="logo"> <a href="home.php"><img src="images/shared/logo.png" style="width:86%;" alt="" /></a> </div>
    <div id="top-search" style="float:left; margin-left:16%;" >
      <table border="0" cellpadding="0" cellspacing="0">        
        <tr><td ><p class="logoHeading" >ASHOKA</p></td></tr>
        <tr><td><p class="logoText" >INSTITUTE OF TECHNOLOGY AND MANAGMENT</p></td></tr>
      </table>
    </div>
    <div style=" float: right; margin-top: 0.5%;  margin-right: 0.5%;" > <img src="
    <?php if($_SESSION['TYPE'] == 'SUPERADMIN' )
			{	echo 'images/shared/superadmin.jpg';	}
		else if($_SESSION['TYPE'] == 'ADMIN' )
			{	echo 'images/shared/admin.jpg';	}
		else if($_SESSION['TYPE'] == 'FACULTY' )
			{	echo 'images/shared/faculty.jpg';	}	
	 ?>"  alt="" /> </div>
    <div class="clear"></div>
  </div>
</div>
<div class="clear">&nbsp;</div>
<div class="nav-outer-repeat"> 
  <!--  start nav-outer -->
  <div class="nav-outer"> 
    
    <!-- start nav-right -->
    <div id="nav-right">
      <div class="nav-divider">&nbsp;</div>
      <div class="showhide-account">
        <h3 style="color:#F5F5F5;" ><?php echo  ((strlen($_SESSION['NAME'])) > 15)? substr($_SESSION['NAME'], 0,15).".." :$_SESSION['NAME']; ?><img src="images/table/table_sort_arrow.gif" /></h3>
      </div>
      <div class="nav-divider">&nbsp;</div>
      <a href="logout.php" id="logout"><img src="images/shared/nav/nav_logout.gif" width="64" height="14" alt="" /></a>
      <div class="clear">&nbsp;</div>
      
      <!--  start account-content -->
      <div class="account-content">
        <div class="account-drop-inner">
        <?php if(($_SESSION['TYPE'] == "SUPERADMIN") or ($_SESSION['TYPE'] == "ADMIN")) {?>
        <a href="assign-faculty.php" id="acc-settings">Assign Faculty</a>
          <div class="clear">&nbsp;</div>
          <div class="acc-line">&nbsp;</div>
        <?php } ?>  
        <?php if(($_SESSION['TYPE'] == "SUPERADMIN") or ($_SESSION['TYPE'] == "ADMIN")) {?>
        <a href="subject-add.php" id="acc-settings">Add Subjects</a>
          <div class="clear">&nbsp;</div>
          <div class="acc-line">&nbsp;</div>
        <?php } ?>          
        <?php if($_SESSION['TYPE'] == "FACULTY") {?>
          <a href="profile-faculty.php" id="acc-settings">View Profile</a>
          <div class="clear">&nbsp;</div>
          <div class="acc-line">&nbsp;</div>
          <a href="profile-faculty.php" id="acc-details">Change Password </a>
          <div class="clear">&nbsp;</div>
          <div class="acc-line">&nbsp;</div>
         <?php } ?>  
        <?php if($_SESSION['TYPE'] == "SUPERADMIN") {?>  
       <a href="admin-manager.php" id="acc-project">Admin Manager</a>
          <div class="clear">&nbsp;</div>
        <div class="acc-line">&nbsp;</div>
       <?php } ?>  .
       	<?php if(($_SESSION['TYPE'] == "SUPERADMIN") or ($_SESSION['TYPE'] == "ADMIN")) {?>
          <a href="profile-admin.php" id="acc-inbox">View Profile</a>
         <div class="clear">&nbsp;</div>
        <div class="acc-line">&nbsp;</div>
          <a href="student.php" id="acc-project">Student Login</a>
          <div class="clear">&nbsp;</div>
        <?php } ?>  
          </div>
      </div>
      <!--  end account-content --> 
      
    </div>
    <!-- end nav-right --> 
    
    <!--  start nav -->

    <div class="nav">
      <div class="table">
   	<?php if(($_SESSION['TYPE'] == "SUPERADMIN") or ($_SESSION['TYPE'] == "ADMIN")) {?>
        <ul class="<?php if($pageName =='student-list.php' OR  $pageName =='student-add.php'){echo 'current'; }else{ echo'select';} ?>">
          <li> <a href="student-list.php"> <b>Student</b> </a>
            <div class="select_sub  <?php if($pageName =='student-list.php'  OR  $pageName =='student-add.php' ){echo 'show'; } ?>">
              <ul class="sub">
                <li <?php if($pageName =='student-add.php'){echo 'class="sub_show"'; }?> > <a href="student-add.php">Add New Student</a> </li>
                <li  <?php if($pageName =='student-list.php'){echo 'class="sub_show"'; }?> > <a href="student-list.php">Student List</a> </li>
              </ul>
            </div>
          </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
       
        <ul class="<?php if($pageName =='faculty-list.php' OR  $pageName =='faculty-add.php'){echo 'current'; }else{ echo'select';} ?>">
          <li><a href="faculty-list.php"><b>Faculty</b><!--[if IE 7]><!--></a><!--<![endif]--> 
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div class="select_sub <?php if($pageName =='faculty-list.php'  OR  $pageName =='faculty-add.php' ){echo 'show'; } ?>">
              <ul class="sub">
                <li <?php if($pageName =='faculty-add.php'){echo 'class="sub_show"'; }?> ><a href="faculty-add.php">Add Faculty</a></li>
                <li <?php if($pageName =='faculty-list.php'){echo 'class="sub_show"'; }?> ><a href="faculty-list.php">Faculty List</a></li>
                >
              </ul>
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]--> 
          </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
      <?php } ?>   
      <?php if($_SESSION['TYPE'] == "FACULTY") {?>
        <ul class="<?php if($pageName =='attendance.php'){echo 'current'; }else{ echo'select';} ?>">
          <li><a href="attendance.php"><b>Attendance</b></a> </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
        <?php } ?>
      <?php if(($_SESSION['TYPE'] == "SUPERADMIN") OR ($_SESSION['TYPE'] == "ADMIN" )) {?>
        <ul class="<?php if($pageName =='attendance-admin.php'){echo 'current'; }else{ echo'select';} ?>">
          <li><a href="attendance-admin.php"><b>Attendance </b></a> </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
        <?php } ?>        
        <ul class="<?php if($pageName =='assignment.php'){echo 'current'; }else{ echo'select';} ?>">
          <li><a href="assignment.php"><b>Assignment</b><!--[if IE 7]><!--></a><!--<![endif]--> 
            <!--[if lte IE 6]><table><tr><td><![endif]--> 
            
            <!--[if lte IE 6]></td></tr></table></a><![endif]--> 
          </li>
        </ul>        
        <div class="nav-divider">&nbsp;</div>
<?php if(($_SESSION['TYPE'] == "SUPERADMIN") or ($_SESSION['TYPE'] == "ADMIN")) {?>        
        <ul class="<?php if($pageName =='notice-board.php'){echo 'current'; }else{ echo'select';} ?>">
          <li><a href="notice-board.php"><b>Notice Board</b><!--[if IE 7]><!--></a><!--<![endif]-->           
          </li>
        </ul>
        <div class="nav-divider">&nbsp;</div>
<?php } ?>        
        <ul class="select">
          <li><a href="report-final.php"><b>Report</b><!--[if IE 7]><!--></a><!--<![endif]--> 
            <!--[if lte IE 6]><table><tr><td><![endif]-->
            <div class="select_sub">            
            </div>
            <!--[if lte IE 6]></td></tr></table></a><![endif]--> 
          </li>
        </ul>
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
    </div>
    <!--  start nav --> 
    
  </div>
  <div class="clear"></div>
  <!--  start nav-outer --> 
</div>
