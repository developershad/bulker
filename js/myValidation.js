$().ready(function() {	
	$("#studentAddForm").validate({
		rules: {			
			course: {
				required: true,
				},
			rollNo: {
				number: true,
				},											
			studentName: {
				required: true,
				},				
			fatherName: {
				required: true,
				},				
			/*motherName: {
				required: false,
				},				
			dob: {
				required: false,
				},*/				
			nationality: {
				required: true,
				},				
			mobile: {
				required: true,
				number: true,
				
				},				
			/*email: {
				required: false,
				email: true,
				},*/
			sex: {
				required: true,
				},				
			categtory: {
				required: true,
				},				
			permamentAddress: {
				required: true,
				},				
			residentialAddress: {
				required: true,
				},								
																				
		},
	});	
	$("#facultyAddForm").validate({
		rules: {									
			facultyName: {
				required: true,
				},				
			fatherName: {
				required: true,
				},				
			motherName: {
				required: true,
				},				
			experience: {
				required: true,
				},
			dob: {
				required: true,
				},				
			nationality: {
				required: true,
				},				
			mobile: {
				required: true,
				number: true,
				},				
			email: {
				required: true,
				email: true,
				},			
			permamentAddress: {
				required: true,
				},				
			residentialAddress: {
				required: true,
				},
			username: {
				required: true,
				},									
			password: {
				required: true,
				},									
			confirmPassword: {
				required: true,
				equalTo: '#password',
				},									
																				
		},
	});	
$("#facultyEditForm").validate({
		rules: {									
			facultyName: {
				required: true,
				},				
			fatherName: {
				required: true,
				},				
			motherName: {
				required: true,
				},				
			experience: {
				required: true,
				},
			dob: {
				required: true,
				},				
			nationality: {
				required: true,
				},				
			mobile: {
				required: true,
				number: true,
				},				
			email: {
				required: true,
				email: true,
				},			
			permamentAddress: {
				required: true,
				},				
			residentialAddress: {
				required: true,
				},
			username: {
				required: true,
				},									
			password: {
				required: true,
				},									
			confirmPassword: {
				required: true,
				equalTo: '#password',
				},									
																				
		},
	});	
	$("#studentEditForm").validate({
		rules: {			
			course: {
				required: true,
				},
			rollNo: {
				number: true,
				},											
			studentName: {
				required: true,
				},				
			fatherName: {
				required: true,
				},				
			/*motherName: {
				required: true,
				},				
			dob: {
				required: true,
				},	*/			
			nationality: {
				required: true,
				},				
			mobile: {
				required: true,
				number: true,
				
				},				
			/*email: {
				required: true,
				email: true,
				},*/
			sex: {
				required: true,
				},				
			categtory: {
				required: true,
				},				
			permamentAddress: {
				required: true,
				},				
			residentialAddress: {
				required: true,
				},								
																				
		},
	});	
	$("#adminLoginForm").validate({
		rules: {			
			username: {
				required: true,
				},
			password: {
				required: true,
				},																										
		},
	});	
	$("#facultyLogin").validate({
		rules: {			
			username: {
				required: true,
				},
			password: {
				required: true,
				},
			loginType: {
				required: true,
				},																														
		},
	});	
	$("#assignmentForm").validate({
		rules: {			
			course: {
				required: true,
				},	
			topic: {
				required: true,
				},	
			lastDate: {
				required: true,
				},
			assignment: {
				required: true,
				},																																		
		},
	});	
	$("#noticeBoardForm").validate({
		rules: {			
			heading: {
				required: true,
				},																																
		},
	});	
	$("#adminAddForm").validate({
		rules: {			
			name: {
				required: true,
				},	
			fatherName: {
				required: true,
				},	
			motherName: {
				required: true,
				},	
			email: {
				required: true,
				},	
			dob: {
				required: true,
				},	
			nationality: {
				required: true,
				},	
			mobile: {
				required: true,
				},	
			password: {
				required: true,
				},	
			confirmPassword: {
				required: true,
				equalTo: '#password',
				},	
			username: {
				required: true,
				},	
			permamentAddress: {
				required: true,
				},	
			residentialAddress: {
				required: true,
				},									
																																																							
		},
	});	
	$("#adminEditForm").validate({
		rules: {			
			name: {
				required: true,
				},	
			fatherName: {
				required: true,
				},	
			motherName: {
				required: true,
				},	
			email: {
				required: true,
				},	
			dob: {
				required: true,
				},	
			nationality: {
				required: true,
				},	
			mobile: {
				required: true,
				},	
			password: {
				required: true,
				},	
			confirmPassword: {
				required: true,
				equalTo: '#passwordEdit',
				},	
			username: {
				required: true,
				},	
			permamentAddress: {
				required: true,
				},	
			residentialAddress: {
				required: true,
				},									
																																																							
		},
	});
	$("#subjectAddForm").validate({
		rules: {			
			subjectName: {
				required: true,
				},																																																					
		},
	});															
});

