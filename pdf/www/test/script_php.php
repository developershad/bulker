<?php 
include("../config.php");
	$course=$_GET['course'];
	$courseName = courseName($course, $conn);
	$branch=$_GET['branch'];
	$branchName = branchName($branch, $conn);
	$courseYear=$_GET['courseYear'];
	$yearName = yearName($courseYear, $conn);
		
    $studentListData  = searchStudentList($course, $branch, $courseYear, $conn);
	$student = finalReport($course, $branch, $courseYear, $conn);
	$subject1 = $subject2 = $subject3 = $subject4 = $subject5 = $subject6 = $subject7 = $subject8  =0;
	$subject1T = $subject2T = $subject3T = $subject4T = $subject5T = $subject6T = $subject7T = $subject8T  =0;
	
	$subject1 = subjectName($course, $branch, $courseYear, 1, $conn);
	$subject2 = subjectName($course, $branch, $courseYear, 2, $conn);
	$subject3 = subjectName($course, $branch, $courseYear, 3, $conn);
	$subject4 = subjectName($course, $branch, $courseYear, 4, $conn);
	$subject5 = subjectName($course, $branch, $courseYear, 5, $conn);
	$subject6 = subjectName($course, $branch, $courseYear, 6, $conn);
	$subject7 = subjectName($course, $branch, $courseYear, 7, $conn);
	$subject8 = subjectName($course, $branch, $courseYear, 8, $conn);
	
	$subject1T = subjectTotal($course, $branch, $courseYear, 1, $conn);
	$subject2T = subjectTotal($course, $branch, $courseYear, 2, $conn);
	$subject3T = subjectTotal($course, $branch, $courseYear, 3, $conn);
	$subject4T = subjectTotal($course, $branch, $courseYear, 4, $conn);
	$subject5T = subjectTotal($course, $branch, $courseYear, 5, $conn);
	$subject6T = subjectTotal($course, $branch, $courseYear, 6, $conn);
	$subject7T = subjectTotal($course, $branch, $courseYear, 7, $conn);
	$subject8T = subjectTotal($course, $branch, $courseYear, 8, $conn);

 ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<title>Ashoka Software</title>
<style>
#product-table tr.alternate-row	{
	background: #ececec;
	}
</style>
</head>

<body>
<img src="logo.png" ><img src="logo-txt.png" />
<table border="0px" width="100%" cellpadding="0" cellspacing="0"  >
<tr><td>&nbsp;</td></tr>
<tr>
<td><b>Course : </b></span><?php echo $courseName; ?></td>
<td><b>Branch : </b></span><?php echo $branchName; ?></td>
<td><b>Year : </b></span><?php echo $yearName?></td>
</tr>
<tr><td>&nbsp;</td></tr>
</table>

<table border="1px" width="100%" cellpadding="0" cellspacing="0" id="product-table" >
  <tr >
    <th class="table-header-repeat line-left minwidth-1"><a>SN</a> </th>
    <th class="table-header-repeat line-left minwidth-1"><a >Name</a> </th>
    <th class="table-header-repeat line-left minwidth-1"><a >Roll No</a></th>
    <?php if($subject1){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject1; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <?php if($subject2){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject2; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <?php if($subject3){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject3; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <?php if($subject4){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject4; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <?php if($subject5){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject5; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <?php if($subject6){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject6; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <?php if($subject7){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject7; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <?php if($subject8){ ?>
    <th class="table-header-repeat line-left"><a ><?php echo $subject8; ?></a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
    <?php } ?>
    <th class="table-header-repeat line-left"><a >Total</a>
      <table  width="100%" style="margin-top:10px;" >
        <tr>
          <td style="border:0px;" class="tableHeading">T</td>
          <td style="border:0px;" class="tableHeading">P</td>
          <td style="border:0px;" class="tableHeading">%</td>
        </tr>
      </table>
    </th>
  </tr>
   <?php $sn=0; foreach($student as $stdVal){ ++$sn;
  ?>
  <tr <?php echo ($sn%2==0)?'class="alternate-row"':''; ?> >
    <td style="padding-left: 4px;" ><?php echo $sn; ?></td>
    <td style="padding-left: 4px;"><?php echo $stdVal['studentName']; ?></td>
    <td style="padding-left: 4px;" ><?php echo $stdVal['rollNo']; ?></td>
    <?php if($subject1){ ?>
     <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject1T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub1']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub1']/$subject1T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
      <?php if($subject2){ ?>
    <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject2T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub2']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub2']/$subject2T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
      <?php if($subject3){ ?>
    <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject3T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub3']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub3']/$subject3T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
      <?php if($subject4){ ?>
    <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject4T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub4']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub4']/$subject4T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
      <?php if($subject5){ ?>
    <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject5T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub5']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub5']/$subject5T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
      <?php if($subject6){ ?>
    <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject6T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub6']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub6']/$subject6T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
      <?php if($subject7){ ?>
    <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject7T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub7']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub7']/$subject7T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
      <?php if($subject8){ ?>
    <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $subject8T; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $stdVal['sub8']; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo round($stdVal['sub8']/$subject8T*100).'%'; ?></td>
        </tr>
      </table></td>
      <?php } ?>
 <?php 
 $totalLecture = 0;
 $totalPresent = 0;
 $totalPer = 0;
 if($subject1){	$totalLecture = $totalLecture + $subject1T;		}
 if($subject2){	$totalLecture = $totalLecture + $subject2T;		}
 if($subject3){	$totalLecture = $totalLecture + $subject3T;		}
 if($subject4){	$totalLecture = $totalLecture + $subject4T;		}
 if($subject5){	$totalLecture = $totalLecture + $subject5T;		}
 if($subject6){	$totalLecture = $totalLecture + $subject6T;		}
 if($subject7){	$totalLecture = $totalLecture + $subject7T;		}
 if($subject8){	$totalLecture = $totalLecture + $subject8T;		}
 
  
 if($subject1){	$totalPresent = $totalPresent + $stdVal['sub1'];		}
 if($subject2){	$totalPresent = $totalPresent + $stdVal['sub2'];		}
 if($subject3){	$totalPresent = $totalPresent + $stdVal['sub3'];		}
 if($subject4){	$totalPresent = $totalPresent + $stdVal['sub4'];		}
 if($subject5){	$totalPresent = $totalPresent + $stdVal['sub5'];		}
 if($subject6){	$totalPresent = $totalPresent + $stdVal['sub6'];		}
 if($subject7){	$totalPresent = $totalPresent + $stdVal['sub7'];		}
 if($subject8){	$totalPresent = $totalPresent + $stdVal['sub8'];		}
 
 $totalPer =  round( $totalPresent/$totalLecture*100);
 
  ?>     
      
      <td><table  width="100%" >
        <tr>
          <td style="border:0px; text-align: center; " ><?php echo $totalLecture; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $totalPresent; ?></td>
          <td style="border:0px; text-align: center; " ><?php echo $totalPer.'%'; ?></td>
        </tr>
      </table></td>
  </tr>
  <?php } ?>
</table>




</body> </html>
