<?php include('config.php'); 
	if($_SESSION['TYPE'] != "SUPERADMIN")
	{
		if($_SESSION['TYPE'] != "ADMIN")
		{
			header("location:home.php");	
		}
	}	
	$id = $_SESSION['ID'];
	if(isset($_POST['submit']))
	{
		$updateData = updateAdmin($id, $_POST, $conn);	
	}	
	$adminData = adminViewById($id, $conn);	
//	echo "<pre>"; print_r($adminData); die;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include('common/head.php'); ?>
<style>
#table-content {
    line-height: 16px;
    margin: 0 10px 10px;
    min-height: 0;
}
</style>
</head>
<body>
<?php include('common/nav.php') ?>
<div class="clear"></div>

<!-- start content-outer ........................................................................................................................START -->
<div id="content-outer"> 
  <!-- start content -->
  <div id="content"> 
    
    <!--  start page-heading -->
    
    <!-- end page-heading -->
    
    <table border="0" width="100%" cellpadding="0" cellspacing="0" id="content-table">
      <tr>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowleft.jpg" width="20" height="300" alt="" /></th>
        <th class="topleft"></th>
        <td id="tbl-border-top">&nbsp;</td>
        <th class="topright"></th>
        <th rowspan="3" class="sized"><img src="images/shared/side_shadowright.jpg" width="20" height="300" alt="" /></th>
      </tr>
      <tr>
        <td id="tbl-border-left"></td>
        <td><!--  start content-table-inner ...................................................................... START -->
          
          <div id="content-table-inner">
            <table border="0" width="100%" cellpadding="" cellspacing="0">
              <tr>
                <td>
                <?php if(isset($updateData)) { ?>
                <div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
				<tr>
					<td class="green-left">Record Updated Sucessfully</td>
					<td class="green-right"><a class="close-green"><img src="images/table/icon_close_green.gif"   alt="" /></a></td>
				</tr>
				</table>
				</div>
                <?php } ?>
                </td>
              </tr>
              <tr valign="top">
                <td>                 
                  <div id="editForm" style="width:96%; margin-bottom:1%;" class="editFormBackground" >
                    <h1 class="editHording" >PROFILE VIEW</h1>
                    <table style="border:1px solid #FFF;" width="100%" cellpadding="0" cellspacing="0">
                      <tr valign="top">
                        <td><!-- start id-form -->
                          
                     <form name="adminAddForm" id="adminAddForm" action="" method="post"  style=" margin: 1% 0 0;" />
                      <input type="hidden" name="id" value="<?php echo $adminData[0]['id'] ?>" />
                      <input type="hidden" name="OldFacultyImage" value="<?php echo isset($adminData[0]['facultyImage'])?$adminData[0]['facultyImage']:''; ?>" />
                          
                    <table border="0" cellpadding="0" cellspacing="0"  id="id-form"  >                    
                    <tr>
                      <th valign="top"><span class="leftPadding_2">Admin name:</span></th>
                      <td><input type="text" class="inp-form" name="name" value="<?php echo $adminData[0]['name'] ?>"  /></td>
                      <th valign="top"><span class="leftPadding_2">Father's name:</span></th>
                      <td><input type="text" class="inp-form" name="fatherName" value="<?php echo $adminData[0]['fatherName'] ?>" /></td>
                      <th valign="top"><span class="leftPadding_2">Mother's name:</span></th>
                      <td><input type="text" class="inp-form" name="motherName" value="<?php echo $adminData[0]['motherName'] ?>" /></td>
                    </tr>
                    <tr>                      
                      <th valign="top"><span class="leftPadding_2">Email:</span></th>
                      <td><input type="text" class="inp-form" name="email" value="<?php echo $adminData[0]['email'] ?>" /></td>
                      <th valign="top"><span class="leftPadding_2">DOB</span></th>
                      <td><input type="text" class="inp-form" name="dob" id="dob" readonly="readonly" value="<?php echo $adminData[0]['dob'] ?>" /></td>
                      <th valign="top"><span class="leftPadding_2">Nationality:</span></th>
                      <td><input type="text" class="inp-form" name="nationality" value="<?php echo $adminData[0]['nationality'] ?>"  /></td>
                    </tr>
                    <tr>
                      <th valign="top"><span class="leftPadding_2">Mobile</span></th>
                      <td><input type="text" class="inp-form" name="mobile" value="<?php echo $adminData[0]['mobile'] ?>" /></td>
                      <th valign="top"><span class="leftPadding_2">Type</span></th>
                      <td><input type="text" class="inp-form" name="type" value="<?php echo $adminData[0]['type'] ?>" readonly="readonly" /></td>
                      <th valign="top"><span class="leftPadding_2">Sex</span></th>
                      <td><select name="sex" id="sex" class="selectStyle"  >                                                
                            <option value="Male" <?php if($adminData[0]['sex'] == "Male"){ echo 'selected="selected"'; } ?> >Male</option>
                            <option value="Female" <?php if($adminData[0]['sex'] == "Female"){ echo 'selected="selected"'; } ?>  >Female</option>
                          </select></td>
                    </tr>
                    <tr>                      
                      <th valign="top"><span class="leftPadding_2">Username:</span></th>
                      <td><input type="text" class="inp-form" name="username" id="username" disabled="disabled" value="<?php echo $adminData[0]['username'] ?>"  readonly="readonly" /></td>
                      <th valign="top"><span class="leftPadding_2">Password</span></th>
                      <td><input type="password" class="inp-form" name="password" id="password" value="<?php echo $adminData[0]['password'] ?>" /></td>
                      <th valign="top"><span class="leftPadding_2">Confirm Password:</span></th>
                      <td><input type="password" class="inp-form" name="confirmPassword" id="confirmPassword" value="<?php echo $adminData[0]['password'] ?>" /></td>
                    </tr>
                    <tr>                          
                      <th valign="top"><span class="leftPadding_2">Marital Status</span></th>
                      <td><select name="maritalStatus" class="selectStyle" >                                                        
                            <option value="Unmarried" <?php if($adminData[0]['maritalStatus'] == "Unmarried"){ echo 'selected="selected"'; } ?> >Unmarried</option>
                            <option value="Married" <?php if($adminData[0]['maritalStatus'] == "Married"){ echo 'selected="selected"'; } ?> >Married</option>
                          </select></td>                                                                           	  
                      <th valign="top"><span class="leftPadding_2">Permament:</span></th>
                      <td><textarea rows="" cols="" class="form-textarea" name="permamentAddress" style="width:90%; height:50px;" ><?php echo $adminData[0]['permamentAddress'] ?></textarea></td>
                      <th valign="top"><span class="leftPadding_2">Residential:</span></th>
                      <td><textarea rows="" cols="" class="form-textarea" name="residentialAddress" style="width:90%; height:50px;" ><?php echo $adminData[0]['residentialAddress'] ?></textarea></td>                      
                    </tr>
                    <tr>
                      <th>&nbsp;</th>
                      <td valign="top"><input type="submit" value="Submit" class="form-submit" name="submit" id="submit" />
                        <input type="reset" value="" class="form-reset"  /></td>
                      <td></td>
                    </tr>
                  </table>
                          </form>
                          
                          <!-- end id-form  --></td>
                        <td></td>
                      </tr>
                      <tr>
                        <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                        <td></td>
                      </tr>
                    </table>
                    <div class="clear"></div>
                  </div>
                  
                  <!-- end id-form  --> 
                  </td>
              </tr>
              <tr>
                <td><img src="images/shared/blank.gif" width="695" height="1" alt="blank" /></td>
                <td></td>
              </tr>
            </table>
            <!--  start table-content  -->
            <div>
              <center>
                <img id="loaderImage" />
              </center>
            </div>
            <div id="table-content"></div>
            <!--  end content-table  -->
            <div class="clear"></div>
          </div>
          
          <!--  end content-table-inner ............................................END  --></td>
        <td id="tbl-border-right"></td>
      </tr>
      <tr>
        <th class="sized bottomleft"></th>
        <td id="tbl-border-bottom">&nbsp;</td>
        <th class="sized bottomright"></th>
      </tr>
    </table>
    <div id="stdDel" style="display:none;" ></div>
    <div class="clear">&nbsp;</div>
  </div>
  <!--  end content -->
  <div class="clear">&nbsp;</div>
</div>
<!--  end content-outer........................................................END -->

<div class="clear">&nbsp;</div>

<!-- start footer -->
<div id="footer"> 
  <!--  start footer-left -->
  <?php include('common/footer.php') ?>
  <!--  end footer-left -->
  <div class="clear">&nbsp;</div>
</div>
<!-- end footer --> 
<script type="text/javascript">
	window.onload = function(){
		new JsDatePick({
			useMode:2,
			target:"dob",
			dateFormat:"%Y-%m-%d"
		});
	};
</script>
</body>
</html>